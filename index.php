<?php 
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: MichaЕ‚ Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
// Ustawienia PHP
ini_set('display_errors','Off');
ini_set('magic_quotes_gpc','Off');
ini_set('register_globals','Off');

date_default_timezone_set('Europe/Warsaw');
set_include_path('.' 
	.  PATH_SEPARATOR . './libs/'
	.  PATH_SEPARATOR . './system/models/'
	.  PATH_SEPARATOR . './system/filters/'
	.  PATH_SEPARATOR . get_include_path());

try {	

// Ustawia autoload w Cube_Loader
require_once 'Cube/Loader.php';
Cube_Loader::registerAutoload();

require_once 'Cube/Exception.php';

// Sprawdz magic quotes
require_once 'Cube/Functions.php';
checkMagicQuotes();

// ZaЕ‚aduj konfiguracje
require_once 'config.php';

// ZaЕ‚aduj rejestr
require_once 'Cube/Registry.php';
$registry = Cube_Registry::getInstance();
$registry->set('systemTemplates', $systemTemplates);

// ZaЕ‚aduj sterownik bazy danych
require_once 'Cube/DbMysql.php';
$dbAdapter = Cube_DbMysql::getInstance($dbSettings);

// ZaЕ‚aduj obiekt Language
$registry->set('ModulesNotLang', $ModulesNotLang);
require_once 'Cube/Language.php';
$language = new Cube_Language();
$registry->set('language', $language);

// ZaЕ‚aduj obiekt request
require_once 'Cube/Request.php';
$request = new Cube_Request();



// ZaЕ‚aduj Config
require_once 'Cube/Config.php';
$config = Cube_Config::getInstance();

// Rejestruj 
$registry->set('config', $config);
$registry->set('request', $request);
$registry->set('dbAdapter', $dbAdapter);
$registry->set('systemLanguages', $systemLanguages);
$registry->set('selectLanguages', $selectLanguages);


require_once 'Cube/UserSession.php';
$session = new Cube_UserSession();
$session->impress();
$session->setTheme($request->getTheme());

$registry->set('session', $session);
$registry->set('currentLanguage', $session->getLanguage());

//redirect dla jezykowosci
/*
if ( ($request->getModule()!='admin') 
	 && ($request->isRedirectedLang()) 
   )
{
	if( !($request->getLang()) )
	{
	//jezeli nie ma ustawionego jezyka, to ustaw jezyk z sesji.
		$request->setLang($session->getLanguageTemp());
		$request->redirectLang();  
	}
	else 
	if (strcmp($request->getLang(),$session->getLanguageTemp()))
	{
		$request->redirectLang();
		//zapisz jezyk do sesji
		$session->setLanguageTemp($request->getLang());
	}
}*/
// ZaЕ‚aduj fron controller
require_once 'Cube/Controller/Front.php';
$front = Cube_Controller_Front::getInstance();
$front->dispatch();


}
catch (Exception $e)
{
	echo $e->getMessage();
}
					 
?>
