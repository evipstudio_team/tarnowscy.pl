<?php

	$dbSettings = array(
		'host'   => 'localhost',
		'name'   => 'tarnowscy_tarn',
		'user'   => 'tarnowscy_tarn',
		'pass'   => 'rzF51Hdm'
	);
	
	//jezyki widoczne w Konfiguracja - domyslny jezyk
	$systemLanguages = array('pl' => 'Polski');	
							 
	//jezyki widoczne w Modułu - dodaj jezyk
	$selectLanguages=array
		(	
			array('name'=>'Angielski', 'short_name'=>'en'),
			array('name'=>'Niemiecki', 'short_name'=>'ge'),
			array('name'=>'Francuski', 'short_name'=>'fr')
		);
	//nazwy modułów które nie podlegają jezykowosci		
	$ModulesNotLang= array ('modules','settings','admins','users'); 
	//$systemTemplates = array('default', 'userLogin');
	$systemTemplates = array('default');
	# dla wiekszej ilości szablonow, np: $systemTemplates = array('default', 'blue', 'red');	
							 
# domyślny startowy moduł w panelu administracyjnym. Należy pamiętać aby moduł istniał i miał uprawnienia 'administrator', 
# tak aby każdemu mógł się na starcie wyśweitlić.
define('DEFAULT_ADMIN_MODULE', 'articles');

# galeria zaawansowana. ściezka do katalogu w ktorym beda trzymane zdjęcia. PAMIETAJ o chmod 777 na ten katalog.
# UWAGA! katalog ten musi zawierać katalog 'temp' z chmod 777. Reszta katalogów bedzie tworzona automatycznie wg kategori/podkategorii.							 
define('ADVGALLERY_DIR', 'public/advgallery/');
# galeria zaawansowana. maksymalna szerokość miniaturki.
define('ADVGALLERY_THUMB_WIDTH', 120); 
# galeria zaawansowana. maksymalna wysokość miniaturki.
define('ADVGALLERY_THUMB_HEIGHT', 90); 
# galeria zaawansowana. maksymalna szerokość miniaturki małej.
define('ADVGALLERY_THUMB2_WIDTH', 120); 
# galeria zaawansowana. maksymalna wysokość miniaturki małej.
define('ADVGALLERY_THUMB2_HEIGHT', 90);

define('KONTAKT_REDIRECT_URL', 'bilety_miesieczne-t1_6.html');
define('DIR_BANNERS', 'public/banner/');
define('DIR_BANNERS_TEMP', 'public/banner/temp/');

# newsy zaawansowane. ściezka do katalogu w ktorym beda trzymane duze zdjęcia. PAMIETAJ o chmod 777 na ten katalog.							 
define('ADVNEWS_DIR_BIG_PHOTOS', 'public/news/');
# newsy zaawansowane. ściezka do katalogu w ktorym beda trzymane miniaturki. PAMIETAJ o chmod 777 na ten katalog.	  
define('ADVNEWS_DIR_THUMBS', 'public/news/thumbs/'); 
# newsy zaawansowane. maksymalna szerokość miniaturki.
define('ADVNEWS_THUMB_WIDTH', 120); 
# newsy zaawansowane. maksymalna wysokość miniaturki.
define('ADVNEWS_THUMB_HEIGHT', 90); 

#===================== POD TA KRESKA WKLEJAJ USTAWIENIA DODATKOWYCH MODUŁÓW:

define('MEDIA_DIR', 'public/media/');
define('MEDIA_DIR_TEMP', 'public/media/temp/');
							 					 
define('CAREER_DIR', 'public/career/');
define('CAREER_DIR_TEMP', 'public/career/temp/');
?>
