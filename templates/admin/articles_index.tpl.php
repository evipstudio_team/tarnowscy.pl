<?php		

if (isset($this->errors))
{
	echo '<div class="errorBox"><ul>';
	foreach ($this->errors as $error)
	{
		echo "<li>$error</li>";
	}
	echo '</ul></div>';
}

if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';
else
{ 			
echo '<h1>Zarządzaj danymi</h1>

<table cellspacing="0">
<tr>
	<td>
	<form action="admin,articles,search.html" method="post">Wyszukaj podstronę: (numer, tytuł lub autor) <input type="text00" name="search" value="'.$this->search->getSearchString().'" /><input type="submit" name="submit" id="submit" value="SZUKAJ" />
	</form>
	</td>
	</tr>
</table>';

echo '<form action="admin,articles,deleteAll.html" class="in-table" method="post">			
<table cellspacing="0">
			<thead>
				<tr>
					<td class="tocenter"><a href="'.$this->order->getlink('id').'.html" >ID'.$this->order->getImageStatus('id').'</a></td>
					<td class="tocenter">Usuń grupowo</td>	
					<td><a href="'.$this->order->getlink('title').'.html" >Tytuł'.$this->order->getImageStatus('title').'</a></td>
					<td class="tocenter"><a href="'.$this->order->getlink('author').'.html" >AUTOR'.$this->order->getImageStatus('author').'</a></td>
					<td class="tocenter"><a href="'.$this->order->getlink('date').'.html" >Data dodania'.$this->order->getImageStatus('date').'</a></td>
					<td class="toright">Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	$rows = (array)$this->rows;
	//print_r($rows);
	if (count($rows) < 1) echo '<tr><td colspan="6">Nieodnaleziono żadnych podstron w bazie.</td></tr>';
	else {	
		foreach($rows as $r) 
		{
			$class = getTableClass();	
			//$t_img = '';
			//if ($r['filename'] != '') $t_img = '<img src="public/advnews/photo/thumbs/'.$r['filename'].'" alt="" />';	
			echo '			<tr'.$class.'>
					<td class="tocenter">'.$r['id'].'</td>
					<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td>
					<td>'.$r['title'].'</td>
					<td class="tocenter">'.$r['author'].'</td>
					<td class="tocenter">'.date('d.m.Y H:i', $r['add_date']).'</td>
					<td class="toright" width="120"><a href="admin,articles,edit,id_'.$r['id'].'.html" class="edit" title="edytuj"></a><a href="admin,articles,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" class="delete" title="usuń"></a></td>
				</tr>';
		}		
	}

	echo '</tbody></table>';
	if (count($rows) > 0) 
	{
		echo '<input type="submit" value="USUŃ ZAZNACZONE" class="red2" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" /></form>';
	}	
}	
?>
