<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
	
	echo '<h1>Zarządzaj danymi</h1>';
	
	echo '<table id="podzial" cellspacing="0">
			<tr>
			
			<td id="td_left">';
				
				$mark='mark';
				if( $this->cid>0 )
					$mark=null;
							
				echo '<a href="admin,advgallery,index.html" class="close00'.$mark.'">&laquo; Zobacz wszystkie wpisy &raquo;</a>';
		
		echo '<br clear="all" />'.$this->jsTree.'</td>';
			
			
	echo '<td id="td_right">';
	$path=null;
	foreach ($this->currentPath as $row)
	{
		$path.='<a href="admin,advgallery,index,cid_'.$row['id'].'.html">'.$row['name'].'</a>';
		$path.=' &raquo; ';
	}
	$path=substr($path,0,-2);
	
	echo '<div id="path">Aktywna kategoria: '.$path.'</div>';
	
		
		echo '<h2>Kategorie</h2>
		<form action="admin,advgallery,poscategories,cid_'.$this->cid.'.html" method="post">	
			<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Nazwa</td>
							<td>Ilość wpisów w kategorii</td>
							<td class="tocenter">Pozycja</td>
							<td class="tocenter">Status</td>	
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="6">Nie odnaleziono żadnych kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					
					if ($r['active']) {
						$status = '<span class="green">aktywna</span>';
						$status_action = '<a href="admin,advgallery,deactivecat,id_'.$r['id'].',cid_'.$this->cid.'.html" class="deactive" title="deaktywuj"></a>';
					} else {
						$status = '<span class="red">nieaktywna</span>';
						$status_action = '<a href="admin,advgallery,activecat,id_'.$r['id'].',cid_'.$this->cid.'.html" class="active" title="aktywuj"></a>';				
					} 				
					if ($r['id']==1) 
					{
						//jezeli korzeń to usun opcje: usun i deaktywuj i edytuj.	
						echo '			<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<td>'.$r['amount'].'</td>
							<td class="tocenter">'.null.'</td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright" width="120"></td>
						</tr>';
					}
					else
					{ 				
						echo '<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<td>'.$r['amount'].'</td>
							<td class="tocenter"><input type="text" style="width: 40px; text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright" width="120">'.$status_action.'<a href="admin,advgallery,editcat,id_'.$r['id'].',cid_'.$this->cid.'.html" class="edit" title="edytuj"></a><a href="admin,advgallery,deletecat,id_'.$r['id'].',cid_'.$this->cid.'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
						</tr>';
					}
				}		
			}
			
			echo '</tbody></table>';
			
			echo'
			<a href="admin,advgallery,addcat,cid_'.$this->cid.'.html" class="add2">DODAJ KATEGORIE W AKTYWNEJ KATEGORII</a>
			<a href="admin,advgallery,photos,cid_'.$this->cid.'.html" class="add3">DODAJ WPIS W AKTYWNEJ KATEGORII</a>';
			if (count($rows) > 0)
			echo '<input type="submit" value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
			
			echo '</form><br clear="all" /><br /><br />';
		
		echo '
		<h2>Zarządzaj wpisami w aktywnej kategorii</h2>
		<table cellspacing="0">
		<tr><td>
		<form action="admin,advgallery,search,cid_'.$this->cid.'.html" method="post">Wyszukaj pliki (id , tytuł, nazwa pliku): <input type="text00" name="search" value="'.$this->search->getSearchString().'" style="width:150px;" /><input type="submit" name="submit" id="submit" value="SZUKAJ" />
		</form>
		</td></tr></table';
		
		$colspan=9;
		echo'
		
		<form action="admin,advgallery,posdeleteMark,cid_'.$this->cid.'.html" method="post">	
		<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter"><a href="'.$this->order->getlink('id').',cid_'.$this->cid.'.html" >ID'.$this->order->getImageStatus('id').'</a></td>
							<td class="tocenter">Usuń grupowo</td>	
							<td><a href="'.$this->order->getlink('title').',cid_'.$this->cid.'.html" >Tytuł'.$this->order->getImageStatus('title').'</a></td>
							<td><a href="'.$this->order->getlink('filename').',cid_'.$this->cid.'.html" >Nazwa pliku'.$this->order->getImageStatus('filename').'</a></td>
							<td class="tocenter"><a href="'.$this->order->getlink('date').',cid_'.$this->cid.'.html" >Data dodania'.$this->order->getImageStatus('date').'</a></td>
							<td class="tocenter">Kategoria</td>
							<td class="tocenter">Zdjęcie</td>';
							
							if( $this->cid>0 )
							{
								echo '<td>Główne zdjęcie</td>';
								$colspan+=1;
							}
						echo '<td class="tocenter">Pozycja</td>			
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->photos;

			if (count($rows) < 1) {
				echo '<tr><td colspan="'.$colspan.'">Nie odnaleziono żadnych zdjęć w wybranej kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}

					if ($r['main']==1) 
						$main = '<b>TAK</b>';
					else 
						$main = '<b>NIE</b><br /><a href="admin,advgallery,setmainPhoto,id_'.$r['id'].',cid_'.$this->cid.'.html">Zdjęcie główne</a>';
					
						
					$t_img = '';
	if ($r['filename'] != '') $t_img = '<img src="public/advgallery/photo/mini/'.$r['filename'].'" alt="" />';
						
					echo '<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td>
							<td>'.$r['title'].'</td>
							<td>'.$r['filename'].'</td>
							<td class="tocenter">'.date('d.m.Y H:i', $r['add_date']).'</td>
							<td class="tocenter">'.$r['cat'].'</td>
							<td class="tocenter thumbs_photo">'.$t_img.'</td>';
						if ($this->cid > 0) 
						{
							echo '<td>'.$main.'</td>';
							echo '<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>';
						}
						else
							echo '<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos_all'].'" /></td>';
									
						echo' 
							<td class="toright" width="120"><a href="admin,advgallery,edit,id_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,advgallery,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>';
		if (count($rows) > 0)	
		{
			echo '<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" class="red2" />';
			echo '<input type="submit" name="pos" value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
		}
			
			
			echo '</form><br /><br /><br /><br />';
			
		echo '</td></tr></table>';
	}

?>
