<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else 
	{
	
echo '<h1>Dodaj menu</h1>';

echo '<table id="podzial" cellspacing="0">
			<tr>
				<td id="td_left"><a href="admin,menus,index.html" class="close00">&laquo; Zobacz wszystkie wpisy &raquo;</a><br clear="all" />'.$this->jsTree.'</td>';
				
echo '
<td id="td_right">
<form id="add_cat" action="admin,menus,addcat.html" onSubmit="return isValid()" method="post" enctype="multipart/form-data">
<table cellspacing="0" id="normal">
<tr><td><label for="cid"><strong>Menu:</strong></label></td><td><select name="cid"><option value="0">- wybierz menu -</option>'.$this->categoriesList.'</select></td></tr>
<tr><td><label for="name"><strong>Nazwa:</strong></label></td><td><input type="text" name="name" id="name" value="'.$this->name.'" /></td></tr>
<!-- <tr><td><label for="label"><strong>Label:</strong></label></td><td><input type="text" name="label" id="label" value="'.$this->label.'" /></td></tr> -->
<tr>
<td><label for="href"><strong>Adres URL:</strong></label></td>
<td><select name="subpage" id="subpage" onChange="SwitchSubpage(this.value);"><option value="0"> -- WYBIERZ PODSTRONĘ -- </option>'.$this->subpage.'</select><br />
<select name="advnews" id="advnews" onChange="SwitchAdvnews(this.value);"><option value="0">- WYBIERZ AKTUALNOŚĆ -</option>'.$this->advnews.'</select><br />
<select name="advgallery" id="advgallery" onChange="SwitchAdvgallery(this.value);"><option value="0">- WYBIERZ GALERIĘ -</option>'.$this->advgallery.'</select><br />
<input type="text" name="href" id="href" value="'.$this->href.'" />
<input type="text" name="href_readonly" id="href_readonly" value="" style="display:none;" readonly/>
<input type="button" value="Resetuj pola" onClick="SwitchReset()" class="red" /></td>
</tr>
<tr><td><label for="language"><strong>Język:</strong></label></td><td><select id="language" name="language">'.getSelectLanguages('pl').'</select></td></tr>
<tr><td><label for="extra">Dodatkowe info:</label></td><td><input type="text" name="extra" id="extra" value="'.$this->extra.'" /></td></tr>';
		 /*if($this->logged)
			echo '<tr><td><label for="logged">Tylko zalogowani:</label></td><td><input type="checkbox" id="logged" name="logged" value="1" checked="checked" /></td></tr>';	
		 else
			echo '<tr><td><label for="logged">Tylko zalogowani:</label></td><td><input type="checkbox" id="logged" name="logged" value="1" /></td></tr>';*/
		
		if($this->target)
			echo '<tr><td><label for="target">Nowe okno:</label></td><td><input type="checkbox" name="target" id="target" value="1" checked="checked" /></td></tr>';	
		 else
			echo '<tr><td><label for="target">Nowe okno:</label></td><td><input type="checkbox" name="target" id="target" value="1" /></td></tr>';
echo '
<tr><td><label for="file1">Obrazek:</label></td><td><input type="file" name="file1" id="file1" /><br /> </td></tr>';
		
echo '
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>

</td></tr></table>';		
	}

?>
