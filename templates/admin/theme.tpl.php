﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>VipoCMS</title>
	<link rel="stylesheet" type="text/css" href="templates/admin/styles/style.css" />	
<?php
 echo '<script type="text/javascript" src="templates/admin/js/script.js"></script>
		  <script type="text/javascript" src="templates/admin/tree/dtree.js"></script>
		  <link rel="stylesheet" type="text/css" href="templates/admin/tree/dtree.css" />';
 if ($this->_config->get('wysiwyg_'.$this->_session->getUsername()) == '1') 
 {
  echo '<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_start.js"></script>';
 } 
?>	
<!-- /TinyMCE -->

</head>
<body>
<!--wrap-->
<div id="wrap">


	<!--header-->
	<div id="header">
	
		<span id="naglowek">
		Panel administracyjny<br />
		<span>VipoCMS</span>
		</span>
		
		<span id="user">
		zalogowany jako: <span><?php echo $this->_session->getUsername(); ?></span> | <a href="admin,auth,logout.html">wyloguj</a><br />
		<small>ostatnia akcja: <?php echo date("Y-m-d H:i"); ?></small>
		</span>
		
	</div>
	<!--header-->
	
	
	
	

	<!--menu-->
	<div id="menu">
	<?php 
		$currentModule = $this->_request->getController();
		//$modules = (array)$this->_getModules(null, null, 'name');
		$modules = (array)$this->_getModules();
		foreach ($modules as $module) 
			{
				if ($module['name'] == $currentModule) 
				echo '<a class="activemenu" href="admin,'.$module['name'].'.html">'.runDefined('_MODULE_'.$module['name'].'_').'</a>';
				else 
				echo '<a href="admin,'.$module['name'].'.html">'.runDefined('_MODULE_'.$module['name'].'_').'</a>';
			}
	?>
	</div>
	<!--menu-->
	
	
	
	<!--submenu-->
	<div id="submenu">
	<?php 
		$currentAction = $this->_request->getAction();
		$actions = (array)$this->_getActions($currentModule);
		foreach ($actions as $action) 
		{
			if ($action == $currentAction) echo '<a href="admin,'.$currentModule.','.$action.'.html" class="activemenu">'.runDefined('_ACTION_'.$action.'_').'</a>';
			else echo '<a href="admin,'.$currentModule.','.$action.'.html">'.runDefined('_ACTION_'.$action.'_').'</a>';
		}
	?>
	</div>
	<!--submenu-->
	
	
	
	
	
	<div id="maintab"><?php echo $controller; ?></div>
	
	
	
<div id="footer">
<a href="http://www.evipstudio.pl/dokumentacja/VipoCMS.pdf" target="_blank" class="vipo"><img src="templates/admin/images/vipocms.png" alt="VipoCMS" /></a>
<span>Wszelkie prawa zastrzeżone. Powered by <a href="http://www.evipstudio.pl" target="_blank" title="Projektujemy i tworzymy strony www, strony internetowe. Projekty graficzne stron www. Systemy zarządzania treścią CMS. Hosting. Biuro - Lubin - Galeria Cuprum.">evipstudio.pl</a></span>
</div>
	
</div>
<!--wrap-->


</body>
</html>
