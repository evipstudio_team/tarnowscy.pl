<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$logged = null;
		if ($this->row['logged'] == '1') $logged = ' checked="checked"';
		
		
echo '
<form action="admin,articles,insert.html" method="post">
<h1>Dodaj podstronę</h1>
<table cellspacing="0" id="normal">
<tr><td><label for="author"><strong>Autor:</strong></label></td><td><input type="text" name="author" id="author" value="'.$this->row['author'].'" /></td></tr>
<!-- <tr><td><label for="logged">Tylko zalogowani:</label></td><td><input type="checkbox" name="logged" value="1"'.$logged.' /></td></tr> -->
<tr><td><label for="title"><strong>Tytuł:</strong></label></td><td><input type="text" name="title" id="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="meta_title">Meta title:</label></td><td><input type="text" name="meta_title" value="'.$this->row['meta_title'].'" /></td></tr>
<tr><td><label for="contents"><span class="b">Treść:</span></label></td><td><textarea name="contents" id="contents" class="tiny" rows="28">'.$this->row['contents'].'</textarea></td></tr>
<tr><td><label for="gallery_cid">Powiąż z kat. galerii:</label></td><td><select name="gallery_cid"><option value="0">brak wiązania</option>'.$this->categoriesList.'</select></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>	
</table>
</form>';
}
?>
