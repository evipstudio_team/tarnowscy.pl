<?php

	$rows = (array)$this->rows;

	echo '<h1>Zarządzaj administratorami</h1>
		<table id="tab-admins" cellspacing="0">
			<thead>
				<tr>
					<td class="tocenter">ID</td>
					<td class="tocenter">Login</td>
					<td>Email</td>
					<td class="tocenter">Rola</td>
					<td class="tocenter">Status konta</td>
					<td class="toright">Akcja</td>
				</tr>
			</thead>
			<tbody>';

		foreach ($rows as $row) 
		{
			if ($this->_session->getRole() == 'administrator') 
			{
				if ($row['role'] == 'root')
					continue;
				
			}
			if ($row['active']) {
				$status = '<span class="green">aktywne</span>';
				$act = ' title="deaktywuj" class="deactive">';
				$sact = 'deactive';
			}	
			else {
				$status = '<span class="red">nieaktywne</span>';
				$act = ' title="aktywuj" class="active">';
				$sact = 'active';
			}	
			
			$role = '<form action="admin,admins,role,id_'.$row['id'].'.html" method="post">
				<select name="role">';
			switch ($row['role'])
			{
				case 'root': $role .= '<option value="root" selected="selected">root</option>
										<option value="administrator">administrator</option>
										'; 
							 break;
				case 'administrator': $role .= '<option value="root">root</option>
										<option value="administrator" selected="selected">administrator</option>
										'; 
									 break;
			}
			$role .= '</select> <input type="submit" value="AKTUALIZUJ" /></form>';
			
			$act_edit = '<a href="admin,admins,edit,id_'.$row['id'].'.html" class="edit" title="edytuj"></a>';
			
			$act_access = '<a href="admin,admins,access,id_'.$row['id'].'.html" title="uprawnienia" class="access"></a>';
			
			$act_passwd_id = '<a href="admin,admins,passwdid,id_'.$row['id'].'.html" title="zmień hasło" class="password"></a>';
			
			if ($row['login'] == $this->_session->getUsername()) {
				$act = '';
				$role = $row['role'];
				$delu = '';
			}	
			else {
				$act = '<a href="admin,admins,'.$sact.',id_'.$row['id'].'.html"'.$act.'</a>';
				$delu = '<a href="admin,admins,delete,id_'.$row['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\'") class="delete" title="usuń"></a>';
			}	
			//oryginal
			//if ($row['role'] == 'root' && $this->_session->getRole() == 'administrator') 
			//	$role = $row['role'];
			
			//
			if (!($this->_session->getRole() == 'root') )
				$role = $row['role'];
				
			$class = getTableClass();		
			echo '<tr'.$class.'>
					<td class="tocenter">'.$row['id'].'</td>
					<td class="tocenter">'.$row['login'].'</td>
					<td><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></td>
					<td class="tocenter">'.$role.'</td>
					<td class="tocenter">'.$status.'</td>
					<td class="toright">'.$delu.$act.$act_edit.$act_passwd_id.$act_access.'</td>
				</tr>';
		}

	echo '</tbody></table>';			
?>		
