<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {

		echo '
		
<h1>Edytuj kategorię</h1>
<form id="add_cat" action="admin,banner,editcat,id_'.$this->id.'.html" method="post">';
		
		
		if ($this->row['random'] == 1) $random = ' checked="checked"';
		else $random = '';
				
		echo '

<table cellspacing="0" id="normal">
<tr><td><label for="name"><strong>Nazwa:</strong></label></td><td><input type="text" name="name" value="'.$this->row['name'].'" /></td></tr>
<tr><td><label for="note">Notatka:</label></td><td><input type="text" name="note" value="'.$this->row['note'].'" /></td></tr>
<tr><td><label for="view_amount">Ilość wyśw.:</label></td><td><input class="short" type="text" name="view_amount" value="'.((int)$this->row['view_amount']).'" /></td></tr>
<tr><td><label for="random">Losowo:</label></td><td><input type="checkbox" class="check" name="random"'.$random.' value="1" /></td></tr>		
<tr><td colspan="2">
<p>* "Ilość wyświetlanych" - aby wyświetlać wszystkie zostaw 0<br />
* "Notatka" może posłużyć jako opis wymiarów<br />
* Pola <b>pogrubione</b> są wymagane.</p></td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="AKTUALIZUJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr></table>
</form>';
	}

?>
