<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			
		
echo '
<h1>Dodaj wpis</h1>';

echo '<table id="podzial" cellspacing="0">
			<tr>
				<td id="td_left"><a href="admin,advnews,index.html" class="close00">&laquo; Zobacz wszystkie wpisy &raquo;</a><br clear="all" />'.$this->jsTree.'</td>';	
		
		
		
		echo '<td id="td_right">';

echo '
<form action="admin,advnews,insert.html" method="post" enctype="multipart/form-data">
<table cellspacing="0" id="normal">
<tr style="display:none;"><td><label for="language"><strong>Język:</strong></label></td><td><select name="language">'.getSelectLanguages('pl').'</select></td></tr>
<tr><td><label for="author"><strong>Autor:</strong></label></td><td><input type="text" name="author" value="'.$this->row['author'].'" /></td></tr>
<tr><td><label for="title"><strong>Tytuł:</strong></label></td><td><input type="text" name="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="contents"><strong>Treść:</strong></label></td><td><textarea class="tiny" name="contents" rows="28">'.$this->row['contents'].'</textarea></td></tr>
<tr><td><label for="add_date"><strong>Data:</strong></label></td><td><input type="text" name="add_date" value="'.$this->row['add_date'].'" id="CalendarInput"/>';
	
//skrypt z kalendarzem
//parametr funkcji być równy id inputu
pickup_calendar('CalendarInput');
//END skrypt z kalendarzem
echo '</td></tr>';
	
echo '<tr><td><label for="gallery_cid">Powiąż z kat. galerii:</label></td><td><select name="gallery_cid"><option value="0">brak wiązania</option>'.$this->categoriesList.'</select></td></tr>';


				foreach ((array)$this->cats as $c)
				{
					$checked=NULL;
					if ( ($c['id'] == $this->cid) || (($this->cid == 0)&&($c['id'] == 1)) )
						$checked='checked="checked"';
					if ($c['id'] == 1)  
						echo '<tr><td><strong>KATEGORIE:</strong></td><td><input type="checkbox" name="cat_value[]" value="1_1" id="1_1" '.$checked.' /> <input type="hidden" class="hidden" name="cat_id[]" value="1" /><label for="1_1">Główna</label><br />';
					else	
						echo '<input type="checkbox" name="cat_value[]" value="1_'.$c['id'].'"id="'.$c['id'].'" '.$checked.' /> <input type="hidden" name="cat_id[]" value="'.$c['id'].'" /> <label for="'.$c['id'].'">'.$c['name'].'</label><br />';
				}

echo '</td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>

</td></tr></table>';
}
?>
