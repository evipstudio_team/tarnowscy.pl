<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$logged = null;
		if ($this->row['logged'] == '1') $logged = ' checked="checked"';
	
echo '
<form action="admin,articlestoc,add.html" method="post">
<h1>Dodaj blok informacyjny</h1>
<table cellspacing="0" id="normal">
<tr><td><label for="language"><strong>Język:</strong></label></td><td><select name="language">'.getSelectLanguages('pl').'</select></td></tr>
<tr><td><label for="name"><strong>Nazwa:</span></strong></td><td><input type="text" name="name" value="'.$this->row['name'].'" /></td></tr>
<tr><td><label for="logged">Tylko zalogowani:</label></td><td><input type="checkbox" name="logged" value="1"'.$logged.' /></td></tr>
<tr><td><label for="contents"><strong>Treść:</strong></label></td><td><textarea name="contents" class="tiny" id="contents" rows="28">'.$this->row['contents'].'</textarea></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>';
}
?>
