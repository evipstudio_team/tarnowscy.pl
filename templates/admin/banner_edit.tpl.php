<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		echo '
		
<h1>Edytuj wpis</h1>
<form id="add_photo" action="admin,banner,edit,id_'.$this->id.'.html" method="post">';
				
		echo '
		
<table cellspacing="0" id="normal">
<tr><td><label for="cid"><strong>Kategoria:</strong></label></td><td><select name="cid"><option value="0">- WYBIERZ KATEGORIĘ -</option>'.$this->categoriesList.'</select></td></tr>';
		
		$ext = explode('.', $this->row['filename']);
		$ext = strtolower($ext[count($ext)-1]);
		
		if ($ext == 'swf') {
			
			echo '
			<tr><td colspan="2">
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'.$this->row['width'].'" height="'.$this->row['height'].'" id="'.$this->row['filename'].'" align="middle">
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="allowFullScreen" value="false" />
			<param name="movie" value="'.DIR_BANNERS.$this->row['filename'].'" />
			<param name="quality" value="high" />
			<param name="wmode" value="transparent">
			<param name="bgcolor" value="#ffffff" />	
			<embed src="'.DIR_BANNERS.$this->row['filename'].'" wmode="transparent" quality="high" bgcolor="#ffffff" width="'.$this->row['width'].'" height="'.$this->row['height'].'" name="top_baner" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
			</object>
			</td></tr>';
		
		} else echo '<tr><td colspan="2"><img src="'.DIR_BANNERS.$this->row['filename'].'" alt="" /></td></tr>';
		
		echo '<tr><td><label for="start"><strong>Od:</strong></label></td><td>
		<select name="od_day" style="width: 60px;">';
		
		for ($i = 1; $i <= 31; $i++)
		{
			if ($this->row['od_day'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>
		<select name="od_month" style="width: 60px;">';
		
		for ($i = 1; $i <= 12; $i++)
		{
			if ($this->row['od_month'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>
		<select name="od_year" style="width: 80px;">';
		
		for ($i = (date('Y')-1); $i <= (date('Y')+5); $i++)
		{
			if ($this->row['od_year'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>	
		
		<select name="od_hour" style="width: 60px; margin-left: 30px;">';
		
		for ($i = 0; $i <= 23; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['od_hour'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		<select name="od_min" style="width: 60px;">';
		
		for ($i = 0; $i <= 59; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['od_min'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		</td></tr>

		<tr><td><label for="end"><strong>Do:</strong></label></td><td>
		<select name="do_day" style="width: 60px;">';
		
		for ($i = 1; $i <= 31; $i++)
		{
			if ($this->row['do_day'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>
		<select name="do_month" style="width: 60px;">';
		
		for ($i = 1; $i <= 12; $i++)
		{
			if ($this->row['do_month'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>
		<select name="do_year" style="width: 80px;">';
		
		for ($i = (date('Y')-1); $i <= (date('Y')+5); $i++)
		{
			if ($this->row['do_year'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>	
		
		<select name="do_hour" style="width: 60px; margin-left: 30px;">';
		
		for ($i = 0; $i <= 23; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['do_hour'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		<select name="do_min" style="width: 60px;">';
		
		for ($i = 0; $i <= 59; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['do_min'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		</td></tr>		
		
<tr><td><label for="www">Odnośnik (http://):</label></td><td><input type="text" name="www" value="'.$this->row['www'].'" /></td></tr>
<tr><td><label for="title">Tytuł:</label></td><td><input type="text" name="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="width">Szerokość:</label></td><td><input type="text" class="short" name="width" value="'.$this->row['width'].'" /> px</td></tr>
<tr><td><label for="height">Wysokość:</label></td><td><input type="text" class="short"  name="height" value="'.$this->row['height'].'" /> px</td></tr>
<tr><td colspan="2"><p>Pola "Wysokość" i "Długość" wypełnij w przypadku bannera FLASH aby określić atrybuty width i height tagu OBJECT<br />Pola <b>pogrubione</b> są wymagane.</p></td></tr>
<tr><td><label for="description">Opis:</label></td><td><small>(pole opis wypełniamy tylko w przypadku wyboru kategorii "Główna Pasek")</small><br /><textarea name="description" class="tiny">'.$this->row['description'].'</textarea></td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="AKTUALIZUJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>';
	}
 
?>
