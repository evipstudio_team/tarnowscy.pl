<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$random = null;
		if ($this->row['random'] == 1) $random = ' checked="checked"';
		
		echo '

<h1>Dodaj kategorię</h1>
<form id="add_cat" action="admin,banner,addcat.html" method="post">

<table cellspacing="0" id="normal">
<tr><td><label for="name"><strong>Nazwa:</strong></label></td><td><input type="text" name="name" value="'.$this->row['name'].'" /></td></tr>
<tr><td><label for="note">Notatka:</label></td><td><input type="text" name="note" value="'.$this->row['note'].'" /></td></tr>
<tr><td><label for="view_amount">Ilość wyśw.:</label></td><td><input class="short" type="text" name="view_amount" value="'.((int)$this->row['view_amount']).'" /></td></tr>
<tr><td><label for="random">Losowo:</label></td><td><input type="checkbox" class="check" name="random"'.$random.' value="1" /></td></tr>
<tr><td colspan="2">
<p>* "Ilość wyświetlanych" - aby wyświetlać wszystkie zostaw 0<br />
* "Notatka" może posłużyć jako opis wymiarów<br />
* Pola <b>pogrubione</b> są wymagane.</p></td></tr>
<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form><br /><br />';

			echo '
			

<h2>Zarządzaj kategoriami</h2>
<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Nazwa</td>
							<td>Notatka</td>
							<td class="tocenter">Ilość wyświetlanych</td>
							<td class="tocenter">Wyświetlaj losowo</td>
							<td class="tocenter">Status</td>	
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="7">Nie odnaleziono żadnych kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					$class = getTableClass();
					
					if ($r['active']) {
						$status = '<span class="green">aktywna';
						$status_action = '<a href="admin,banner,deactive,id_'.$r['id'].'.html" title="deaktywuj" class="deactive"></a>';
					} else {
						$status = '<span class="red">nieaktywna';
						$status_action = '<a href="admin,banner,active,id_'.$r['id'].'.html" title="aktywuj" class="active"></a>';				
					} 				
					
					$losowo = 'nie'; 
					if ($r['random'] == 1) $losowo = 'tak'; 
					 				
					echo '			<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<td>'.$r['note'].'</td>
							<td class="tocenter">'.$r['view_amount'].'</td>
							<td class="tocenter">'.$losowo.'</td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright">'.$status_action.'<a href="admin,banner,editcat,id_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,banner,deletecat,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>';
	}

?>
