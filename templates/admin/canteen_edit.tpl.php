<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
	
echo '<h1>Edytuj apartament</h1>';

echo '<table id="podzial" cellspacing="0">
			<tr>
			
			<td id="td_left">';
			
	
		$array_json=$this->array_json;
	require('jscripts/calendarEvent/calendar.php');

		
echo '
</td>
<td id="td_right">

<form action="admin,canteen,edit,id_'.$this->row['id'].'.html" method="post">
<table cellspacing="0" id="normal">
<tr><td><label for="title"><strong>Tytuł:</strong></label></td><td><input type="text" name="title" id="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="city">Miasto:</label></td><td><input type="text" name="city" id="city" value="'.$this->row['city'].'" /></td></tr>
<tr><td><label for="street">Ulica:</label></td><td><input type="text" name="street" id="street" value="'.$this->row['street'].'" /></td></tr>
<tr><td><label for="zip_code">Kod pocztowy:</label></td><td><input type="text" name="zip_code" id="zip_code" value="'.$this->row['zip_code'].'" /></td></tr>
<tr><td><label for="contents"><strong>Treść:</strong></label></td><td><textarea name="contents" id="contents" class="tiny" rows="28">'.$this->row['contents'].'</textarea></td></tr>
<tr><td><label for="google_maps"><span class="b">Mapka:</span></label></td><td><textarea name="google_maps" id="google_maps" rows="10" cols="60">'.$this->row['google_maps'].'</textarea></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="hidden" name="id" value="'.$this->row['id'].'" /><input type="submit" name="submit" id="submit" value="AKTUALIZUJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form><br /><br /><br />';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														OKRESY DAT
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


echo '
<h2>Dodaj datę</h2>
<form action="admin,canteen,addDatePeriod,id_'.$this->row['id'].'.html" method="post">
<table cellspacing="0" id="normal">
<tr><td><label for="start_date"><strong>Data początku:</strong></label></td><td><input type="text" name="start_date" value="'.date('d-m-Y', $this->start_date).'" id="StartInput"/>';
//skrypt z kalendarzem
//parametr funkcji być równy id inputu
pickup_calendar('StartInput');
//END skrypt z kalendarzem
echo '</td></tr>';
echo '<tr><td><label for="end_date"><strong>Data końca:</strong></label></td><td><input type="text" name="end_date" value="'.date('d-m-Y', $this->end_date).'" id="EndInput"/>';
//skrypt z kalendarzem
//parametr funkcji być równy id inputu
pickup_calendar('EndInput');
//END skrypt z kalendarzem
echo '</td></tr>
<tr><td><label for="overwrite">Nadpisać:</label></td><td><input type="checkbox" name="overwrite" id="overwrite" value="1" /></td></tr>
<tr><td colspan="2"><input type="hidden" name="id" value="'.$this->row['id'].'" /><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form><br />';

echo '<h2>Okresy dat</h2>
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">ID</td>
						<td class="tocenter">Data początku</td>
						<td class="tocenter">Data końca</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->dates;
		
		if (count($rows) < 1) echo '<tr><td colspan="4">Brak dat.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				echo '<tr'.$class.'>
						<td class="tocenter">'.$r['id'].'</td>
						<td class="tocenter">'.date('d.m.Y', $r['start_date']).'</td>
						<td class="tocenter">'.date('d.m.Y', $r['end_date']).'</td>
						<td class="toright" width="120"><a href="admin,canteen,editDatePeriod,id_'.$this->row['id'].',idDate_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,canteen,deleteDatePeriod,id_'.$this->row['id'].',idDate_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany okres?\')" title="usuń" class="delete"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table><br /><br />';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														ZDJĘCIA
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_photos">
	<h2><img src="templates/admin/images/icon_photo.png" alt="" />Zdjęcia</h2>
	<form action="admin,canteen,addphoto,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>Zdjęcie nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Obrazek:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>Zdjęcie nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Obrazek:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

		  <tr>
			<td>
				<label>Zdjęcie nr. 3:</label><br />
				<label for="title3">Tytuł:</label><input type="text" name="title3" id="title3" value="" /><br />
				<label for="file3"><strong>Obrazek:</strong></label><input type="file" name="file3" id="file3" /><br />
			</td>
					
			<td>
				<label>Zdjęcie nr. 4:</label><br />
				<label for="title4">Tytuł:</label><input type="text" name="title4" id="title4" value="" /><br />
				<label for="file4"><strong>Obrazek:</strong></label><input type="file" name="file4" id="file4" /><br />
			</td>
		  </tr>
		
    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane zdjęcia</h2>
	<form action="admin,canteen,photoPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td class="tocenter">Tytuł/Zdjęcie</td>
						<td class="tocenter">Główne zdjęcie</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->photos;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak zdjęć.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				if ($r['main']==1) {
					$main = '<b>TAK</b>';
				} else {
					$main = '<b>NIE</b><br /><a href="admin,canteen,setmainPhoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html">Zdjęcie główne</a>';
				}
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td class="tocenter">'.$r['title'].'<br /><br /><a href="public/canteen/photo/'.$r['name'].'" rel="lytebox"><img src="public/canteen/photo/mini/'.$r['name'].'" alt="" /></a></td>
						<td class="tocenter">'.$main.'</td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="toright" width="120"><a href="admin,canteen,editphoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,canteen,deletephoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')"  style="float:right;" />';
		echo '	<input type="submit" name="pos"value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form>
	</div>';
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														PDFy
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_pdf">
	<h2><img src="templates/admin/images/icon_pdf.png" alt="" />Pliki PDF</h2>
	<form action="admin,canteen,addpdf,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>PDF nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Plik:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>PDF nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Plik:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane pliki PDF</h2>
	<form action="admin,canteen,pdfPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td>Nazwa</td>
						<td class="tocenter">Plik</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->pdf;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak plików PDF.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td>'.$r['title'].'</td>
						<td class="tocenter"><a href="public/canteen/pdf/'.$r['name'].'" target="_blank" title="podgląd" class="preview"></a></td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="toright" width="120"><a href="admin,canteen,editpdf,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,canteen,deletepdf,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" />';
		echo '	<input type="submit" name="pos"value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form>
	</div>';
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													Dokumenty
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_doc">
	<h2><img src="templates/admin/images/icon_doc.png" alt="" />Dokumenty</h2>
	<form action="admin,canteen,adddoc,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>Dokument nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Plik:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>Dokument nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Plik:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane dokumenty</h2>
	<form action="admin,canteen,docPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td>Nazwa</td>
						<td class="tocenter">Plik</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->doc;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak dokumentów.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td>'.$r['title'].'</td>
						<td class="tocenter"><a href="public/canteen/document/'.$r['name'].'" target="_blank" class="preview" title="podgląd"></a></td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="tocenter" width="120"><a href="admin,canteen,editdoc,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" class="edit" title="edytuj"></a><a href="admin,canteen,deletedoc,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" class="delete" title="usuń"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" />';
		echo '	<input type="submit" name="pos"value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form>
	</div>';


}
echo '</td></tr></table>';

?>
