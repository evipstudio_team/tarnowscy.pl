<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
	
	echo '
<h1>Konfiguracja modułu</h1>
<form action="admin,advgallery,settings.html" method="post">
<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Moduł</td>
							<td>Zdjęć na stronę</td>
							<td class="toright">Status</td>	
						</tr>
					</thead>
					<tbody>';
					
					echo '<tr class="alt">
							<td class="tocenter">0</td>
							<td>Galeria</td>
							<td><input type="text" name="per_page" value="'.$this->row['per_page'].'" /></td>
							<td class="toright">Aktywna</td>
						</tr>';
					
					if ($this->row['advnews'] == 1) {
						$n_status = '<span class="green">aktywna</span>';
						$n_status_action = '<a href="admin,advgallery,deactiveNews.html" title="deaktywuj" class="deactive"></a>';
					} else {
						$n_status = '<span class="red">nieaktywna</span>';
						$n_status_action = '<a href="admin,advgallery,activeNews.html" title="aktywuj" class="active"></a>';				
					}					


					echo '<tr>
							<td class="tocenter">1</td>
							<td>Aktualności</td>
							<td><input type="text" name="per_news" value="'.$this->row['per_news'].'" /></td>
							<td class="toright">'.$n_status.''.$n_status_action.'</td>
						</tr>';


					if ($this->row['articles'] == 1) {
						$a_status = '<span class="green">aktywna</span>';
						$a_status_action = '<a href="admin,advgallery,deactiveArticles.html" title="deaktywuj" class="deactive"></a>';
					} else {
						$a_status = '<span class="red">nieaktywna</span>';
						$a_status_action = '<a href="admin,advgallery,activeArticles.html" title="aktywuj" class="active"></a>';				
					} 

					
					echo '<tr class="alt">
							<td class="tocenter">2</td>
							<td>Podstrony</td>
							<td><input type="text" name="per_articles" value="'.$this->row['per_articles'].'" /></td>
							<td class="toright">'.$a_status.''.$a_status_action.'</td>
						</tr>';
					
					if ($this->row['advnews_categories'] == 1) {
						$a_status = '<span class="green">aktywna</span>';
						$a_status_action = '<a href="admin,advgallery,deactiveCats.html" title="deaktywuj" class="deactive"></a>';
					} else {
						$a_status = '<span class="red">nieaktywna</span>';
						$a_status_action = '<a href="admin,advgallery,activeCats.html" title="aktywuj" class="active"></a>';				
					} 

					
					echo '<tr>
							<td class="tocenter">3</td>
							<td>Kategorie akutalności</td>
							<td><input type="text" name="per_categories" value="'.$this->row['per_categories'].'" /></td>
							<td class="toright">'.$a_status.''.$a_status_action.'</td>
						</tr>';
		
			echo '</tbody></table>
			<input type="submit" name="submit" id="submit" value="ZAPISZ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" />
			</form>';
	}
	
?>
