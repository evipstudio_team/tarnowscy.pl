<?php

echo '
<h1>Zarządzaj modułami</h1>
<form action="admin,modules,pos.html" class="in-table" method="post">	
<table cellspacing="0">
			<thead>
				<tr>
					<td class="tocenter">ID</td>
					<td>Nazwa modułu</td>
					<td>Opis</td>
					<td>Informacje dodatkowe</td>
					<td class="tocenter">Pozycja</td>
					<td class="toright">Akcja</td>
				</tr>
			</thead>
			<tbody>';
	$rows = (array)$this->rows;
	
	if (sizeof($rows) < 1) echo '<tr><td colspan="6">Nie odnaleziono żadnych modułów w bazie.</td></tr>';
	else {
		foreach($rows as $row) 
		{
			$class = getTableClass();
			
			if ($row['active']) 
				$status = '<span class="green">aktywny</span> <a href="admin,modules,deactive,id_'.$row['id'].'.html" title="deaktywuj" class="deactive"></a>';
			else 
				$status = '<span class="red">nieaktywny</span> <a href="admin,modules,active,id_'.$row['id'].'.html" title="aktywuj" class="active"></a>';
			
			/*$access = '<form class="access" action="admin,modules,access,id_'.$row['id'].'.html" method="post">
				<fieldset>
				<select name="access">';
			switch ($row['admin_access'])
			{
				case 'root': $access .= '<option value="root" selected="selected">root</option>
										<option value="administrator">administrator</option>
										<option value="none">none</option>
										'; 
							 break;
				case 'administrator': $access .= '<option value="root">root</option>
										<option value="administrator" selected="selected">administrator</option>
										<option value="none">none</option>
										'; 
									 break;
				default: $access .= '<option value="root">root</option>
										<option value="administrator">administrator</option>
										<option value="none" selected="selected">none</option>
										'; 
			}
			$access .= '</select><br /> <input type="submit" class="sub" value="aktualizuj" /></fieldset></form>';
			*/
			if ($row['name'] != 'modules' && $row['name'] != 'admins' && $row['name'] != 'settings')
				$unaction = '<a href="admin,modules,uninstall,name_'.$row['name'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz odinstalować wybrany element?\')" class="delete" title="odinstaluj"></a>';
			else {
				$status = '<span class="green">aktywny</span>';
				$unaction = ' brak możliwości odinstalowania';
			}		
			echo '<tr'.$class.'>
					<td class="tocenter">'.$row['id'].'</td>
					<td>'.$row['name'].'</td>
					<td>'.$row['description'].'</td>
					<td>'.$row['info'].'</td>
					<td class="tocenter"><input type="text" class="in-table-short" style="width: 40px;text-align: center;" name="pos_'.$row['id'].'" value="'.$row['pos'].'" /></td>
					<td class="toright">'.$status.''.$unaction.'</td>
				</tr>';
		}		
	}

	echo '</tbody></table>';
	if (count($rows) > 0)
				echo '<input type="submit" value="AKTUALIZUJ POZYCJĘ" style="float:right;" /> ';
	echo '</form>';

?>
