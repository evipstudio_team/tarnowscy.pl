<?php

echo '
<h1>Zarządzaj blokami informacyjnymi</h1>
<table cellspacin="0">
			<thead>
				<tr>
					<td class="tocenter">ID</td>
					<td>Nazwa</td>
					<td>Język</td> 
					<td>Link</td>			
					<td class="toright">Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	$rows = (array)$this->rows;
	
	if (count($rows) < 1) echo '<tr><td colspan="5">Nieodnaleziono żadnych tabeli zawartości w bazie.</td></tr>';
	else {	
		foreach($rows as $r) 
		{
			$class = getTableClass();
			
			if ($r['language'] == 'all') $r['language'] = 'wszystkie';
			else $r['language'] = '<img src="languages/flags/'.$r['language'].'.gif">';
			
			echo '<tr'.$class.'>
					<td class="tocenter">'.$r['id'].'<br /><a href="admin,articlestoc,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
					<td>'.$r['name'].'</td>
					<td>'.$r['language'].'</td>
					<td>*-t'.$r['id'].'_NR_ARTYKUŁU.html</td>
					<td class="toright"><a href="admin,articlestoc,edit,id_'.$r['id'].'.html">Edytuj</a></td>
				</tr>';
		}		
	}
	echo '</tbody></table>';	
?>
