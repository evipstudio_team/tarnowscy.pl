<?php 


	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		echo '

<h2>Dodaj banner</h2>

<form id="add_photo" action="admin,banner,insert.html" method="post" enctype="multipart/form-data">
<table cellspacing="0" id="normal">
<tr><td><label for="cid"><strong>Kategoria:</strong></label></td><td><select name="cid"><option value="0">- WYBIERZ KATEGORIĘ -</option>'.$this->categoriesList.'</select></td></tr>
<tr><td><label for="photo">Obrazek:</label></td><td><input type="file" name="photo" id="photo" class="input" /></td></tr>
<tr><td><label for="start"><strong>Od:</strong></label></td><td>
		<select name="od_day" style="width: 50px;">';
		
		for ($i = 1; $i <= 31; $i++)
		{	
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			if ($this->row['od_day'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>
		<select name="od_month" style="width: 50px; margin-left: 4px;">';
		
		for ($i = 1; $i <= 12; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			if ($this->row['od_month'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>
		<select name="od_year" style="width: 80px; margin-left: 4px;">';
		
		for ($i = (date('Y')-1); $i <= (date('Y')+5); $i++)
		{
			if ($this->row['od_year'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>	
		
		<select name="od_hour" style="width: 50px; margin-left: 20px;">';
		
		for ($i = 0; $i <= 23; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['od_hour'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		<select name="od_min" style="width: 50px; margin-left: 4px;">';
		
		for ($i = 0; $i <= 59; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['od_min'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		</td></tr>

<tr><td><label for="end"><strong>Do:</strong></label></td><td>
		<select name="do_day" style="width: 50px;">';
		
		for ($i = 1; $i <= 31; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			if ($this->row['do_day'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>
		<select name="do_month" style="width: 50px; margin-left: 4px;">';
		
		for ($i = 1; $i <= 12; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			if ($this->row['do_month'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>
		<select name="do_year" style="width: 80px; margin-left: 4px;">';
		
		for ($i = (date('Y')-1); $i <= (date('Y')+5); $i++)
		{
			if ($this->row['do_year'] == $i) echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else echo '<option value="'.$i.'">'.$i.'</option>';
		}

		echo '</select>	
		
		<select name="do_hour" style="width: 50px; margin-left: 20px;">';
		
		for ($i = 0; $i <= 23; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['do_hour'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		<select name="do_min" style="width: 50px; margin-left: 4px;">';
		
		for ($i = 0; $i <= 59; $i++)
		{
			if ($i < 10) $ii = '0'.$i;
			else $ii = $i;
			
			if ($this->row['do_min'] == $i) echo '<option value="'.$i.'" selected="selected">'.$ii.'</option>';
			else echo '<option value="'.$i.'">'.$ii.'</option>';
		}

		echo '</select>	
		</td></tr>		
		
						
<tr><td><label for="www">Odnośnik (http://):</label></td><td><input type="text" name="www" value="'.$this->row['www'].'" /></td></tr>
<tr><td><label for="title">Tytuł:</label></td><td><input type="text" name="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="width">Szerokość:</label></td><td><input type="text" class="short" name="width" value="'.$this->row['width'].'" /> px</td></tr>
<tr><td><label for="height">Wysokość:</label></td><td><input type="text" class="short"  name="height" value="'.$this->row['height'].'" /> px</td></tr>';
		
		//echo '<div><label for="description">Opis:<br /> <small>(pole opis wypełniamy tylko w przypadku wyboru kategorii "Główna Pasek")</small></label><textarea name="description">'.$this->row['description'].'</textarea></div>';
		echo '<tr><td colspan="2"><p>Pola "Wysokość" i "Długość" wypełnij w przypadku bannera FLASH aby określić atrybuty width i height tagu OBJECT<br />Pola <b>pogrubione</b> są wymagane.</p></td></tr>
				<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" />
</td></tr>
</table>
</form><br /><br />


<h2>Zarządzaj bannerami</h2>

<form id="show" action="admin,banner.html" method="post">
<table cellspacing="0">
<tr><td><label for="cid"><strong>Wyświetl z kategorii:</strong></label> <select name="cid"><option value="0"> WSZYSTKIE KATEGORIE </option>'.$this->categoriesList.'</select><input type="submit" name="submit" id="submit" value="POKAŻ" /></td></tr>
</table>
</form>

		<table cellspacing="0">
			<thead>
						<tr>
							<td>ID</td>
							<td>Kategoria</td>
							<td>Wyświetlany od-do</td>
							<td>Nazwa pliku</td>		
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			if (count($rows) < 1) {
				echo '<tr><td colspan="5">Nie odnaleziono żadnych bannerów w wybranej kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					$class = getTableClass();	
					echo '<tr'.$class.'>
							<td>'.$r['id'].'</td>
							<td>'.$this->categories[$r['cid']]['name'].'</td>
							<td>'.date('d/m/Y H:i', $r['start']).' - '.date('d/m/Y H:i', $r['end']).'</td>
							<td>'.$r['filename'].'</td>
							<td class="toright" width="120"><a href="admin,banner,edit,id_'.$r['id'].'.html" class="edit" title="edytuj"></a> <a href="admin,banner,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" class="delete" title="usuń"></a></td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>';
	}
?>
