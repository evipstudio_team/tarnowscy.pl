<script language="JavaScript" src='templates/admin/js/tigra_calendar/calendar_eu.js'></script>
<link rel="stylesheet" href='templates/admin/js/tigra_calendar/calendar.css'>

<script language="JavaScript">

var obj = eval(<? echo json_encode($array_json); ?>);

var A_CALTPL = 
{
	'months' : ["Stycze&#324",
				"Luty",
				"Marzec",
				"Kwiecie&#324",
				"Maj",
				"Czerwiec",
				"Lipiec",
				"Sierpie&#324",
				"Wrzesie&#324",
				"Pa&#378dziernik",
				"Listopad",
				"Grudzie&#324"],
	'weekdays' : [	"Nd",
					"Pon",
					"Wt",
					"&#346r",
					"Czw",
					"Pt",
					"Sob"
					],
	'yearscroll': true,
	'weekstart': 1,
	'centyear'  : 70,
	'imgpath' : 'templates/admin/js/tigra_calendar/img/'
}
	
	new tcal(obj, A_CALTPL);
	//new tcal ({obj_tcal
	//	// if referenced by ID then form name is not required
	//	'controlname': 'CalendarInput'
	//}, A_CALTPL);
</script>