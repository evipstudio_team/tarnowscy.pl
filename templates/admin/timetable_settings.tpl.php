<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
	
	echo '
<h1>Konfiguracja modułu</h1>
<form action="admin,timetable,settings.html" method="post">
<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Opis</td>
							<td>Ilość</td>
							<td class="toright">Status</td>	
						</tr>
					</thead>
					<tbody>';
					
					echo '<tr class="alt">
							<td class="tocenter">0</td>
							<td>Zdjęć na stronę</td>
							<td><input type="text" name="per_page" value="'.$this->row['per_page'].'" /></td>
							<td class="toright">Aktywna</td>
						</tr>';
					
					if ($this->row['advnews_categories'] == 1) 
					{
						$a_status = '<span class="green">aktywna</span>';
						$a_status_action = '<a href="admin,timetable,deactiveCats.html" title="deaktywuj" class="deactive"></a>';
					} else {
						$a_status = '<span class="red">nieaktywna</span>';
						$a_status_action = '<a href="admin,timetable,activeCats.html" title="aktywuj" class="active"></a>';				
					} 

					
					echo '<tr>
							<td class="tocenter">3</td>
							<td>Kategorii na stronę</td>
							<td><input type="text" name="per_categories" value="'.$this->row['per_categories'].'" /></td>
							<td class="toright">'.$a_status.''.$a_status_action.'</td>
						</tr>';
		
			echo '</tbody></table>
			<input type="submit" name="submit" id="submit" value="ZAPISZ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" />
			</form>';
	}
	
?>
