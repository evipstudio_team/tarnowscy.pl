<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {	
		echo '

<form id="add-admin" action="admin,admins,edit,id_'.$this->id.'.html" method="post">
<h1>Edycja danych "'.$this->login.'"</h1>
<table cellspacing="0" id="normal">
<tr><td><label for="name"><strong>Imię:</strong></label></td><td><input name="name" id="name" type="text" value="'.$this->name.'" /></td></tr>
<tr><td><label for="surname"><strong>Nazwisko:</strong></span></label></td><td><input name="surname" id="surname" type="text" value="'.$this->surname.'" /></td></tr>
<tr><td><label for="email"><strong>Email:</strong></label></td><td><input name="email" id="email" type="text" value="'.$this->email.'" /></td>	</tr>
<tr><td><label for="phone">Telefon:</label></td><td><input name="phone" id="phone" type="text" value="'.$this->phone.'" /></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" value="DODAJ" /><input type="reset" value="ANULUJ" /></td></tr>
</table>
</form>';
	}

?>
