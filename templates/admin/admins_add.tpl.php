<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {	
		//print_r($this->row);
		//exit;
		if ($this->_session->getRole() == 'root')
			$allRole = '<option value="root">root</option><option value="administrator" selected="selected">administrator</option>';
		else
			$allRole = '<option value="administrator" selected="selected">administrator</option>';
		
		//oryginal
		/*switch ($this->row['role'])
		{
			case 'root': $allRole = '<option value="root" selected="selected">root</option><option value="administrator">administrator</option>'; break;
			default: $allRole = '<option value="root">root</option><option value="administrator" selected="selected">administrator</option>';
		}*/
	
		echo '
<form id="add-admin" action="admin,admins,add.html" method="post">
<h1>Dodaj administratora</h1>
<table cellspacing="0" id="normal">
<tr><td><label for="login"><strong>Login:</strong></label></td><td><input name="login" id="login" type="text" value="'.$this->row['login'].'" /></td></tr>
<tr><td><label for="name"><strong>Imię:</strong></label></td><td><input name="name" id="name" type="text" value="'.$this->row['name'].'" /></td></tr>
<tr><td><label for="surname"><strong>Nazwisko:</strong></label></td><td><input name="surname" id="surname" type="text" value="'.$this->row['surname'].'" /></td></tr>
<tr><td><label for="pass"><strong>Hasło:</strong></label></td><td><input name="pass" type="password" value="'.$this->row['pass'].'" /></td></tr>
<tr><td><label for="repass"><strong>Powtórz hasło:</strong></label></td><td><input name="repass" type="password" value="'.$this->row['repass'].'" /></td></tr>
<tr><td><label for="role"><strong>Rola:</strong></label></td><td><select name="role">'.$allRole.'</select></td></tr>
<tr><td><label for="email"><strong>Email:</strong></label></td><td><input name="email" id="email" type="text" value="'.$this->row['email'].'" /></td></tr>
<tr><td><label for="phone">Telefon:</label></td><td><input name="phone" id="phone" type="text" value="'.$this->row['phone'].'" /></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="submit" value="DODAJ" /><input type="reset" value="ANULUJ" /></td></tr>
</table>
</form>';
	}

?>
