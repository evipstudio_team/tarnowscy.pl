<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else 
	{
		
echo '<h1>Zarządzaj menu</h1>';

echo '<table id="podzial" cellspacing="0">
			<tr>
				<td id="td_left">';
				
				$mark='mark';
				if( $this->cid>0 )
					$mark=null;
							
				echo '<a href="admin,menus,index.html" class="close00'.$mark.'">&laquo; Zobacz wszystkie menu &raquo;</a>';
		
		echo '<br clear="all" />'.$this->jsTree.'</td>';	

		
		echo '<td id="td_right">';
		$path=null;
		foreach ($this->currentPath as $row)
		{
			$path.='<a href="admin,menus,index,cid_'.$row['id'].'.html">'.$row['name'].'</a>';
			$path.=' &raquo; ';
		}
			$path=substr($path,0,-2);
		
		echo '<div id="path">Aktywne menu: '.$path.'</div>';
		
		
		echo '<h2>Menu</h2>
		<form action="admin,menus,poscategories,cid_'.$this->cid.'.html" method="post">	
			<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Nazwa</td>
							<!--<td>Odnośnik</td>-->
							<td>Adres</td>
							<td>Nowe okno</td>
							<td class="tocenter">Pozycja</td>
							<td class="tocenter">Status</td>	
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="7">Nie odnaleziono żadnych kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					
					if ($r['active']) {
						$status = '<span class="green">aktywna</span>';
						$status_action = '<a href="admin,menus,deactivecat,id_'.$r['id'].',cid_'.$this->cid.'.html" class="deactive" title="deaktywuj"></a>';
					} else {
						$status = '<span class="red">nieaktywna</span>';
						$status_action = '<a href="admin,menus,activecat,id_'.$r['id'].',cid_'.$this->cid.'.html" class="active" title="aktywuj"></a>';				
					} 

					$target=null;
					if ($r['target']== 1)
						$target='tak';
					else if ($r['target']== 0)
						$target='nie';
						
					if ($r['id']==1) 
					{
						//jezeli korzeń to usun opcje: usun i deaktywuj i edytuj.	
						echo '			<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<!--<td>'.$r['label'].'</td>-->
							<td>'.$r['href'].'</td>
							<td>'.null.'</td>
							<td class="tocenter">'.null.'</td>
							<td class="tocenter" width="120">'.$status.'</td>
							<td></td>
						</tr>';
					}
					else
					{ 				
						echo '<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<!--<td>'.$r['label'].'</td>-->
							<td>'.$r['href'].'</td>
							<td>'.$target.'</td>
							<td class="tocenter"><input type="text" class="in-table-short" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright" width="120">'.$status_action.'<a href="admin,menus,editcat,id_'.$r['id'].',cid_'.$this->cid.'.html" title="edytuj" class="edit"></a><a href="admin,menus,deletecat,id_'.$r['id'].',cid_'.$this->cid.'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
						</tr>';
					}
				}		
			}
			echo '</tbody></table>
			<a href="admin,menus,addcat,cid_'.$this->cid.'.html" class="add2" style="float:left;">DODAJ PODMENU W AKTYWNYM MENU</a>
			<input type="submit" value="AKTUALIZUJ POZYCJĘ" style="float:right;" />
			</form>
			</td></tr></table>';			
		
	}

?>
