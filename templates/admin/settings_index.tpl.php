<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			$wysiwyg = null;
			if ($this->row[$this->wys] == '1') $wysiwyg = ' checked="checked"';
			
			if ($this->_session->getRole() == 'administrator') {

echo '
<h1>Konfiguracja</h1>
<form action="admin,settings.html" method="post">
<table cellspacing="0" id="normal">
<tr><td><input type="checkbox" name="wysiwyg" id="wysiwyg" value="1"'.$wysiwyg.' /> <label for="wysiwyg">Używaj edytora WYSIWYG</label></td></tr>
<tr><td><input type="hidden" name="act" value="1" /><input type="submit" name="submit" id="submit" value="ZAPISZ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>';
			} else {
				$languages = null;
				foreach ($this->systemLanguages as $k => $v) 
				{
					if ($this->row['default_language'] == $k) $languages .= '<option value="'.$k.'" selected="selected">'.$v.'</option>';	
					else $languages .= '<option value="'.$k.'">'.$v.'</option>';
				}
				
				$modules = null;
				foreach ($this->allowed_modules as $v) 
				{
					if ($this->row['default_module'] == $v['name']) $modules .= '<option value="'.$v['name'].'" selected="selected">'.$v['name'].'</option>';	
					else $modules .= '<option value="'.$v['name'].'">'.$v['name'].'</option>';
				}			
				
echo '
<h2>Konfiguracja</h2>
<form action="admin,settings.html" method="post">
<table cellspacing="0" id="normal">
<tr><td><label for="default_module">Startowy moduł:</label></td><td><select name="default_module">'.$modules.'</select></td></tr>
<tr><td><label for="default_language">Domyślny język:</label></td><td><select name="default_language">'.$languages.'</select></td></tr>
<tr><td colspan="2"><input type="checkbox" class="check" name="wysiwyg" id="wysiwyg" value="1"'.$wysiwyg.' /> <label for="wysiwyg">Używaj edytora WYSIWYG</label></td></tr>
<tr><td colspan="2"><input type="hidden" name="act" value="1" /><input type="submit" name="submit" id="submit" value="ZAPISZ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form><br /><br />';
				
	foreach ($this->systemLanguages as $k => $v) 
	{
		echo '
		<form action="admin,settings.html" method="post">
		<h2>'.$v.' - konfiguracja zależna od języka.</h2>
		<table cellspacing="0" id="normal">
		<tr>
			<td><label for="keywords_'.$k.'">Meta keywords:</label><br /><textarea name="keywords_'.$k.'" rows="4" cols="32">'.$this->row['keywords_'.$k].'</textarea></td>
			<td><label for="description_'.$k.'">Meta description:</label><br /><textarea name="description_'.$k.'" rows="4" cols="32">'.$this->row['description_'.$k].'</textarea></td>
			<td><label for="title_'.$k.'">Meta title:</label><br /><textarea name="title_'.$k.'" rows="4" cols="32">'.$this->row['title_'.$k].'</textarea></td>
			<td><label for="footer_'.$k.'">Stopka:</label><br /><textarea name="footer_'.$k.'" rows="4" cols="32">'.$this->row['footer_'.$k].'</textarea></td>
		</tr>
		<tr><td colspan="4"><input type="hidden" name="lang" value="'.$k.'" /><input type="submit" name="submit" id="submit" value="ZAPISZ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';
	}
}
}
?>
