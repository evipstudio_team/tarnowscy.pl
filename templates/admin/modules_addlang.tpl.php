<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$select_lang=null;
		$select_lang.='<option value= selected >Wybierz jezyk';
		$Languages=$this->Languages;
		foreach($Languages as $key => $r)
		{
		//zbuduj selecta dla jezyków
			$key_id=$key+1;
			if ( ! strcmp ($this->row['name'],$r['name']))
				$select_lang.='<option value=admin,modules,addlang,id_'.$key_id.'.html selected >'.$r['name'].'</option>';
			else
				$select_lang.='<option value=admin,modules,addlang,id_'.$key_id.'.html >'.$r['name'].'</option>';
		}
		
		$active = ' checked="checked"';
		if (($this->row['active']!='') || (isset($this->row['active'])) )
		{
			if ($this->row['active'] != '1')  
				$active = null;
		}
		
		echo '<form action="admin,modules,addlang.html" method="post">
		<fieldset>
			<legend>Dodaj język</legend>
			<div><label for="id_lang">Miasto:</label>
			<select name="id_lang" id="id_land" onchange="location = this.options[this.selectedIndex].value;">'.$select_lang.'</select></div>
			<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" id="name" class="short" value="'.$this->row['name'].'" /></div>			
			<div><label for="short_name"><span class="b">Dwuliterowy kod</span></label><input type="text" name="short_name" id="short_name" value="'.$this->row['short_name'].'" /></div>
			<div><label for="active"><span>Aktywny:</span></label><input type="checkbox" name="active" value="1"'.$active.' /></div>	
			<div><p>Pola pogrubione są wymagane.</p></div>
			<div>
				<input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first" />			
				<input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>	
		</fieldset>
		</form>';
		
		echo '<h1>Zarządzaj językami</h1>
		<table id="language" class="clear">
			<thead>
				<tr>
					<td>#</td>
					<td>Nazwa języka</td>
					<td>Kod</td>
					<td>Status</td>
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	$rows = (array)$this->rows;
	
	if (sizeof($rows) < 1) echo '<tr><td colspan="7">Nie odnaleziono żadnych języków w bazie.</td></tr>';
	else 
	{
		foreach($rows as $row) 
		{
			$class = getTableClass();
			
			if ($row['active']) 
				$status = '<span class="green">aktywny</span><br /><a href="admin,modules,deactivelang,id_'.$row['id'].'.html">deaktywuj</a>';
			else 
				$status = '<span class="red">nieaktywny</span><br /><a href="admin,modules,activelang,id_'.$row['id'].'.html">aktywuj</a>';
			
			$edit='<a href="admin,modules,editlang,id_'.$row['id'].'.html">Edytuj</a>';
			$delete='<br /><a href="admin,modules,deletelang,id_'.$row['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąc wybrany język?\')" >usuń</a></td>';
			echo '<tr'.$class.'>
					<td>'.$row['id'].$delete.'</td>
					<td>'.$row['name'].'</td>
					<td>'.$row['short_name'].'</td>
					<td>'.$status.'</td>
					<td>'.$edit.'</td>
					
				</tr>';
		}		
	}

	echo '</tbody></table>';

	}
?>
