<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	//echo ADVGALLERY_DIR.$this->row['path'].'\thumbs\\'.$this->row['filename'];
	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else 
	{
		
echo '<h1>Edytuj wpis</h1>';
	
echo '<table id="podzial" cellspacing="0">
			<tr>
				<td id="td_left"><a href="admin,timetable,index.html" class="close00">&laquo; Zobacz wszystkie wpisy &raquo;</a><br clear="all" />'.$this->jsTree.'</td>';


echo '
<td id="td_right">

<form id="add_photo" action="admin,timetable,edit,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
<table cellspacing="0" id="normal">
<tr><td><label for="cid"><strong>Kategoria:</strong></label></td><td><select name="cid"><option value="0">- wybierz kategorię -</option>'.$this->categoriesList.'</select></td></tr>
<tr><td><label for="title"><strong>Tytuł:</strong></label></td><td><input type="text" name="title"id="title" value="'.$this->row['title'].'" /></td></tr>
<tr><td><label for="contents"><strong>Treść:</strong></label></td><td><textarea class="tiny" name="contents" rows="28">'.$this->row['contents'].'</textarea></td></tr>
<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
<tr><td colspan="2"><input type="hidden" name="oldCid" value="'.$this->row['id'].'" /><input type="submit" name="submit" id="submit" value="AKTUALIZUJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
</table>
</form>';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														ZDJĘCIA
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_photos">
	<h2><img src="templates/admin/images/icon_photo.png" alt="" />Zdjęcia</h2>
	<form action="admin,timetable,addphoto,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>Zdjęcie nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Obrazek:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>Zdjęcie nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Obrazek:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

		  <tr>
			<td>
				<label>Zdjęcie nr. 3:</label><br />
				<label for="title3">Tytuł:</label><input type="text" name="title3" id="title3" value="" /><br />
				<label for="file3"><strong>Obrazek:</strong></label><input type="file" name="file3" id="file3" /><br />
			</td>
					
			<td>
				<label>Zdjęcie nr. 4:</label><br />
				<label for="title4">Tytuł:</label><input type="text" name="title4" id="title4" value="" /><br />
				<label for="file4"><strong>Obrazek:</strong></label><input type="file" name="file4" id="file4" /><br />
			</td>
		  </tr>
		
    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane zdjęcia</h2>
	<form action="admin,timetable,photoPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td class="tocenter">Tytuł/Zdjęcie</td>
						<td class="tocenter">Główne zdjęcie</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->photos;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak zdjęć.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				if ($r['main']==1) {
					$main = '<b>TAK</b>';
				} else {
					$main = '<b>NIE</b><br /><a href="admin,timetable,setmainPhoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html">Zdjęcie główne</a>';
				}
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td class="tocenter">'.$r['title'].'<br /><br /><img src="public/timetable/photo/mini/'.$r['name'].'" alt="" /></td>
						<td class="tocenter">'.$main.'</td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="toright" width="120"><a href="admin,timetable,editphoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html"title="edytuj" class="edit"></a><a href="admin,timetable,deletephoto,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a>
						</td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" />';
		echo '	<input type="submit" name="pos" value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form>
	</div>';
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														PDFy
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_pdf">
	<h2><img src="templates/admin/images/icon_pdf.png" alt="" />Pliki PDF</h2>
	<form action="admin,timetable,addpdf,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>PDF nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Plik:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>PDF nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Plik:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane pliki PDF</h2>
	<form action="admin,timetable,pdfPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td>Nazwa</td>
						<td class="tocenter">Plik</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->pdf;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak plików PDF.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td>'.$r['title'].'</td>
						<td class="tocenter"><a href="public/timetable/pdf/'.$r['name'].'" target="_blank" title="podgląd" class="preview"></a></td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="toright" width="120"><a href="admin,timetable,editpdf,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,timetable,deletepdf,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" />';
		echo '	<input type="submit" name="pos"value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form>
	</div>';
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													Dokumenty
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	echo '
	<div id="div_doc">
	<h2><img src="templates/admin/images/icon_doc.png" alt="" />Dokumenty</h2>
	<form action="admin,timetable,adddoc,id_'.$this->row['id'].'.html" method="post" enctype="multipart/form-data">
		  <table cellspacing="0" class="label_gallery" border="1">
		  <tr>
			<td>
				<label>Dokument nr. 1:</label><br />
				<label for="title1">Tytuł:</label><input type="text" name="title1" id="title1" value="" /><br />
				<label for="file1"><strong>Plik:</strong></label><input type="file" name="file1" id="file1" /><br />
			</td>

			<td>
				<label>Dokument nr. 2:</label><br />
				<label for="title2">Tytuł:</label><input type="text" name="title2" id="title2" value="" /><br />
				<label for="file2"><strong>Plik:</strong></label><input type="file" name="file2" id="file2" /><br />
			</td>
		  </tr>

    	<tr><td colspan="2"><input type="submit" name="submit" id="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" value="WYCZYŚĆ" /></td></tr>
		</table>
		</form><br /><br />';	
						
	echo '<h2>Dodane dokumenty</h2>
	<form action="admin,timetable,docPosdeleteMark,id_'.$this->row['id'].'.html" method="post">
	<table id="tab-zlecenie">
				<thead>
					<tr>
						<td class="tocenter">Usuń grupowo</td>
						<td>Nazwa</td>
						<td class="tocenter">Plik</td>
						<td class="tocenter">Pozycja</td>
						<td class="toright">Akcja</td>
					</tr>
				</thead>
				<tbody>	';
		
		$rows = (array)$this->doc;
		
		if (count($rows) < 1) echo '<tr><td colspan="5">Brak dokumentów.</td></tr>';
		else 
		{	
			foreach($rows as $r) 
			{	
				$class = getTableClass();
				
				echo '<tr'.$class.'>
						<td class="tocenter"><input type="checkbox" name="delete_'.$r['id'].'" value="1"/></td> 
						<td>'.$r['title'].'</td>
						<td class="tocenter"><a href="public/timetable/document/'.$r['name'].'" target="_blank" title="podgląd" class="preview"></a></td>
						<td class="tocenter"><input type="text" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td class="toright" width="120"><a href="admin,timetable,editdoc,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" title="edytuj" class="edit"></a><a href="admin,timetable,deletedoc,id_'.$this->row['id'].',idFile_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')" title="usuń" class="delete"></a></td>
					  </tr>';
			}		
		}


	echo '</td></tr></table>';
	if (count($rows) > 0)
	{
		echo '	<input type="submit" name="deleteMark" value="USUŃ ZAZNACZONE" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć zaznaczone elementy?\')" style="float:left;" />';
		echo '	<input type="submit" name="pos"value="AKTUALIZUJ POZYCJĘ" style="float:right;" />';
	}
	echo '</form></div>';

echo '</td></tr></table>';
}

?>
