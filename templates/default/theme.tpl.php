<?php 
	
	//$latestNewsBlock = $this->getBlock('latestNews');	
	//$latestNews = (array)$latestNewsBlock->getLatest();	// $latestNews = dwuwymiarowa tablica zawierająca ostatnie X newsów (X do ustawienia w panelu)
													// jak zwykle - trzeba potraktować ją foreach'em
	//$jsMenu = $this->getBlock('JsMenu');	
	
	$newsBlock=$this->getBlock('GetAdvNews');
	$latestNews=(array)$newsBlock->getAll('n.main != 1','n.id DESC LIMIT 5');
	$mainNews=(array)$newsBlock->getAll('n.main = 1','n.id DESC LIMIT 1');
	
	$bannerBlock = $this->getBlock('Banner');
	$banners1 = $bannerBlock->get(1);	
	$banners2 = $bannerBlock->get(2);	
	
														
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php echo str_replace('&oacute;', 'ó', $meta); ?>
<link href="templates/default/styles/main.css" rel="stylesheet" type="text/css" />
<script src="templates/default/js/menu.js" type="text/javascript"></script>

<script type="text/javascript" src="templates/default/js/jquery-lightbox/js/jquery.js"></script>
<script type="text/javascript" src="templates/default/js/jquery-lightbox/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="templates/default/js/jquery-lightbox/css/jquery.lightbox-0.5.css" media="screen" />

	<script type="text/javascript">
    $(function() {
        $('a[@rel*=lightbox]').lightBox(); 
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
		background-color: #444;
		padding: 10px;
		width: 520px;
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
		border: 5px solid #3e3e3e;
		border-width: 5px 5px 20px;
	}
	#gallery ul a:hover img {
		border: 5px solid #fff;
		border-width: 5px 5px 20px;
		color: #fff;
	}
	#gallery ul a:hover { color: #fff; }
	</style>
	
	<?php
if ($currentURL == '' || $currentURL == 'index.php' || $currentURL == 'index.html')	
	require_once('templates/default/js/anythingslider/index.html');
	?>
	
</head>
<body>



<!--wrap begin-->
<div id="wrap">




	<!--header begin-->
	<div id="header">
		
		<a href="index.html" id="logo" title="Transport Osobowy - Tarnowscy"></a>
		
		<div id="menu">
		<?php
		
		echo '<ul id="nav-one" class="nav">';
		foreach ($menu_2 as $menu) 
			{
				$href = explode('*', $menu['href']);
				$pos = strpos($currentURL, $href[0]); // false jęśli nie odnaleziono
				$menu['href'] = str_replace('*', '', $menu['href']); // usuwamy znak * z linku
				
				$target=null;
				if($menu['target'] == 1)
					$target='target="_blank"';
						
				$mark=null;
				if (! ($pos === false) ) 
					$mark='class="mark"';
					 
				echo '<li><a href="'.$menu['href'].'" title="'.$menu['extra'].'" '.$mark.' '.$target.'>'.$menu['name'].'</a>';	
												
				$menu_string='menu_'.$menu['id'];
				if (is_array($$menu_string)) 
				{
					echo '<ul style="display:block;">';
					foreach ($$menu_string as $link) 
					{
						$href = explode('*', $link['href']);
						$pos = strpos($currentURL, $href[0]); // false jęśli nie odnaleziono
						$link['href'] = str_replace('*', '', $link['href']); // usuwamy znak * z linku
						
						$target=null;
						if($link['target'] == 1)
							$target='target="_blank"';
						
						$mark=null;
						if (! ($pos === false) ) 
							$mark='class="mark"';
					 
						echo '<li><a href="'.$link['href'].'" title="'.$link['extra'].'" '.$mark.' '.$target.'>'.$link['name'].'</a>';	
					}
					echo '</ul>';
				}
				
				echo '</li>';
			}
			echo '</ul>';
			?>
		</div>
		
	</div>
	<!--header end-->
	
	
	
	
	
	
	<!--left begin-->
	<div id="left">
		
		<!-- AKTUALNOŚCI NA GŁÓWNEJ -->
		<?php
		if ($currentURL == '' || $currentURL == 'index.php' || $currentURL == 'index.html')
		{
		echo '
			<h1>Aktualności</h1>';
		
echo '<ul id="slider">';
			foreach ($latestNews as $n)
			{
				$more=true;
				if (strlen($n['contents']) > 250) 
				{
					$description = substr($n['contents'], 0, 250);
					$tmp = explode(' ', $description);
					array_pop($tmp);
					$tmp = implode(' ', $tmp); 
					$n['contents'] = $tmp;
				}
				else 
					$more=false;

				echo '<li>
				<div class="silder_div">
				<h3>'.$n['title'].'</h3>
				'.$n['contents'].'';
				if ($more) 
					echo '<a href="aktualnosci_pokaz,'.$n['id'].'.html" class="more">Więcej</a>';
				
				echo '</div></li>';
			}
			echo '</ul><br />';

			echo '<div id="taxi"><a href="http://tarnowscytaxi.pl/" target="_blank" title="Tarnowscy Taxi"><img src="templates/default/img/banerek-tarnowscy-pl.jpg" alt="Tarnowscy Taxi"></a></div>';

		}
		
		
		
		else {
		
		echo $controller;
		
		//jezeli view to sprawdz czy jest galeria
		if ($this->row['gallery_cid'] > 0) 
		{
			$gall = $this->getBlock('LatestPhotosAdv');
			$photos = $gall->getFromCid($this->row['gallery_cid'], 2);
			if (count($photos) > 0) 
			{
				echo '<br clear="all" />
				<table cellpadding="0" cellspacing="0" class="gallery">';
				$a = 0;
				foreach ($photos as $p)
				{
					$img = 'public/advgallery/photo/mini/'.$p['filename'];
					$href = 'public/advgallery/photo/'.$p['filename'];
					$a++;
					if ($a == 0) 
					echo '<tr>'; 
					echo '<td><a href="'.$href.'" rel="lightbox[vacation]"><img src="'.$img.'" border="0" /></a></td>';
					if ($a == 3) 
					{ 
						echo '</tr>';
						$a = 0;
					}
				}
				if ($a != 0) echo '</tr>';
				echo '</table><br /><a href="galeria_'.$this->row['gallery_cid'].'.html" class="more02">więcej</a>';
			}	
		}
		
		}
		?>
		<!-- / ZWYKLY KONTROLER Z PODŁĄCZANA GALERIA -->
		
		
		
		<!--banner_left begin-->
		<br clear="all" />
		<div id="banner_left">
			<?php
	echo '';
		
		
		foreach ($banners1 as $b)
		
		{
				echo '<div>';
				
				$ext = explode('.', $b['filename']);
				$ext = strtolower($ext[count($ext)-1]);
				
				if ($ext == 'swf') {
					echo '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'.$b['width'].'" height="'.$b['height'].'" id="'.$b['filename'].'" align="middle">
						<param name="allowScriptAccess" value="sameDomain" />
						<param name="allowFullScreen" value="false" />
						<param name="movie" value="public/banner/'.$b['filename'].'" />
						<param name="quality" value="high" />
						<param name="wmode" value="transparent">
						<param name="bgcolor" value="#ffffff" />	
						<embed src="public/banner/'.$b['filename'].'" wmode="transparent" quality="high" bgcolor="#ffffff" width="'.$b['width'].'" height="'.$b['height'].'" name="top_baner" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
						</object></td>';				
				} else {		
					if ($b['www'] == 'http://' || $b['www'] == '') echo '<img src="public/banner/'.$b['filename'].'" alt="'.$b['title'].'" title="'.$b['title'].'" border="0" />';
					else echo '<a href="'.$b['www'].'"><img src="public/banner/'.$b['filename'].'" alt="'.$b['title'].'" title="'.$b['title'].'" border="0" /></a>';
				}
			echo '</div>';
		}
	?>
		</div>
		<!--banner_left end-->
		
	</div>
	<!--left end-->
	
	
	
	
	
	
	<!--right begin-->
	<div id="right">
		
		
		<div id="contact">
			<span class="nag">Szybki kontakt</span><br />
			<div>
				<?php
				$toc = $this->getBlock('GetToc');
				echo $toc->get(1);
				?>
			</div>
		</div>
		
		
		
		<div id="bannersright">
			<a href="https://www.facebook.com/Taxi-Lubin-Tarnowscy-994757213975661/" target="_blank" title="tarnowscy.pl na Facebook"><img src="templates/default/img/fb.png" alt="tarnowscy.pl na Facebook" /></a>
			<a href="http://www.e-podreczniki.nextshop.pl" target="_blank" title="nexto.pl"><img src="templates/default/img/nt.png" alt="nexto.pl" /></a>
		</div>
		
		
	</div>
	<!--right end-->
	
	
	
	<br clear="all" />	
	<!--bottom begin-->
	<div id="bottom">
	<?php
		
		
		foreach ($banners2 as $b)
		
		{
		echo '<div>'.$b['title'].'<br />';
				$ext = explode('.', $b['filename']);
				$ext = strtolower($ext[count($ext)-1]);
				
				if ($ext == 'swf') {
					echo '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'.$b['width'].'" height="'.$b['height'].'" id="'.$b['filename'].'" align="middle">
						<param name="allowScriptAccess" value="sameDomain" />
						<param name="allowFullScreen" value="false" />
						<param name="movie" value="public/banner/'.$b['filename'].'" />
						<param name="quality" value="high" />
						<param name="wmode" value="transparent">
						<param name="bgcolor" value="#ffffff" />	
						<embed src="public/banner/'.$b['filename'].'" wmode="transparent" quality="high" bgcolor="#ffffff" width="'.$b['width'].'" height="'.$b['height'].'" name="top_baner" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
						</object>';				
				} else {		
					if ($b['www'] == 'http://' || $b['www'] == '') echo '<img src="public/banner/'.$b['filename'].'" alt="'.$b['title'].'" title="'.$b['title'].'" border="0" />';
					else echo '<a href="'.$b['www'].'"><img src="public/banner/'.$b['filename'].'" alt="'.$b['title'].'" title="'.$b['title'].'" border="0" /></a>';
				}
				echo '</div>';
		}
	?>
	</div>
	<!--bottom end-->
	
	
	


</div>
<!--wrap end-->



<!--footer begin-->
<div id="footer">
	<div id="in">

		<div id="menu2">
		<?php
		foreach ($menu_7 as $menu) 
		{			
					$href = explode('*', $menu['href']);
					$pos = strpos($currentURL, $href[0]); // false jęśli nie odnaleziono
					$menu['href'] = str_replace('*', '', $menu['href']); // usuwamy znak * z linku
					
					$target=null;
					if($menu['target'] == 1)
						$target='target="_blank"';
						
					$mark=null;
					if (! ($pos === false) ) 
						$mark='class="mark"';
					 
					echo '<a href="'.$menu['href'].'" title="'.$menu['extra'].'" '.$mark.' '.$target.'>'.$menu['name'].'</a>';
					
		}
		?>
		</div>
		<br clear="all" />
		<span>
		Copyright Tarnowscy 2011.<script language="JavaScript" type="text/javascript">
<!-- 
// function s4upl() { return "&amp;r=er";}
//--> 
</script>
<!--            <script language="JavaScript" type="text/javascript" src="http://adstat.4u.pl/s.js?tarnowscy"></script>-->
            <script language="JavaScript" type="text/javascript">
<!-- 
// s4uext=s4upl();
// document.write('<img alt="statystyka" src="http://stat.4u.pl/cgi-bin/s.cgi?i=tarnowscy'+s4uext+'" width="1" height="1">')
//--> 
</script>Wszelkie prawa zastrzeżone. Powered by <a href="http://www.evipstudio.pl" target="_blank" title="evipstudio.pl - E-VIP Robert Zachar - Strony WWW - Reklama - Systemy Zarządzania Treścią - Sklepy Internetowe - Poligrafia - Studio Graficzne - Lubin." class="evip">evipstudio.pl</a>
		</span>

	</div>
</div>
<!--footer end-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129674025-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-129674025-1');
</script>

</body>
</html>
<?php 
function check_menu(array $menus, $row)
 {
	if(is_null($row))
		return false;
	if( is_array($menus) )
	{
		foreach($menus as $menu) 
		{
			$menu_string='menu_'.$menu['id'];
			//echo $menu_string;
			$href = $menu['href'];
			//echo $href;
			//pobierz podstronę
			//preg_match('/[^.]+\.[^.]+$/', $host, $matches);
			if( preg_match('/.*-((\d){1,3})\.html$/', $href, $matches) )
			{
				$subpage=$matches[1];
				//echo 'Podstrona to '.$matches[1];
				//print_r($matches);		
				//print_r($row);
				if($subpage==$row['id'])
					//echo 'OOOOOOOOOOOOOOOOOOOOOK';
					return true;
			}
		}
	}
	return false;	
 }
?>
