<?php

	$row = (array)$this->row;			
	$id = $this->id;													
	
		
	echo '<h1>Rozkład Jazdy</h1>';


	if (count($row) < 1) echo '<p>Nie odnaleziono rozkładu o podanym ID w bazie.</p>';
	else {
	
		$t_img = '';
		if ($row['filename'] != '') $t_img = '<a href="public/timetable/photo/' . $row['filename'] . '" rel="lightbox" class="img"><img src="public/timetable/photo/mini/' . $row['filename'] . '" alt="" /></a>';
		echo '
					<div class="akt">
					<h3>'.$row['title'].'</h3>
					'.$row['contents'].'<br clear="all" />
					<a href="javascript:history.back();" class="more">Powrót</a>
					</div>
		';
	}
	
	//pokaz dodatkowe elementy (zdjęcia,dokumenty,PDFy)
	require 'templates/default/show_upload.php';
	
?>
