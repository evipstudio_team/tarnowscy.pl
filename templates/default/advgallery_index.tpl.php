<?php 
	//$categoryPath = $this->currentCatPath;
	$subcats = $this->subcats;
	
	$path=null;
	foreach ($this->currentPath as $row)
	{
		$path.='<a href="galeria_'.$row['id'].'.html">'.$row['name'].'</a>';
		$path.=' &raquo; ';
	}
	$path=substr($path,0,-2);
	
	echo '<div id="path">Aktualna kategoria: '.$path.'</div>';
	
	
	echo '<h1>Galeria</h1>';
	
	
	//print_r($subcats);
	
	if (count($subcats) > 0) 
	{
		echo '<h2>Kategorie</h2>
		<table cellpadding="0" class="gallery">
		<tr><td colspan="2">'.$categoryPath.'</td></tr>';
		
		$a = 0;
		foreach ($subcats as $s)
		{
			$a++;
			if ($a == 0) 
				echo '<tr>'; 
				
			if ($s['filename'] == '')
				$img = 'public/default.jpg';
			else 
				$img = 'public/advgallery/photo/mini/'.$s['filename'];	
		
			echo '
			<td>
			<a href="galeria_'.$s['id'].'.html">'.$s['name'].'</a><br />
			<a href="galeria_'.$s['id'].'.html"><img src="'.$img.'" border="0" /></a>
			</td>
			';
			
			if ($a == 3) {
				echo '</tr>';
				$a = 0;
			}
		}
		if ($a != 0)
			echo '</tr>';
			
		echo '</table>';
		
		$pages = $this->pages_cat;
		
		if ($pages->hasPrev() || $pages->hasNext()) 
		{ 
			echo '<div class="pages-nav">';
			
			if ($pages->hasPrev()) {
				echo $pages->getPrevLink('<a href="'.$this->template_cat.'">Poprzednia</a>');
			} else {
				echo '<span class="noactive">Poprzednia</span>';
			}
			
			echo ''.$pages->getPages() .'';
			
			if ($pages->hasNext()) {
				echo $pages->getNextLink('<a href="'.$this->template_cat.'">Następna</a>');
			} else {
				echo '<span class="noactive">Następna</span>';
			}
			
			echo '</div>';
		}	
	}	
		
		
		
	if (count($this->rows) > 0) 
	{
		$gallery = $this->rows;	
		$pages = $this->pages;
		//$ccat = $this->currentCat;
	
		echo '<h2>Zdjęcia w kategorii</h2>
		<table cellpadding="0" class="gallery">
		<tr><td colspan="3">'.$categoryPath.'</td></tr>';
		
		$a = 0;
		foreach ($gallery as $p)
		{
			$a++;
			if ($a == 0) 
				echo '<tr>'; 
				
			$img = 'public/advgallery/photo/mini/'.$p['filename'];
			$href = 'public/advgallery/photo/'.$p['filename'];
			
			echo '<td>
			<a href="'.$href.'" rel="lightbox"><img src="'.$img.'" border="0" /></a>
			</td>';
			
			if ($a == 3) {
				echo '</tr>';
				$a = 0;
			}
		}
		
		if ($a != 0)
			echo '</tr>';
		
		echo '</table><br /><a href="javascript:history.back();" class="more">powrót</a>';
		
			// stronnicowanie juz znamy z news_index.tpl.php....	
		if ($pages->hasPrev() || $pages->hasNext()) { 
			echo '<div class="pages-nav">';
			
			if ($pages->hasPrev()) {
				echo $pages->getPrevLink('<a href="'.$this->template.'">Poprzednia</a>');
			} else {
				echo '<span class="noactive">Poprzednia</span>';
			}
			
			echo ''.$pages->getPages() .'';
			
			if ($pages->hasNext()) {
				echo $pages->getNextLink('<a href="'.$this->template.'">Następna</a>');
			} else {
				echo '<span class="noactive">Następna</span>';
			}
			
			echo '</div>';
		}
		
	}

?>
