<?php

	/* NIE RUSZAC */
	$news = $this->rows;			/* news = tablica dwuwymiarowa zawierająca wszystkie newsy wyświetlane na danej podstronie
									   należy ją potraktować pętlą foreach, przyklad poniezej.
									*/
	$pages = $this->pages;			// obiekt operujący na stronnicowaniu wyników, opis poniżej z przykladem	
	$statusComments = $this->statusComments; //	komentarz włączone (true) lub nie (false)											
	/* END NIE RUSZAC */
	// wyświetla wszystkie newsy:


	echo '<h1>Aktualności</h1>';
	
	echo '
	<div id="sortuj">
	<form action="aktualnosci_szukaj.html" method="post">
	<select name="year"><option value="0" >- WYBIERZ ROK </option>'.$this->yearList.'</select>
	<select name="month"><option value="0" >- WYBIERZ MIESIĄC </option>'.$this->yearMonth.'</select>
	<input type="submit" name="submit" id="submit" value="Szukaj" class="submit" />
	<input type="text" name="search" value="'.$this->search->getSearchString().'" />
	</form>
	</div>
	';
	
	
	if (count($news) < 1) echo '<p>Nie odnaleziono żadnych newsów w bazie.</p><br /><br />';
	
	else {
		
		foreach ($news as $n)
		{
		
		if (strlen($n['contents']) > 250) {
				$description = substr($n['contents'], 0, 250);
				$tmp = explode(' ', $description);
				array_pop($tmp);
				$tmp = implode(' ', $tmp); 
				$n['contents'] = $tmp;
			}
			
		$t_img = '';
		if ($n['filename'] != '') $t_img = '<img src="public/advnews/photo/mini/' . $n['filename'] . '" alt="" />';
		
		echo '<div class="akt">
			<a href="aktualnosci_pokaz,'.$n['id'].'.html"><h3>'.$n['title'].'</h3></a>
			<a href="aktualnosci_pokaz,'.$n['id'].'.html">'.$t_img.'</a>
			'.$n['contents'].'<br />
			<a href="aktualnosci_pokaz,'.$n['id'].'.html" class="more2">Więcej</a>
			</div>';
		}
	}
	

	if ($pages->hasPrev() || $pages->hasNext()) { 
		echo '<div class="pages-nav">';
		
		if ($pages->hasPrev()) {
			echo $pages->getPrevLink('<a href="'.$this->template.'">Poprzednia</a>');
		} else {
			echo '<a href="#">Poprzednia</a>';
		}
		
		echo ''.$pages->getPages() .'';
		
		if ($pages->hasNext()) {
			echo $pages->getNextLink('<a href="'.$this->template.'">Następna</a>');
		} else {
			echo '<a href="#">Następna</a>';
		}
		
		echo '</div>';
	}	
	
		/* $n dostępne w foreach to tablica zawierająca jednego newsa, dane:
		 $n['id'] = id newsa
		 $n['add_date'] = data dodania, timestamp (patrz opis add_date w subpage.tpl.php)
		 $n['author'] = autor newsa
		 $n['title'] = tytuł newsa
		 $n['contents'] = pełna treśc newsa
		 $n['short_contents'] = krótka treśc newsa podana w panelu
		 $n['comments_amount'] = ilośc komentarzy
	
		foreach ($news as $n) 
		{
			// if który sprawdza czy są włączone komentarze, jeśli tak ustawia tymczasową zmienną zawierająca link do komentarzy:
			if ($statusComments) {
				$comments_link = '<a href="aktualnosci_pokaz,'.$n['id'].'.html">komentarze ('.$n['comments_amount'].')</a>';
			}
			
			// przyklad zastosowania:
			echo '<div class="news"><table width="100%">
<tr><td colspan="2"><span>'.$n['title'].'</span></td></tr>
			<tr><td width="135">'.$t_img.'</td><td>krotka
                tresc </td></tr>
<tr>
  <td colspan="2" class="doot" align="right">spacja</td>
</tr>
<tr>
  <td colspan="2" class="doot">
    <p align="justify">'.$n['contents'].'</td>
</tr>
			<tr><td colspan="2" class="doot" align="right"><a href="http://www.zszio.pl">&gt;&gt;
                powrót</a></td></tr>
			</table></div>';
		}

	
	

	/*
		OPIS STRONNICOWANIA (jest to przykład tego, jak ja to stosuję):
		1. warunek if ($pages->hasPrev() || $pages->hasNext()) { } to warunek, który sprawdza czy należy wyświetlić listę
		   możliwych stron; wyświetla je tylko jeżli strona posiada następna lub poprzednią strone 
		
		2. warunek if ($pages->hasPrev()) {} sprawdza czy istnieje poprzednia strona, jeśli tak to wyświetla to,
		   co jest w szablonie strony, czyli: $pages->getPrevLink(SZABLON);
		   musimy to tak wyświetlać poniewaz nie wiemy jaki numer ma poprzednia strona, generuje to dynamicznie,
		   tak prawdę mówiąc to dalem to dlatego żebyś mogł edytować sobie classy tych odnośników poprzednich,
		   czyli Ciebie interesuje tylko to: class="prev">Poprzednia</a>, reszta musi zostać jak jest.
		   
		   Jeśli strona nie istnieje wyswietla: <a href="#" class="prev">Poprzednia</a> lub cokolwiek innego co zechcesz
		   	   	
		3. echo '<p>Strony: '.$pages->getPages() .'</p>';
			ta linia wyświetla wszystkie dostepne strony w postaci [1] [2] [3] etc... 
		
		4. warunek if ($pages->hasNext()) {} sprawdza czy istnieje następna strona, jeśli tak to wyświetla to,
		   co jest w szablonie strony, czyli: $pages->getNextLink(SZABLON);
		   musimy to tak wyświetlać poniewaz nie wiemy jaki numer ma nastepna strona, generuje to dynamicznie,
		   tak prawdę mówiąc to dalem to dlatego żebyś mogł edytować sobie classy tych odnośników nastepnych,
		   czyli Ciebie interesuje tylko to: class="next">Następna</a>, reszta musi zostać jak jest.
		   
		   Jeśli strona nie istnieje wyswietla: <a href="#" class="next">Następna</a> lub cokolwiek innego co zechcesz
		
		
		wynik działania tego przykładu możesz zobaczyc tutaj:  http://nexter.pl/aktualnosci.html
		gdy podstron jest bardzo duzo to wyswietaja sie tak:
		[1] [2] [3] [4] [5] [6] ... [15] [16] [17] ... [28] [29] [30]
		czyli pierwsze, srodkowe i ostatnie, oczywiscie to sie zmienia, potestujesz sobie.
   	   				
	*/

?>
