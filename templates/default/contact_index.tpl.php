<?php
	//$toc = $this->getBlock('GetToc');
	//$toc = $toc->get(1);
	$display = true;
	if (!is_null($this->message)) $display = false;
	
	if (isset($this->errors)) {
	 	$display = false;
	 	header('refresh: 3; url='.KONTAKT_REDIRECT_URL);
		echo '<table cellpadding="0" cellspacing="0"><tr><td><h1>Napotkano błąd.</h1></td></tr><tr><td><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul><br /><h3>Przekierowywanie...</h3></td></tr></table>';
	}
	
	if ($display) {
		echo '
			<table cellpadding="0" cellspacing="0">
		<tr><td><h1>Kontakt</h1></td></tr>
		<tr><td>
		<form action="wyslij.html" method="post">
		<table cellpadding="5" cellspacing="0" id="send_us">
		<tr><td colspan="2">Pola oznaczone gwiazdką (<span>*</span>) są wymagane</td></tr>
		<tr><td width="60"><label for="imie">Imię:</label></td><td><input type="text" id="imie" name="imie" value="" /></td></tr>
		<tr><td><label for="email">E-mail:</label></td><td><input type="text" id="email" name="email" value="" /> <span>*</span></td></tr>
		<tr><td><label for="telefon">Telefon:</label></td><td><input type="text" id="telefon" name="telefon" value="" /> <span>*</span></td></tr>
		<tr><td><label for="subject">Temat:</label></td><td><input type="text" id="subject" name="subject" value="" /></td></tr>
		<tr><td><label for="contents">Treść:</label></td><td><textarea id="contents" name="contents" rows="8" cols="50"></textarea> <span>*</span></td></tr>
		
		<tr><td></td><td><input type="submit" value="wyślij" class="send" /> <input type="reset" value="wyczyść" class="send" /></td></tr>
		</table>
		</form>
		</td></tr>
		</table>';	
	
	} else { echo $this->message; }
		
?>		
