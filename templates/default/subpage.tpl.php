<?php

	/* NIE RUSZAC */
	$toc = $this->toc;				// zawiera tabele zawartości (menu), lub NULL jesli podstrona jest bez tabeli
	$homepage = $this->homePage;	// true = to jest strona główna, false = zwykla podstrona
	$row = $this->row;				/* row = tablica zawierająca dane podstrony:
									   UWAGA !: $row jest rowne '' jesli podstrona jest strona glowna ! Jak to obsluzyc ? ponizej jest pokazane
										$row['id'] = numer ID podstrony 
										$row['add_date'] = timestamp dodania podstrony, aby wyświetlic nalezy uzyć funkcji date(), np: date('d.m.Y H:i', $row['add_date']), więcej na: www.php.net/date
										$row['views'] = ilość wyświetleń podstrony
										$row['author'] = autor podstrony
										$row['title'] = tytuł podstrony
										$row['contents'] = zawartość podstrony
									*/
									
	if (!is_array($row) || $row == '') {
		$article = $this->getBlock('GetArticle');	// obiekt za pomocą którego możemy pobrać stronę główna, zależnie od języka
	}
	
	if ($this->kontakt == true)
	{
		$article = $this->getBlock('GetArticle');
		$row = $article->get(6);
	}	
	
	if ($row['id'] == 6) {
		
	
		$formularz = '<form action="wyslij.html" method="post">
<table cellpadding="0" cellspacing="0" class="form">
		<tr>

			<td><label for="AAA">Imię</label></td>
			<td><input type="text" id="AAA" name="AAA" value="'.$this->row['AAA'].'" /></td>
		</tr>
		<tr>
			<td><label for="BBB">Nazwisko</label></td>
			<td><input type="text" id="BBB" name="BBB" value="'.$this->row['BBB'].'" /></td>
		</tr>
		<tr>

			<td><label for="CCC">Ulica</label></td>
			<td><input type="text" id="CCC" name="CCC" value="'.$this->row['CCC'].'" /></td>
		</tr>
		<tr>
			<td><label for="DDD">Kod pocztowy</label></td>
			<td><input type="text" id="DDD" name="DDD" value="'.$this->row['DDD'].'" /></td>
		</tr>
		<tr>

			<td><label for="EEE">Miejscowość</label></td>
			<td><input type="text" id="EEE" name="EEE" value="'.$this->row['EEE'].'" /></td>
		</tr>
		<tr>
			<td><label for="FFF">Przystanek początkowy (skąd?)</label></td>
			<td><input type="text" id="FFF" name="FFF" value="'.$this->row['FFF'].'" /></td>
		</tr>
		<tr>

			<td><label for="GGG">Przystanek końcowy (dokąd?)</label></td>
			<td><input type="text" id="GGG" name="GGG" value="'.$this->row['GGG'].'" /></td>
		</tr>
		<tr>
			<td><label for="HHH">Kierunek</label></td>
			<td>
				<select id="HHH" name="HHH">';
				
				switch ($this->row['HHH'])
				{
					case 'bilet w jedną stronę': $formularz .= '<option value="bilet w jedną stronę" selected="seleected">bilet w jedną stronę</option><option value="w dwie strony">w dwie strony</option>'; break;
					case 'w dwie strony': $formularz .= '<option value="bilet w jedną stronę">bilet w jedną stronę</option><option value="w dwie strony" selected="seleected">w dwie strony</option>'; break;
					default: $formularz .= '<option value="bilet w jedną stronę">bilet w jedną stronę</option>

					<option value="w dwie strony">w dwie strony</option>';
				}
				
		$formularz .=	'	</select>
			</td>
		</tr>
		<tr>
			<td><label for="III">Nr dokumentu uprawniającego do zniżek<br />lub dowodu osobistego (jeśli bilet normalny)</label></td>
			<td><textarea id="III" name="III" rows="2" cols="22">'.$this->row['III'].'</textarea></td>

		</tr>
		<tr>
			<td><label for="JJJ">Data ważności</label></td>
			<td><input type="text" id="JJJ" name="JJJ" value="'.$this->row['JJJ'].'" /></td>
		</tr>
		<tr>
			<td><label for="KKK">Rodzaj biletu</label></td>
			<td>

				<select id="KKK" name="KKK">';
				
				$tmp = array(
				'normalny' => 'normalny',
				'uczeń' => 'uczeń do 24 lat',
				'student' => 'student do 26 lat',
				'niepełnosprawny' => 'dziecko niepełnosprawne',
				'nauczyciel' => 'nauczyciel',
				'nauczyciel_akademicki' => 'nauczyciel akademicki',
				'"niewidomy' => 'osoba niewidoma'
				);
				
				foreach ($tmp as $k=>$v)
				{
					if ($this->row['KKK'] == $k) $formularz .= '<option value="'.$k.'" selected="selected">'.$v.'</option>';
					else $formularz .= '<option value="'.$k.'">'.$v.'</option>';
				}
					
					
		$formularz .=	'	</select>
			</td>
		</tr>
		<tr>
			<td><label for="LLL">Nr pesel (bez kresek)</label></td>

			<td><input type="text" id="LLL" name="LLL" value="'.$this->row['LLL'].'" /></td>
		</tr>
		<tr>
			<td><label for="MMM">Tel. kontaktowy</label></td>
			<td><input type="text" id="MMM" name="MMM" value="'.$this->row['MMM'].'" /></td>
		</tr>
		<tr>
			<td><label for="NNN">Adres e-mail</label></td>

			<td><input type="text" id="NNN" name="NNN" value="'.$this->row['NNN'].'" /></td>
		</tr>
		<tr>
			<td><label for="OOO">Uwagi</label></td>
			<td><input type="text" id="OOO" name="OOO" value="'.$this->row['OOO'].'" /></td>
		</tr>
		<tr>
			<td><label for="PPP">Wpisz na którym kursie podać bilet</label></td>

			<td><input type="text" id="PPP" name="PPP" value="'.$this->row['PPP'].'" /></td>
		</tr>
		<tr><td></td><td><input type="submit" value="Zamów" class="send" /> <input type="reset" value="Wyczyść" class="send" /></td></tr>
		</table>
		';
		
		$display = true;
		if (!is_null($this->message)) $display = false;
		
		if (isset($this->errors)) {
		 	$display = false;
			echo '<table cellpadding="0" cellspacing="0"><tr><td><h1>Napotkano błąd.</h1></td></tr><tr><td><ul>';
			foreach ($this->errors as $error)
			{
				echo "<li>$error</li>";
			}
			echo '</ul></td></tr></table>';
		}
		
		if (!$display) { echo $this->message; }	
		$row['contents'] = str_replace('{{bilet}}', $formularz, $row['contents']);	
	}
	
	
	// MAPKA
	if ($row['id'] == 4) {
		$kod_mapki = '<iframe width="328" height="255" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  style="border:1px solid #aaa;" src="http://maps.google.com/maps/ms?msa=0&amp;msid=203072764677756509340.0004abb74cd1f28e77e01&amp;hl=pl&amp;ie=UTF8&amp;vpsrc=6&amp;ll=51.395296,16.20801&amp;spn=0.013656,0.028067&amp;z=14&amp;output=embed"></iframe><br  Poleć znajomym  /><small><a href="http://maps.google.com/maps/ms?msa=0&amp;msid=203072764677756509340.0004abb74cd1f28e77e01&amp;hl=pl&amp;ie=UTF8&amp;vpsrc=6&amp;ll=51.395296,16.20801&amp;spn=0.013656,0.028067&amp;z=14&amp;source=embed"  Poleć znajomym  style="color:#0000FF;text-align:left">Pokaż Transport Osobowy - Tarnowscy na większej mapie</a></small>';
		$row['contents'] = str_replace('{{mapka}}', $kod_mapki, $row['contents']);
	}	
	

	echo '
	<h1>'.$row['title'].'</h1>
	'.$row['contents'].'';	
									
	//pokaz dodatkowe elementy (zdjęcia,dokumenty,PDFy)
	require 'templates/default/show_upload.php';
	
	
?>
