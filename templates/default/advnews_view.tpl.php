<?php 

	/* NIE RUSZAC */
	$row = $this->news;			/* row = tablica zawierająca wszystkie info odnośnie newsa:
								 $row['id'] = id newsa
								 $row['add_date'] = data dodania, timestamp (patrz opis add_date w subpage.tpl.php)
								 $row['author'] = autor newsa
								 $row['title'] = tytuł newsa
								 $row['contents'] = pełna treśc newsa
								 $row['short_contents'] = krótka treśc newsa podana w panelu
								 $row['comments_amount'] = ilośc komentarzy
								*/
								
	$comments = $this->comments; /* comments = tablica dwuwymiarowa zawierająca wszystkie komenatrze do newsa
									   należy ją potraktować pętlą foreach, przyklad poniezej.
									*/						
	$statusComments = $this->statusComments; //	komentarz włączone (true) lub nie (false)
	$id = $this->id;													
	/* END NIE RUSZAC */


	
	echo '<h1>AKTUALNOŚCI</h1>';

//d	print_r($this->row);
	if (count($row) < 1) echo '<p>Nie odnaleziono newsa o podanym ID w bazie.</p>';
	
	else {
	
		$t_img = '';
		if ($row['filename'] != '') $t_img = '<a href="public/advnews/photo/' . $row['filename'] . '" rel="lightbox" class="img"><img src="public/advnews/photo/mini/' . $row['filename'] . '" alt="" /></a>';
		echo '
		<div class="akt">
		<h3>'.$row['title'].'</h3>
		'.$t_img.'
		'.$row['contents'].'<br clear="all" />
		<a href="javascript:history.back();" class="more2">Powrót</a>
		</div>';
	}

	//pokaz dodatkowe elementy (zdjęcia,dokumenty,PDFy)
	require 'templates/default/show_upload.php';	
	
	
?>
