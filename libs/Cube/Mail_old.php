<?php

class Cube_Mail
{
	private $_from = '';
	private $_subject = '';
	private $_content = '';
	
	public function __construct($from = '', $sub = '', $content = '')
	{
		if ($from != '') $this->_from = $from;
		if ($sub != '') $this->_subject = $sub;
		if ($content != '') $this->_content = $content;
	}
	
	public function from($from)
	{
		$this->_from = $from;
	}
	
	public function subject($subject)
	{
		$this->_subject = $subject;
	}
	
	public function content($content)
	{
		$this->_content = $content;
	}		
		
	public function send($to)
	{
		if ($this->_from == '') throw new Exception('Cube_Mail: _from is NULL');
		if ($this->_subject == '') throw new Exception('Cube_Mail: _subject is NULL');
		if ($this->_content == '') throw new Exception('Cube_Mail: _content is NULL');
		
		$headers = "From: ".$this->_from." \r\nReply-to: ".$this->_from."\n"
					  . "Content-Type: text/plain; charset=UTF-8; format=flowed\n"
					  . "MIME-Version: 1.0\n"
	    			  . "Content-Transfer-Encoding: 8bit\n"
	                  . "X-Mailer: PHP\n";
		if(@mail($to, $this->_subject(), $this->_content, $headers)) return true;	
		return false;
	}
	
	private function _subject()
	{
		$subject = preg_replace('/([^a-z ])/ie', 'sprintf("=%02x",ord(StripSlashes("\1")))', $this->_subject);
		$subject = str_replace(' ', '_', $subject);
		return "=?utf-8?Q?$subject?=";
	}
}

?>
