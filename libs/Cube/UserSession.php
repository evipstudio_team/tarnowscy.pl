<?php

/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_UserSession
{
	protected $_sessionID;				// ID sesji
	private $_sessionTimeout  = 600;	// czas aktywności sesji = 10 minut
	private $_sessionLifespan = 3600;	// żywotność sesji i cookie = 1 godzina
	protected $_user;					// Array, dane użytkownika
	protected $_config;					// Cube_Config handler	
	protected $_tables = array('sessions' => 'sessions', 'users' => 'users', 'vars' => 'session_vars');	// nazwy tabel
	protected $_request = NULL;
	
	public function __construct()
	{
		$this->_clear();
		$this->_config = Cube_Registry::get('config');	
		//$this->_request = $request;
		//$this->setLanguageTemp($this->_request->getLang());
		
		session_set_save_handler(
			array(&$this, '_session_open_method'),
			array(&$this, '_session_close_method'),
			array(&$this, '_session_read_method'),
			array(&$this, '_session_write_method'),
			array(&$this, '_session_destroy_method'),
			array(&$this, '_session_gc_method')
		);
		
		$userAgent = clear($_SERVER['HTTP_USER_AGENT']);
		
		if ($_COOKIE['PHPSESSID']) { // sprzawdz czy cookie istnieje z sessid
			$this->_sessionID = clear($_COOKIE['PHPSESSID']);	// zapamietaj sessid	
			// sprawdz czy istnieje user
			$sql = 'SELECT COUNT(id) FROM '.$this->_tables['sessions'].' WHERE sid = "'.$this->_sessionID.'"
					AND ('.time().' - create_time) < '.$this->_sessionLifespan.'
					AND user_agent = "'.$userAgent.'"
					AND ('.time().' - last_reaction) <= '.$this->_sessionTimeout.'
					OR last_reaction = 0';
			$stmt = mysql_query($sql);
			if (mysql_num_rows($stmt) < 1) {
				mysql_query('DELETE FROM '.$this->_tables['sessions'].' WHERE sid= "'.$this->_sessionID.'" OR ('.time().' - create_time) > '.$this->_sessionLifespan);		
				mysql_query('DELETE FROM '.$this->_tables['vars'].' WHERE sid NOT IN (SELECT id FROM '.$this->_tables['sessions'].')');
				unset($_COOKIE['PHPSESSID']);	
			} 	
		}
		session_set_cookie_params($this->_sessionLifespan); // ustaw czas życia sesji
		session_start();
	}

	private function garbage() // garbage collector
	{
		mysql_query('DELETE FROM '.$this->_tables['sessions'].' WHERE ('.time().' - create_time) > '.$this->_sessionLifespan);		
		mysql_query('DELETE FROM '.$this->_tables['vars'].' WHERE sid NOT IN (SELECT id FROM '.$this->_tables['sessions'].')');
	}
	
	public function impress()	// aktualizuj sesje, last_reaction
	{
		if ($this->_user['sid']) {
			mysql_query('UPDATE '.$this->_tables['sessions'].' SET last_reaction = '.time().' WHERE id = '.$this->_user['sid']);
			$this->garbage();
		}
	}
	
	public function setLanguageTemp($lang)
	{
		if ($this->_user['sid']) {
			mysql_query('UPDATE '.$this->_tables['sessions'].' SET language_temp = "'.$lang.'" WHERE id = "'.$this->_user['sid'].'"');
			$this->_user['language_temp'] = $lang;
		}	
	}
	
	
	public function setLanguage($lang)
	{
		if ($this->_user['sid']) {
			mysql_query('UPDATE '.$this->_tables['sessions'].' SET language = "'.$lang.'" WHERE id = "'.$this->_user['sid'].'"');
			mysql_query('UPDATE '.$this->_tables['users'].' SET language = "'.$lang.'" WHERE id = "'.$this->_user['id'].'"');
			$this->_user['lang'] = $lang;
		}	
	}

	public function setTheme($theme)
	{
		if ($this->_user['sid']) {
			mysql_query('UPDATE '.$this->_tables['sessions'].' SET theme = "'.$theme.'" WHERE id = "'.$this->_user['sid'].'"');
			mysql_query('UPDATE '.$this->_tables['users'].' SET theme = "'.$theme.'" WHERE id = "'.$this->_user['id'].'"');
			$this->_user['theme'] = $theme;
		}	
	}
	
	public function isAdmin() {
		return ($this->_user['role'] === 'administrator') ? true : false;
	}
	
	public function isLoggedIn() {
		return $this->_user['logged'];
	}
	
	public function getUserId()
	{
		return ($this->_user['logged']) ? $this->_user['id'] : false;
	}
	
	public function login($username, $password)
	{
		$password = sha1($password);
		$stmt = mysql_query('SELECT * FROM '.$this->_tables['users'].' WHERE login = "'.$username.'" AND pass = "'.$password.'" AND active = "1"');
		if (mysql_num_rows($stmt) > 0) {
			$row = mysql_fetch_assoc($stmt);
			$this->_user['id'] 			= $row['id'];
			$this->_user['role'] 		= $row['role'];
			$this->_user['login'] 		= $row['login'];
			$this->_user['active'] 		= $row['active'];
			$this->_user['email'] 		= $row['email'];
			$this->_user['akey'] 		= $row['akey'];
			$this->_user['akey_expire'] 	= $row['akey_expire'];
			$this->_user['language'] 	= $row['language'];
			$this->_user['language_temp'] 	= $row['language_temp'];
			$this->_user['theme'] 		= $row['theme'];
			$this->_user['name'] 		= $row['name'];
			$this->_user['surname'] 	= $row['surname'];
			$this->_user['extra'] 		= unserialize($row['extra']);
			$this->_user['logged'] 		= true;			
		    mysql_query('UPDATE '.$this->_tables['sessions'].' SET logged = 1, uid = "'.$this->_user['id'].'" WHERE id = "'.$this->_user['sid'].'"');
		    return true;
		}
		return false;
	}
	
	public function logout()
	{
		if ($this->_user['logged']) {
			mysql_query('UPDATE '.$this->_tables['sessions'].' SET logged = 0, uid = 0 WHERE id = "'.$this->_user['sid'].'"');
			$this->_clear();	    
		    return true;
		}
		return false;
	}
	
	public function __get($key)
	{
		$stmt = mysql_query('SELECT v FROM '.$this->_tables['vars'].' WHERE sid = "'.$this->_user['sid'].'" AND k = "'.$key.'"');
		if (mysql_num_rows($stmt) > 0) {
			$row = mysql_fetch_assoc($stmt);
		    return unserialize($row['v']);
		}	
		return null;	
	}

	public function __set($key, $val)
	{
		$val = addslashes(serialize($val));		
		$r = mysql_query('SELECT id FROM '.$this->_tables['vars'].' WHERE k = "'.$key.'" AND sid = "'.$this->_user['sid'].'"');
		if (mysql_num_rows($r) > 0) {
			mysql_query('UPDATE '.$this->_tables['vars'].' SET v = "'.$val.'" WHERE k = "'.$key.'" AND sid = "'.$this->_user['sid'].'"');
		} else mysql_query('INSERT INTO '.$this->_tables['vars'].' (id, sid, k, v) VALUES (null, "'.$this->_user['sid'].'", "'.$key.'", "'.$val.'")');
	}
	
	public function _session_open_method($save_path, $session_name) { return true; }
	public function _session_close_method() { return true; }	
	public function _session_write_method($id, $sess_data) { return true; }
	public function _session_gc_method($maxlifetime) { return true; }		
	public function _session_destroy_method($id)
	{
		mysql_query('DELETE FROM '.$this->_tables['sessions'].' WHERE sid = "'.$id.'"');
		return true;
	}
		
	public function _session_read_method($id)
	{
		$userAgent = clear($_SERVER['HTTP_USER_AGENT']);
		$this->_sessionID = $id;		
		
		$stmt = mysql_query('SELECT s.id AS sid, u.id as id, login, role, email, akey, akey_expire, active, u.name, u.surname, u.extra, u.language as language, s.language_temp, u.theme as theme, s.logged as logged  
							 FROM '.$this->_tables['sessions'].' s, '.$this->_tables['users'].' u 
							 WHERE s.sid = "'.$id.'" AND (s.uid = u.id OR s.uid = 0)');
		if (mysql_num_rows($stmt) > 0) {
			$row = mysql_fetch_assoc($stmt);
			$this->_user['sid'] = $row['sid'];						
			if ($row['logged']) {		
				$this->_user['id'] 			= $row['id'];
				$this->_user['role'] 		= $row['role'];
				$this->_user['login'] 		= $row['login'];
				$this->_user['active'] 		= $row['active'];
				$this->_user['email'] 		= $row['email'];
				$this->_user['akey'] 		= $row['akey'];
				$this->_user['akey_expire'] = $row['akey_expire'];
				$this->_user['language'] 	= $row['language'];
				$this->_user['language_temp'] 	= $row['language_temp'];
				$this->_user['theme'] 		= $row['theme'];
				$this->_user['name'] 		= $row['name'];
				$this->_user['surname'] 	= $row['surname'];
				$this->_user['extra'] 		= unserialize($row['extra']);
				$this->_user['logged'] 		= true;						
			} else {
				$stmt = mysql_query('SELECT language, language_temp, theme FROM '.$this->_tables['sessions'].' WHERE sid = "'.$id.'"');
				$row = mysql_fetch_assoc($stmt);
				$this->_user['language'] 	= $row['language'];
				$this->_user['language_temp'] 	= $row['language_temp'];
				$this->_user['theme'] 		= $row['theme'];
				$this->_user['logged'] = false;	
			}	
		} else { 	
			$this->_user['logged'] = false;
			mysql_query('INSERT INTO '.$this->_tables['sessions'].' (sid, logged, uid, create_time, user_agent, language, language_temp, theme) 
					     VALUES ("'.$id.'", 0, 0, '.time().', "'.$userAgent.'", 
						 "'.$this->_config->get('default_language').'", "'.$this->_config->get('default_language').'", "'.$this->_config->get('default_theme').'")');	
			 	     
			$this->_user['language'] = $this->_config->get('default_language');
			$this->_user['language_temp'] = $this->_config->get('default_language');
			$this->_user['theme'] = $this->_config->get('default_theme');
			$stmt = mysql_query('SELECT id FROM '.$this->_tables['sessions'].' WHERE sid = "'.$id.'"');
			if (mysql_num_rows($stmt) > 0) {
				$row = mysql_fetch_assoc($stmt);
				$this->_user['sid'] = $row['id'];		
			}
		}	
		return '';
	}
	
	public function getUsername()
	{
		return ($this->_user['logged']) ? $this->_user['login'] : null;
	}
	
	public function getRole()
	{
		return $this->_user['role'];
	}
	
	public function getName()
	{
		return $this->_user['name'];
	}
	
	public function getSurname()
	{
		return $this->_user['surname'];
	}
	
	public function getExtra()
	{
		return $this->_user['extra'];
	}
	
	public function getFromExtra($key)
	{
		return $this->_user['extra'][$key];
	}
					
	public function getDefaultLanguage()
	{
		return $this->_config->get('default_language');
	}
	
	public function getDefaultTheme()
	{
		return $this->_config->get('default_theme');
	}
		
	public function getLanguageTemp()
	{
		//echo 'LanguageTemp='.$this->_user['language_temp'];
		return $this->_user['language_temp'];
	}
	
	
	public function getLanguage()
	{
		return $this->_user['language'];
	}
	
	public function getTheme()
	{
		return $this->_user['theme'];
	}
		
	public function isActive()
	{
		return $this->_user['active'];
	}
	
	public function getAkey()
	{
		return $this->_user['akey'];
	}
	
	public function getAkeyExpire()
	{
		return $this->_user['akey_expire'];
	}

//	public function getSessionId() {
//		return $this->_sessionID;
//	}	

	protected function _clear()
	{
		$this->_user['id'] 			= 0;
		$this->_user['role'] 		= 'guest';
		$this->_user['login'] 		= 'guest';
		$this->_user['active'] 		= 0;
		$this->_user['email'] 		= '';
		$this->_user['akey'] 		= '';
		$this->_user['akey_expire'] = '';
		$this->_user['logged'] 		= false;
		$this->_user['name'] 		= '';
		$this->_user['surname'] 	= '';
		$this->_user['extra'] 		= array();	
	}					
}

?>
