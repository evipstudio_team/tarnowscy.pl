<?php
class Cube_Model_Tree extends Cube_Model_Abstract
{
  //private $_name;
 
  public function leftMenuTree ($id,$order=null)
  { 
	if (!is_null($order)) $order = ', '.$order;
	$sql='SELECT t.* 
		  FROM '.$this->_name_tree.' t, '.$this->_name_tree_pos.' tp 
		  WHERE t.id = tp.child_id and t.active=1 and tp.depth = 1 and tp.parent_id IN 
		  (
			SELECT parent_id 
			FROM '.$this->_name_tree_pos.' 
			WHERE child_id ='.$id.'
		  )
		 UNION 
		 SELECT t.* 
		 FROM '.$this->_name_tree.' t, '.$this->_name_tree_pos.' tp 
		 WHERE t.id = tp.child_id  and tp.child_id=1	
		 ORDER BY parent_id'.$order;
	//echo $sql;
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;
  }	
  
  public function getAllTreeWSTCC($where=null,$order=null)
  {
	 //zwraca cale drzewa podobnie jak funkcja getall + dodatkowa kolumne child_count z informacja o liczbie potomk�w rodzica
	 //getAllTreeWCC - get all tree with sub tree children count 
	 //potrzebne do rozwijanego menuJS przy okresleniu ilosci dzieci rodzica
	 if (!is_null($order)) $order = ', '.$order;
	if(is_null($where))
	{
		$sql='SELECT t.*,tp2.count_child
			  FROM '.$this->_name_tree.' as t,
			  '.$this->_name_tree_pos.' as tp1 LEFT JOIN 
			  (
				SELECT parent_id,count(*) -1 as count_child 
				FROM '.$this->_name_tree_pos.' 
				GROUP BY parent_id
			  ) tp2 ON (tp1.child_id=tp2.parent_id)
			  WHERE tp1.parent_id=1 and tp1.child_id=t.id
			  ORDER BY tp1.depth'.$order;
	} 
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;  
  }
  
  
  
  public function getAllTreeWCC($where=null,$order=null)
  {
	 //zwraca cale drzewa podobnie jak funkcja getall + dodatkowa kolumne child_count z informacja o liczbie dzieci rodzica
	 //getAllTreeWCC - get all tree with children count 
	 //potrzebne do rozwijanego menuJS przy okresleniu ilosci dzieci rodzica
	 if (!is_null($order)) $order = ', '.$order;
	if(is_null($where))
	{
		$sql='SELECT t.*,tp2.count_child
			  FROM '.$this->_name_tree.' as t,
			  '.$this->_name_tree_pos.' as tp1 LEFT JOIN 
			  (
				SELECT parent_id,count(*) as count_child 
				FROM '.$this->_name_tree_pos.' 
				WHERE depth=1 
				GROUP BY parent_id
			  ) tp2 ON (tp1.child_id=tp2.parent_id)
			  WHERE tp1.parent_id=1 and tp1.child_id=t.id
			  ORDER BY tp1.depth'.$order;
	} 
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;  
  }
  
  
  public function getAllTree($where=null,$order=null)
  {
	//if (!is_null($where)) $where = ' WHERE '.$where;
	if (!is_null($order)) $order = ', '.$order;
	if(is_null($where))
	{
		$sql='SELECT t.*
			FROM '.$this->_name_tree.' t,'.$this->_name_tree_pos.' tp 
			WHERE t.id=tp.child_id and tp.parent_id=1 and t.active=1
			ORDER BY tp.depth'.$order;
	}
	else
	{
		//gdy jest wybrany where
		$sql='SELECT t.*
			 FROM '.$this->_name_tree.' t,'.$this->_name_tree_pos.' tp 
			 WHERE t.id=tp.parent_id and t.active=1 and tp.child_id IN (select id from '.$this->_name_tree.' where '.$where.') 
			 GROUP BY t.id
			 ORDER BY t.parent_id'.$order;
	}
	//echo $sql;	
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;
  }
  
  public function activeSubTree($id)
  {
	  //deaktywuj cale poddrzewo
	  $rows=$this->getSubTreePos($id);
	  
	  $where=null;
	  $sql='UPDATE '.$this->_name_tree.' SET active=1 WHERE (';
	  foreach ($rows as $row)
	  {
		//$sql='UPDATE '.$this->_name_tree.' SET active=0 WHERE id='.$row['child_id'];
		$where.='id='.$row['child_id'].' OR ';
	  }
		$sql.=$where;
		$sql = substr($sql, 0, -4).') ';
	 
	 //echo $sql;
	 mysql_query($sql);
  }
  
  
  public function deactiveSubTree($id)
  {
	  //deaktywuj cale poddrzewo
	  $rows=$this->getSubTreePos($id);
	  
	  $where=null;
	  $sql='UPDATE '.$this->_name_tree.' SET active=0 WHERE (';
	  foreach ($rows as $row)
	  {
		//$sql='UPDATE '.$this->_name_tree.' SET active=0 WHERE id='.$row['child_id'];
		$where.='id='.$row['child_id'].' OR ';
	  }
		$sql.=$where;
		$sql = substr($sql, 0, -4).') ';
	 
	 //echo $sql;
	 mysql_query($sql);
  }
  
  
  public function moveBranchTree($id, $new_id)
  {
	//zmien wiersza w tabeli TREE
	$sql='UPDATE '.$this->_name_tree.' SET parent_id='.$new_id.' WHERE id='.$id;
	//echo 'SQL1='.$sql;
	mysql_query($sql);
		
	//Pobierz id do usuniecia w tabeli TREE_POS
	$sql='SELECT tp2.id as id
		  FROM '.$this->_name_tree_pos.' tp1, '.$this->_name_tree_pos.' tp2 
		  WHERE tp1.child_id = tp2.child_id 
		  AND tp1.parent_id = '.$id.' AND tp2.depth > tp1.depth';
	//echo 'SQL2='.$sql;
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) $rows=mysql_fetch_all($r);	
	else return null;
	
	//usun wybrane wiersze ze starej pozycji
	foreach ($rows as $row)
	{
		$sql='DELETE FROM '.$this->_name_tree_pos.' WHERE id='.$row['id'];
		//echo 'SQL3='.$sql;
		mysql_query($sql);
	}
	
	//dopisz nowa pozycje - po przeniesieniu do tabali TREE_POS
	$sql='INSERT INTO '.$this->_name_tree_pos.' (parent_id, child_id, depth)
		SELECT tp1.parent_id, tp2.child_id, tp1.depth + tp2.depth + 1
		FROM '.$this->_name_tree_pos.' tp1, '.$this->_name_tree_pos.' tp2
		WHERE tp1.child_id = '.$new_id.' AND tp2.parent_id = '.$id;
	//echo 'SQL4='.$sql;
	mysql_query($sql);
  }
  
  public function moveAllBranchTree($id, $new_id)
  {
	//poberz wszystkie dzieci wezla ktory ma bys usuniety. Jezeli nie ma potomkow zostanie zwrocowny sam wezel.
	$rows=$this->getAllChildrenPos($id);
	
	foreach ($rows as $row)
	{
		$this->moveBranchTree($row['child_id'], $new_id);
	}
  }
  
  public function deleteTree($id, $class=null, $new_id=null, $param=true)
  {
    //Usuwa cz�ci drzewa o wskazanym id.
	//$param=true usuwa wszystkich potomko�w i wszystkie zalaczniki (np. dokumenty) ktore byly przypisane do tych potomkow
	//$param=false usuwa wszystkich potomko�w ale przenosi wszystkie zalaczniki (np. dokumenty) ktore byly przypisane do tych potomkow do wskazanego wezla
	//$new_id - jezeli zalaczniki(np. dokumenty) sa przenoszone to to jest id nowego w�z�a do ktorego trzeba przeniesc zalaczniki.
	//Sclass - tablica z obiektami zawierajacym zalaczniki (nazwy modeli do tabel zwiazanych z id drzewa na ktorym trzeba wykonac operacj� usuwania).
	
	//poberz wszystkich potomkow (dzieci i ich dzieci) wezla ktory ma bys usuniety. Jezeli nie ma potomkow zostanie zwrocowny sam wezel.
	$rows=$this->getSubTreePos($id);
	foreach ($rows as $row)
	{
		if( !is_null($class))
		{
			foreach ($class as $c)
			{
				if($param)
				{
					//usun wszystkie dokumenty. 
					$model = new $c();
					$model->deleteAll($row['child_id']);
				}
				else
				{
					//Jezeli wybrana opcja przenies dokumenty to $param=false i dokumenty sa przenoszone
					$model = new $c();	
					$model->setCategories($row['child_id'], $new_id);
					//usun dokumenty z rodzica
					$model->deleteAll($id);
				}
			}
		}
		
		$sql='DELETE FROM '.$this->_name_tree_pos.' where child_id='.$row['child_id'];
		mysql_query($sql);
		
		$sql='DELETE FROM '.$this->_name_tree.' where id='.$row['child_id'];
		mysql_query($sql);

	}
		
  }
  
  public function insertTree($data)
  {
	$parent=$data['parent_id'];
	//zapisz do tabeli tree
	parent::insert($data);
	$id=mysql_insert_id();
	
	//zapisz do tabeli tree_pos
	$sql='INSERT INTO '.$this->_name_tree_pos.' ( id , parent_id , child_id , depth ) 
		  VALUES (NULL ,'.$id.','.$id.', 0)';
	mysql_query($sql);	
	
	$sql='INSERT INTO '.$this->_name_tree_pos.'(parent_id, child_id, depth)
	SELECT parent_id,'.$id.',depth +1 from '.$this->_name_tree_pos.' where child_id ='.$parent ; 
	mysql_query($sql);
	return $id;
  }
  
  public function getAllTreePath($id=null)
  {
	//zwraca dane potrzebne do zbudowania sciezki (Korze�/Rodzic1/Rodzic2). Wszystkie kombinacje drzewa
	//$sql = 'SELECT t.* FROM '.$this->_name_tree_pos.' as tp, '.$this->_name_tree.' as t WHERE t.id=tp.parent_id ORDER BY child_id,parent_id';
	//wersja poprawiona, sortowanie po glebokosci.
	
	//za pomoca where wyrzucamy z drzewa wszystkich potomkow dla danego wezla(id). Potrzebne aby przy usuwaniu wezla nie wskazac elementow do
	//przeniesinie jako dzieci tego wezla, poniewaz dzieci zostana usuniete lub przeniesione.
	$where=null;
	if (!is_null($id)) 
	{
		$where=' AND tp1.child_id NOT IN 
			(
				SELECT child_id
				FROM '.$this->_name_tree_pos.'
				WHERE parent_id ='.$id.'
			)';
		//uwzgledniamy czy kategoria jest aktywna
		//potrzebne aby nie przeniesc categorii do categorii ktora nie jest aktywna
		$where_active=' AND tp1.child_id NOT IN
			(
					   SELECT id 
					   FROM '.$this->_name_tree.' 
					   WHERE active=0
			)';	
		$where.=$where_active;	
	}		
	$sql = 'SELECT t.*, 
			(	SELECT count(*) 
				FROM '.$this->_name_tree_pos.' as tp2 
				WHERE tp1.child_id=tp2.child_id 
				GROUP BY child_id) 
			as depth2
			FROM '.$this->_name_tree_pos.' as tp1, '.$this->_name_tree.' as t
			WHERE t.id=tp1.parent_id'.$where.' 
			ORDER BY depth2,child_id,parent_id';
	//echo $sql;		
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;
  }
 
  public function getBranchTree($id)
  {	
	$sql = 'SELECT t.* FROM '.$this->_name_tree.' as t, '.$this->_name_tree_pos.' as tp 
			WHERE t.id = tp.parent_id and tp.child_id="'.$id.'" 
			ORDER BY t.id';
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	
	if($r != null) return mysql_fetch_all($r);	
	else return null;
		
  }
  
	public function getAllChildren($parent_id,$where=null)
  {
	//pobierz dzieci z tabeli categoirii(drzewa)
	if( !is_null($where) )
		$where=' AND '.$where;
	
	return (parent::getAll('parent_id='.$parent_id.$where,'pos,name') );
  }			
  
  /*public function getAllChildren($parent_id,$active=null)
  {
	//pobierz dzieci z tabeli categoirii(drzewa)
	if( is_null($active) )
		return (parent::getAll('parent_id='.$parent_id) ); 
	else
		//tylko aktywne dzieci
		return (parent::getAll('active=1 AND parent_id='.$parent_id,'pos,id') ); 
  }*/ 
  
  public function getAllChildrenPos($id)
  {
	//pobierz dzieci z tabeli pozycji(drzewa)
	$sql='SELECT * from '.$this->_name_tree_pos.' where parent_id ='.$id.' and depth=1';
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;
  } 

  public function getSubTreePos($id)
  {
	//pobierz potomkow (dzieci i ich dzieci) z tabeli pozycja drzewa
	$sql='SELECT * from '.$this->_name_tree_pos.' where parent_id ='.$id;
	$r = mysql_query($sql) or error_log('Somting wrog in:'.$sql);
	if($r != null) return mysql_fetch_all($r);	
	else return null;
  }   

}
?>
