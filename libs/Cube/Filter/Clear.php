<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_Clear implements Cube_Filter_Interface
{

	protected $_allowedTags = null;
	
    public function __construct($params)
    {
    	if (isset($params['allowedTags'])) 
			$this->_allowedTags = $params['allowedTags'];	
    }

    public function filter($value)
    {
    	$value = strip_tags($value, $this->_allowedTags);
    	//$value = strtr($value, array('Ż'=>'#&#'));
    	return htmlentities($value, ENT_QUOTES, 'UTF-8');
    }
}
