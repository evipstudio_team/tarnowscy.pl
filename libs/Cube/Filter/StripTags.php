<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_StripTags implements Cube_Filter_Interface
{
	protected $_allowedTags = null;
	
	public function __construct($params)
	{
		if (isset($params['allowedTags'])) 
			$this->_allowedTags = $params['allowedTags'];	
	}
	
	public function filter($value)
	{
		return strip_tags($value, $this->_allowedTags);
	}
}

?>
