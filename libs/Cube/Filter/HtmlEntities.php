<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_HtmlEntities implements Cube_Filter_Interface
{
    protected $_quoteStyle	= ENT_COMPAT;
    protected $_charSet		= 'UTF-8';

    public function __construct($params)
    {
   		if (isset($params['quoteStyle'])) $this->_quoteStyle = $params['quoteStyle'];
   		if (isset($params['charSet'])) 	  $this->_charSet    = $params['charSet'];
    }

    public function filter($value)
    {
        return htmlentities((string) $value, $this->_quoteStyle, $this->_charSet);
    }
}
