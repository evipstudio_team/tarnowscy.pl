<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_AllowHtmlStandardAdmin implements Cube_Filter_Interface
{
	protected $_allowedTags = null;
	protected $_nl2br = true;
	
    public function __construct($params)
    {
    	if (isset($params['allowedTags'])) 
			$this->_allowedTags = $params['allowedTags'];
		if (isset($params['nl2br'])) 
			$this->_nl2br = $params['nl2br'];		
    }

    public function filter($value)
    {	
		if ($this->_nl2br) $value = nl2br($value);
		if (is_null($this->_allowedTags)) $this->_allowedTags = '<script><iframe><blockquote><cite><abbr><acronym><p><ul><ol><li><span><pre><b><i><u><br><div><table><tr><th><td><tbody><tfoot><thead><em><strong><strike><img><h1><h2><h3><h4><h5><h6><a><object>';	
		$value = strip_tags($value, $this->_allowedTags);
		$value = addslashes($value);
		return $value;
    }
}
