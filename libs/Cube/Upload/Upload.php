<?php

class Cube_Upload_Upload
{
	protected $_fieldName = null;  // nazwa pola w POST
	protected $_file=null;	//referencja do klasy z plikami	
	protected $_filename=null; //nazwa pliku do zapisania w bazie danych. Zwracana przez funcje _prepareName
	protected $_path = null; 				
	
	
	/* $_FILES */
	protected $_name;       	   // nazwa obrakza - oryginał
	protected $_tmpName;           // nazwa tymczasowa
	protected $_type;			   // typ obrazka
	protected $_error;  		   // error
	protected $_size;		       // waga obrazka
	protected $_extension;		   // rozszerzenie pliku	
	
	/* RESIZE */
	/*protected $_x;				   // szerokość obrazka	
	protected $_y;				   // wysokość obrazka
	protected $_sourceImage;*/	   // handler obrazka
	
	/* PATHS and NAMESPACES */
	
	
	protected $_profiledName;		  // nazwa po konwersji, filtr	
	protected $_saveAs =null; // nazwa zapisuj, :name - oryginalna nazwa po przefiltrowaniu, :ext - rozszerzenie, :date - data, :random - losowy ciąg
	protected $_variables = array();  // zmienne dla saveAs, :tag => value
		
	
	protected $_date = 'YmdHis';
	protected $_random = 5;	

	/* REST */
	protected $_errors = array();  // errory 
	protected $_errorMessages = array('type' => 'Przesłany plik jest niedozwolonego typu (:type).',
									  'size' => 'Maksymalna wielkość pliku to :max kB. Przesłany plik waży :value kB.',
									  'error' => 'Przesyłanie pliku nie powiodło się z nieokreślonych przyczyn. Spróbuj ponownie.',
									  'noImage' => 'Nie przesłano żadnego pliku.',
									  'move' => 'Nie przesłano żadnego pliku.');	
	
	
	protected $_allowedTypes = array('image/jpeg', 'image/gif', 'image/png', 'image/pjpeg', 'image/bmp', 'image/x-png');
	protected $_maxFileSize  = 10240;
	
	public function __construct($fieldName,Cube_Upload_File $file) 
	{
		$this->_file=$file;
		$this->_path=$file->getPath();
		$this->_saveAs=time().'_:name';
		
		// $_files
		$this->_fieldName = $fieldName;
		$this->_name 	  = $_FILES[$this->_fieldName]['name'];
		$this->_tmpName   = $_FILES[$this->_fieldName]['tmp_name'];
		$this->_type      = $_FILES[$this->_fieldName]['type'];
		$this->_error 	  = $_FILES[$this->_fieldName]['error'];
		$this->_size	  = (int) ($_FILES[$this->_fieldName]['size']/1024);
		
		// extension
		$temp = explode('.', $_FILES[$this->_fieldName]['name']);
		$this->_extension = '.'.$temp[count($temp) - 1];
		
		// profiled name
		$this->_profiledName = strtr($this->_name, array(' ' => '_','"' => '','ą' => 'a','ś' => 's','ć' => 'c',
		                                                 'ę' => 'e','ó' => 'o','ń' => 'n','ł' => 'l','ź' => 'z',
														 'ż' => 'z','A' => 'A','Ś' => 'S','Ć' => 'C','Ę' => 'E',
														 'Ó' => 'O','Ń' => 'N','Ł' => 'L','Ź' => 'Z','Ż' => 'Z',
														 '!' => '1','@' => '2','#' => '3','$' => '4','%' => '5', 
														 '^' => '6','&' => '7','*' => '8','(' => '9',')' => '10'));   											 
	}	

	public function test() 
	{	
		if ($this->_size > $this->_maxFileSize) {
			$temp = str_replace(':max', $this->_maxFileSize , $this->_errorMessages['size']);
			$temp = str_replace(':value' ,$this->_size, $temp);
			$this->_errors[] = $temp;
		}			
		if (array_search($this->_type, $this->_allowedTypes) === false) {
			$this->_errors[] = str_replace(':type', $this->_type, $this->_errorMessages['type']);
			//$this->_errors[]=$this->_errorMessages['type'];
		}	
		
		if ($this->_name == '' && $this->_tmpName == '') {
			$this->_errors[] = $this->_errorMessages['noImage'];
		}
		
		if($_FILES[$this->_fieldName]['error'] != UPLOAD_ERR_OK) {
			$this->_errors[] = $this->_errorMessages['error'];	
		}
		//print_r($this->_errors);
		//return $result;
	}
	
	private function move()
	{
		$path = $this->_path.'temp/' . $this->_prepareName();
		//echo 'PATH='.$path;
		$result = move_uploaded_file($_FILES[$this->_fieldName]['tmp_name'], $path);	
	
		if ($result == false || $_FILES[$this->_fieldName]['tmp_name'] == '') 
			$this->_errors[] = $this->_errorMessages['move'];
		return $result;
	}
	
	public function deleteTmp()
	{
		$path = $this->_path.'temp/' . $this->_prepareName();
		//echo $path;
		if (file_exists($path)) 
		{
			//echo 'exist';
			unlink($path);
		}
	}
	
	public function rename()
	{
		if(! rename($this->_path.'temp/'.$this->_filename, $this->_path.$this->_filename) )
		{
			$this->_errors[] = $this->_errorMessages['move'];
		}
	}
	
	
	public function addTemp()
	{
		$this->test();
		if ($this->errorExists()) 
		{
			//$this->_request->redirectFailure($this->getErrors());
			return null;
		}
		$this->move();
		if ($this->errorExists()) 
		{
			return null;
		}
		$this->_filename = $filename= $this->getSaveName();
		return $filename;
	
	
	}
	
	public function getData()
	{
		$data['name']=$this->_filename;
		$data['size']=$this->_size;
		//usuwamy kropkę z początku
		$data['extension']=substr($this->_extension,1,strlen($this->_extension));
		return $data;
	}
		
	protected function _prepareName()
	{
		$random = sha1( mktime() );	
		$name = str_replace(':name', $this->_profiledName, $this->_saveAs);
		//$name = str_replace(':ext', $this->_extension, $name);	
		$name = str_replace(':date', date($this->_date), $name);	
		$name = str_replace(':random', substr($random, 0, $this->_random), $name);			
		if (count($this->_variables > 0)) {
			foreach ($this->_variables as $k => $v)
				$name = str_replace($k, $v, $name);
		}		
		return $name;
	}
	
	/* SETTERY */
	public function setErrorMessage($k, $v) 	{ $this->_errorMessages[$k] = $v; 		}
	public function setErrorMessages($messages) { $this->_errorMessages = $messages; 	}	
	public function setAllowedTypes($types) 	{ $this->_allowedTypes = (array)$types; }
	public function setMaxSize($size)   		{ $this->_maxFileSize = $size; 				}			
	public function setDate($format)   			{ $this->_date = $format; 				}
	public function setRandom($format) 			{ $this->_random = $format; 			}
	public function setPath($path) 				{ $this->_path = $path; 				}		
	public function setSaveAs($format, $variables = array())  		
	{ 
		$this->_saveAs = $format;
		$this->_variables = (array)$variables;			
	}
	
	/* GETTERY */
	public function getName()     	{ return $this->_name;     		}
	public function getTmpName()  	{ return $this->_tmpName;  		}
	public function getType()     	{ return $this->_type;     		}
	public function getErrors()   	{ return $this->_errors;   		}
	public function getSize()     	{ return $this->_size;     		}
	public function getExtension()	{ return $this->_extension;		}
	public function getX()		  	{ return $this->_x;				}
	public function getY()		 	{ return $this->_y;				}
	public function getSaveName() 	{ return $this->_prepareName(); }
	public function getFullPath()	{ return $this->_path . $this->_prepareName(); }
	
	public function errorExists()   { 
		if (count($this->_errors) > 0) return true;
		return false;
	}
}

?>
