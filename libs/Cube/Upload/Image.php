<?php

class Cube_Upload_Image extends  Cube_Upload_Upload
{
	/* RESIZE */
	private $_x;				   // szerokość obrazka	
	private $_y;				   // wysokość obrazka
	private $_sourceImage;	   	   // handler obrazka
	
	
	
	protected $_allowedTypes = array('image/jpeg', 'image/gif', 'image/png', 'image/pjpeg', 'image/bmp', 'image/x-png');
	//protected $_maxFileSize  = 10240;
	
	public function __construct($name,Cube_Upload_File $file)
	{
		Cube_Loader::loadClass('Cube_Upload_Image');
		parent::__construct($name,$file);
		$this->setAllowedTypes($this->_allowedTypes);
	}
	
	public function init()
	{
		$path = $this->_path.'temp/' . $this->_prepareName();
		//echo $path;
		switch($this->_type)
		{
			case 'image/pjpeg':
			case 'image/jpeg': $img = imagecreatefromjpeg($path); break;
			case 'image/gif':  $img = imagecreatefrompng($path);  break;
			case 'image/png':  $img = imagecreatefromgif($path);  break;
		}
			
		$this->_x = imagesx($img);
	   	$this->_y = imagesy($img);	
		$this->_sourceImage = $img;	
	}
	
	public function resizeImages($filename)
	{
		$directorys=$this->_file->getDir();
		
		foreach ($directorys as $dir=>$value)
		{
			$height = $value['height'];
			$width= $value['width'];
			
			//echo 'dir='.$dir;
			//echo 'height='.$height;
			//echo 'width='.$width;		
			
			if ($this->_x > $width || $this->_y > $height) 
			{
				if (!$this->resize($width, $height, $this->_path.$dir)) 
					$this->_errors[] ='Nie udało się zmniejszyć zdjęcia do rozmiarów '.$height.'x'.$height;
			}
			else rename($this->_path.'temp/'.$filename, $this->_path.$dir.$filename);		
		} 
	}
	
	
	public function resize($width, $height, $path, $const = false) 
	{
		if ($const) {
			$new_width  = $width;
            $new_height = $height;
		} else {	
	    	if($this->_x > $this->_y) {
	            $new_width  = $width;
	            $new_height = ceil($width * ($this->_y/$this->_x));
	        }
	        if($this->_x < $this->_y) {
	            $new_width  = ceil($height * ($this->_x/$this->_y));
	            $new_height = $height;
	        }
	        if($this->_x == $this->_y) {
	            $new_width  = $width;
           	 	$new_height = $height;
	        }		
		}
		
		if ($new_width > $width) {
			$new_width  = $width;
	        $new_height = ceil($width * ($this->_y/$this->_x));
		}
		
		if ($new_height > $height) {
			$new_width  = ceil($height * ($this->_x/$this->_y));
	           $new_height = $height;
		}
		
		
	        $newImage = imagecreatetruecolor($new_width, $new_height);
			//imagecopyresized($newImage, $this->_sourceImage, 0, 0, 0, 0, $new_width, $new_height, $this->_x, $this->_y);
	 		imagecopyresampled($newImage, $this->_sourceImage, 0, 0, 0, 0, $new_width, $new_height, $this->_x, $this->_y);
	 
	 		$path = $path.$this->_prepareName();
		
	 		switch($this->_type)
			{
				case 'image/gif':  	$result = imagegif($newImage, $path); break;
				case 'image/png': 	$result = imagepng($newImage, $path, 100); break;
				default: 			$result = imagejpeg($newImage, $path, 100); break;
			}	
	        return $result;
	}
	
		
	/* GETTERY */
	public function getX()		  	{ return $this->_x;				}
	public function getY()		 	{ return $this->_y;				}
	
	
	
}

?>
