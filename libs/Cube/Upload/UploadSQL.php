<?php

class Cube_Upload_UploadSQL 
{
	private $_model= null;
	private $_notNullField=array('id_element','module','category','name');
	private $_data=array();
	
	public function __construct()
	{
		$this->_model= new Upload();	
	}
	
	
	private function valid()
	{
		foreach( $this->_data as $field => $value )
		{
			if ( is_numeric(array_search($field,$this->_notNullField)) )
			{
				if( is_null($value) || ($value=='') )
					throw new Cube_Exception(" W tabeli Upload pole \"$field\" nie moze byc NULL lub puste");
			}
		}
	
	}
	
	private function createData(Cube_Upload_Upload $upload, Cube_Upload_File $file )
	{
		$this->_data=array_merge($upload->getData(),$file->getData());
		$this->_data['add_date'] = time();
	}
	
	public function insert(Cube_Upload_Upload $upload, Cube_Upload_File $file )
	{
		$this->createData($upload,$file);
		$this->valid();
				
		$this->_model->insert($this->_data);		
	}
	
	public function update(Cube_Upload_Upload $upload, Cube_Upload_File $file, $id_photo )
	{
		$this->createData($upload,$file);
		$this->valid();
				
		$this->_model->update($id_photo,$this->_data);		
	}
	
	public function deleteOne($idPhoto)
	{
		$rows=$this->_model->getAll('id='.$idPhoto);
		$this->_model->deleteOne($idPhoto);
		return $rows;
	}
	
	
	public function delete($id,$module,$category)
	{
		$rows=$this->_model->getAll('id_element='.$id.' AND module="'.$module.'" AND category="'.$category.'"');
		$this->_model->delete($id,$module,$category);
		
		return $rows;
	}
	
	public function getFilename($id)
	{
		$data=$this->_model->get($id);
		return $data['name'];	
	}
	
	
	
}

?>
