<?php

//KLASA dla zdjec ktore beda korzystac z klasy IMAGE (maja rozne katalogi i rozne rozmiary)
class Cube_Upload_NoPhoto extends  Cube_Upload_File
{
	private $_categoryClass=null;
	private $_request = null;
	private $_upload=null;
	
	//do ustawienie przez potomka po utworzeniu konstruktora Photo
	//protected $_path=null;
	protected $_maxSize=null;
	protected $_date=null;
	protected $_saveAs=null;
	protected $_allowedTypes=null;
	
	public function __construct($module,$request,$categoryClass) 
	{
		$this->_categoryClass=$categoryClass;
		$this->_request=$request;
		parent::__construct($module,$this->_categoryClass);
		//TYLKO DLA KLAS KORZYSTAJACYCH Z IMAGE.
		//$this->setDir();		
		
	}	

	public function insert($id)
	{
		$this->_id=$id;
		$this->_uploadSQL->insert($this->_upload,$this); 
		
	}
	
	public function add($name)
	//public function addPhoto($id_element,$name)
	{
		$this->_upload=$upload = new Cube_Upload_Upload($name,$this);
		if(! is_null($this->_maxSize) )	
			$upload->setMaxSize($this->_maxSize);
		if(! is_null($this->_date) )		
			$upload->setDate($this->_date);
		if(! is_null($this->_saveAs) )			
			$upload->setSaveAs($this->_saveAs);	
		if(! is_null($this->_allowedTypes) )
				$upload->setAllowedTypes($this->_allowedTypes);		
			
		
		
		if ( is_null($filename=$upload->addTemp()) )
		{
			$this->_request->redirectFailure($upload->getErrors());
		}
		//$upload->init();
		//$image->resizeImages($filename); //lub rename
		$upload->rename(); 
		if ($upload->errorExists())
			$this->_request->redirectFailure($upload->getErrors());
		//usuwa tymczasowe zdjecie
		$upload->deleteTmp();
		
	}
	
	public function update($id,$id_photo)
	{
		//usun edytowane zdjecie
		$filename=$this->_uploadSQL->getFilename($id_photo);
		$this->_deleteFile($filename);
		
		//zmien wpis w bazie na nowe zdjecie
		$this->_id=$id;
		$this->_uploadSQL->update($this->_upload,$this,$id_photo);
	}
	
	public function deleteOne($id_photo)
	{
		$rows=$this->_uploadSQL->deleteOne($id_photo);
		if( count($rows)>0 )
		{
			$row=$rows[0];
			$filename=$row['name'];	
			$this->_deleteFile($filename);
		} 
	}
	
	public function deleteAll($id)
	{
		//usuniecie wszystkich zdjec nalezacych do jednego elementu
		$rows=$this->_uploadSQL->delete($id,$this->_module,$this->_category);
		if( count($rows)>0 )
		{
			foreach( $rows as $row )
			{
				$filename=$row['name'];	
				$this->_deleteFile($filename);
			}
		} 
	}
	
	private function _deleteFile($filename)
	{
		if( !is_null($filename) || $filename!='' ); 
		{		
			$path=$this->_path;
			//$directorys=$this->_dir;
		
			//foreach ($directorys as $dir=>$value)
			{
				if (file_exists($this->_path.$filename))
					unlink($this->_path.$filename);
			}
		}		
		
	}
	
		
	public function deleteAllPhotoWithModule()
	{
		//usuniecie wszystkich zdjęć należących do modulu
	}
}

?>
