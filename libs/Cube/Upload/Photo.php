<?php

//KLASA dla zdjec ktore beda korzystac z klasy IMAGE (maja rozne katalogi i rozne rozmiary)
class Cube_Upload_Photo extends  Cube_Upload_File
{
	private $_categoryClass=null;
	private $_request = null;
	private $_image=null;
	
	//do ustawienie przez potomka po utworzeniu konstruktora Photo
	//protected $_path=null;
	protected $_maxSize=null;
	protected $_date=null;
	protected $_saveAs=null;
	protected $_allowedTypes=null;
	
	public function __construct($module,$request,$categoryClass) 
	{
		$this->_categoryClass=$categoryClass;
		$this->_request=$request;
		//Cube_Loader::loadClass('Cube_Upload_Photo');
		parent::__construct($module,$this->_categoryClass);
		//TYLKO DLA KLAS KORZYSTAJACYCH Z IMAGE.
		$this->setDir();		
		
	}	

	public function insert($id)
	{
		$this->_id=$id;
		$this->_uploadSQL->insert($this->_image,$this); 
		
	}
	
	public function add($name)
	//public function addPhoto($id_element,$name)
	{
		$this->_image=$image = new Cube_Upload_Image($name,$this);
		if(! is_null($this->_maxSize) )	
			$image->setMaxSize($this->_maxSize);
		if(! is_null($this->_date) )		
			$image->setDate($this->_date);
		if(! is_null($this->_saveAs) )			
			$image->setSaveAs($this->_saveAs);	
		if(! is_null($this->_allowedTypes) )
				$image->setAllowedTypes($this->_allowedTypes);		
			
		
		
		if ( is_null($filename=$image->addTemp()) )
		{
			$this->_request->redirectFailure($image->getErrors());
		}
		$image->init();
		$image->resizeImages($filename); //lub rename
		if ($image->errorExists())
			$this->_request->redirectFailure($image->getErrors());
		//usuwa tymczasowe zdjecie
		$image->deleteTmp();
		
	}
	
	public function update($id,$id_photo)
	{
		//usun edytowane zdjecie
		$filename=$this->_uploadSQL->getFilename($id_photo);
		$this->_deleteFile($filename);
		
		//zmien wpis w bazie na nowe zdjecie
		$this->_id=$id;
		$this->_uploadSQL->update($this->_image,$this,$id_photo);
	}
	
	public function deleteOne($id_photo)
	{
		$rows=$this->_uploadSQL->deleteOne($id_photo);
		if( count($rows)>0 )
		{
			$row=$rows[0];
			$filename=$row['name'];	
			$this->_deleteFile($filename);
		} 
	}
	
	public function deleteAll($id)
	{
		//usuniecie wszystkich zdjec nalezacych do jednego elementu
		$rows=$this->_uploadSQL->delete($id,$this->_module,$this->_category);
		if( count($rows)>0 )
		{
			foreach( $rows as $row )
			{
				$filename=$row['name'];
				//echo $filename;
				$this->_deleteFile($filename);
			}
		} 
	}
	
	private function _deleteFile($filename)
	{
		if( !is_null($filename) || $filename!='' ); 
		{		
			$path=$this->_path;
			$directorys=$this->_dir;
		
			foreach ($directorys as $dir=>$value)
			{
				if (file_exists($this->_path.$dir.$filename))
				{
					//echo $this->_path.$dir.$filename;
					unlink($this->_path.$dir.$filename);
				}
			}
		}		
		
	}
	
		
	public function deleteAllPhotoWithModule()
	{
		//usuniecie wszystkich zdjęć należących do modulu
	}
}

?>
