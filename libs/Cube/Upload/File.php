<?php

class Cube_Upload_File
{
	protected $_module=null; 
	protected $_category=null;
	protected $_id=null; //- id elementu
	protected $_title=null;
	protected $_dir = array(); //tabela zawierajaca info o katalogach i rozmiarach zdjec w tych katalogach
	protected $_path = 'public/'; 
	protected $uploadSQL = null; // referencja do klasy oblugujacej operacje na tablicy update
	
	public function __construct($module,$category) 
	{
		$this->_module=$module;
		$this->_category=$category;
		$this->_path.=$module.'/'.$category.'/';
		$this->_uploadSQL = new Cube_Upload_UploadSQL();
		
	}	

	public function getData()
	{
		$data['module']=$this->_module;
		$data['category']=$this->_category;
		$data['id_element']=$this->_id;
		$data['title']=$this->_title;
		return $data;
	}
	
	
	public function getPath()
	{
		return $this->_path;
	}
	
	
	public function getCategory()
	{
		return $this->_category;
	}
	
	public function getModule()
	{
		return $this->_module;
	}
	
	public function getDir()
	{
		return $this->_dir;
	}

		
	public function setDir()
	{
		//pobierz info o katalogach i rozmiarach zdjec
		$model = new photosConfig();
		$module=$this->_module;
		$row=$model->getAll('module="'.$module.'"');	

		if( count($row) < 1)	
			throw new Cube_Exception("Brak konfiguracji zdjec dla modulu \"$module\"");
			
		foreach ($row as $r)
		{
			$dir=$r['directory'];
			//$this->_dir[]=$dir;
			$this->_dir[$dir]['height']=$r['height'];
			$this->_dir[$dir]['width']=$r['width'];
		}	
	}
	
	public function setTitle($title)
	{
		$this->_title=$title;
	}
	
	
	/*public function setID($id)
	{
		$this->_id=$id;
	}
	*/
	
	
	
}	

?>
