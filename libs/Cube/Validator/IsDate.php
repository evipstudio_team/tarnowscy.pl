<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
require_once 'Cube/Validator/Interface.php';

class Cube_Validator_IsDate implements Cube_Validator_Interface
{

    public function __construct($params)
    {
    }

    public function validate($value)
    {
		$wynik=array();
		if (!preg_match('/^(\d\d)-(\d\d)-(\d\d\d\d)$/',$value,$wynik)) 
			return false;
		else
			return( checkdate($wynik[2],$wynik[1],$wynik[3]) );	
		//return true;
	}
}
