<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_Language
{
	private $_ModulesNotLang; //moduły nie podlagająze jezykowaniu
	private $_InstallModules; //zainstalowane moduly jezykowane aktywne i nie aktywne
	private $_ActiveLang; //aktywne jezyki
	private $_StructLang;	  //struktura zaierajaca info o modulach i przyporzadkowanych do nich na podstawie pliku modules info o tabalach i polach podlegajacych jezykowosci
	
	//private $_ModulesAccess; //info o uprawnieniach jezykowych dla kazdego modułu zapisanych w bazie danych.
	
	public function __construct()
	{
		/*$this->_InstallModules=array();
		$this->_StructLang=array();
		$this->_ActiveLang=array();
		
		$model = new Langs();
		$this->_ActiveLang = $model->getAll('active=1','id');
		$this->_ModulesNotLang = Cube_Registry::get('ModulesNotLang');
		$model = new Modules();
		$where_not_lang=$this->whereNotLangs();
		$this->_InstallModules=$model->getAllModules($where_not_lang, 'name');
		if(sizeof($this->_InstallModules) )
		{
			foreach($this->_InstallModules as $mod)
			{
				$name=$mod['name'];
				$tableslang=$this->getTablesLangFromFile($name);
				if($tableslang)
					//jezeli pole w pliku jest ustawione
					$this->_StructLang[$name]=$tableslang;
			}
							
		}*/
				
	}
	
	public function getActiveLang()
    {
		//print_r($this->_ActiveLang);
		return $this->_ActiveLang;
	}
	
	public function getFieldsLang($modules, $table)
	{
		$tables=array();
		foreach($this->_StructLang[$modules] as $key=>$mod)
		{
			if(! strcmp($mod['table'],$table))
			{
				//print("Wynik  funkcji print_r:<BR><pre>");
				return($mod['field']); 
			}	
		}
	}
	
	public function getTablesAndFieldLang($modules)
	{
		return $this->_StructLang[$modules];	
	}
	
	
	public function getTablesLang($modules)
	{
		$tables=array();
		foreach($this->_StructLang[$modules] as $mod)
		{
			$tables[]=$mod['table'];
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($tables);
		//print("</pre><BR>");
		return $tables;
	}
	
	
	public function getModulesLang()
	{
		$modules=array_keys($this->_StructLang);
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($modules);
		//print("</pre><BR>");
		return $modules;
	}
	
	
	public function getStructLang()
	{
		return $this->_StructLang;
		 
	}
	
	private function getTablesLangFromFile($name)
	{
		$name = strtolower($name);
		if (!file_exists("system/modules/$name.php")) return;
			include "system/modules/$name.php";
		if(isSet($moduleTablesLang))
		{
			//print("Wynik  funkcji getTablesLang:<BR><pre>");
			//print_r($moduleTablesLang);
			//print("</pre><BR>");
			return $moduleTablesLang;
			//return $name;
			//return 'TEST';
		}
		else
			return null;
	}

	public function whereNotLangs($synonim=null)
	{
		$ModulesNotLang=$this->_ModulesNotLang;
		$where = null;
		if(!is_null($synonim))
			$synonim.='.';	//dodaj kropke do synonimu tabeli
		foreach($ModulesNotLang as $mnl)
		{
			$where.=$synonim.'name <> "'.$mnl.'" AND ';	
		}
		$where=substr($where,0,-5);
		
		return $where;
	}	
	
	public function getInstallModules()
	{
		return $this->_InstallModules;
	}
	
	
	public function getModulesNotLang()
	{
		return $this->_ModulesNotLang;
	}
	
	/*public function getModulesAccess($idLang)
	{
		$model = new Langs();		
		$where_not_lang=$this->whereNotLangs('m');
		$where=$where_not_lang.' AND l.id='.$idLang;
		$this->_ModulesAccess = $model->getModules($where, 'm.name');
		return $this->ModulesAccess;
	}*/
	
	
	
	
	public function selectAdminLanguages($select,$module)
	{
		//tworzy selecta do wyboru jezykow
				
		$model=new Langs();
		//pobierz jezyki nalezace do modulu
		$moduleLanguages=$model->getLanguages('m.name="'.$module.'" AND l.active=1');
		if( count($moduleLanguages) < 1)
			//jezrzeli nie ma jezykow w module zwroc polski.
			return null;
		
		$new_html=$select;
		
		//tworz selecta
		foreach ($moduleLanguages as $modLang)
		{
				
			$new_html.='<option value="'.strtolower($modLang['name']).'">'.$modLang['name'].'</option>';
			
		}
		$new_html.='</select>';
		
		return $new_html;
	}
	
	public function filterAdminLanguages($filters,$module)
	{
		$new_filter=array();
		$model=new Langs();
		//pobierz jezyki nalezace do modulu
		$moduleLanguages=$model->getLanguages('m.name="'.$module.'" AND l.active=1');
		if( count($moduleLanguages) < 1)
			//jezrzeli nie ma jezykow w module zwroc polski.
			return $filters;
	  
		foreach ($moduleLanguages as $modLang)
		{
			$short_name=strtolower($modLang['short_name']);	
			foreach ($filters as $key=>$f)
			{
				$new_key=$key.'_'.$short_name;
				$new_filter[$new_key]=$f;
			}
		}
		/*print("Wynik  funkcji print_r:<BR><pre>");
		print_r($new_filter);
		print("</pre><BR>");
		print("Wynik  funkcji filters:<BR><pre>");
		print_r($filters);
		print("</pre><BR>");*/
		return array_merge($filters,$new_filter);
	
	}
	
	public function divAdminLanguages($html, $module, $table )
	{
		//Tworzy dodatkowe div jezykowe
		//$html - kod html z wersja polska. UWAGA Kazdy wiersz (input i label) musi byc otoczony oddzielnym divem <div> <div/> 
		//$modules - modul z ktorego pochodza pola. (Zmienna $moduleTablesLang w odpowiednim katalogu modules)
		//$tables - tabele $moduleTablesLang w odpowiednim katalogu modules 
		
		//echo 'module='.$module;
		//echo 'table='.$table;
		
		//wartoscj typ $zmienna['pole'] na  '.$zmienna['pole'].' aby moznabylo wyciagnoac wartosc
		$html=preg_replace('/(\$.*\])/', '\'.$1.\'', $html);
		
		$model=new Langs();
		//pobierz jezyki nalezace do modulu
		$moduleLanguages=$model->getLanguages('m.name="'.$module.'" AND l.active=1');
		//print_r($moduleLanguages);
		if( count($moduleLanguages) < 1)
			//jezrzeli nie ma jezykow w module zwroc polski.
			return $html;
		
		$fields=$this->getFieldsLang($module, $table);
		if( count($fields) < 1)
			//jezeli nie ma pol do jezykowosci to zwroc tylko jezyk polski. EXEPTION
		return $html;
		
		//div dla jezyka polskiego
		$new_html.='<div id="polski">';
		$new_html.='<div><label for="language"><span class="b">Pola dla wesji językowej:polski</span></label></div>';
		
		//wartoscj typ $zmienna['pole'] na  '.$zmienna['pole'].' aby moznabylo wyciagnoac wartosc
		//$new_html.=preg_replace('/(\$.*\])/', '\'.$1.\'', $html);
		$new_html.=$html;
		$new_html.='</div>';
		
		//div dla jezykow obcych
		foreach($moduleLanguages as $modLang)
		{
			//$html_field=$html;
			$replace=null;
			$short_name=strtolower($modLang['short_name']);
			$name=strtolower($modLang['name']);
			
			$new_html.='<div id="'.$name.'">';
					
			foreach ($fields as $field)
			{
				$replace[]=$field.'_'.$short_name;	
			}
			//print_r($replace);
			//print_r($fields);
			
			//popraw html ze wzgledu na pola
			$html_field=stro_replace($fields,$replace,$html);
			
			//wartoscj typ $zmienna['pole'] na  '.$zmienna['pole'].' aby moznabylo wyciagnoac wartosc
			//$html_field=preg_replace('/(\$.*\])/', '\'.$1.\'', $html_field);
			
			$html_lang_version='<div><label for="language"><span class="b">Pola dla wesji językowej:'.$name.'</span></label></div>
			'; //UWAGA ten apostrof musi byc w nowej linii.
			
			$html_field=$html_lang_version.$html_field;
			//przed divami dopisz display none
			$new_html.=preg_replace('/<div>(.*)<\/div>/', '<div style="display: none;">$1</div>', $html_field);
			//$new_html.=$html_field;	
			$new_html.='</div>';
			
			
		}
		//echo htmlspecialchars($new_html);
		return $new_html;

	}

	
	
	
	
	
}

?>
