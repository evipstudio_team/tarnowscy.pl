<?php

/**
 *	CMS for Evipstudio
 *	Author: Switez
 *  04-06.2011 
 */ 

class Cube_SearchOrder_Field
{
	private $_name = NULL; // nazwa pola 
    private $_sql = NULL; // zapis po kotorym bedziemy sie odwolywac do bazy danych
    

    public function __construct($name, $sql) 
    {
    	$this->_name = $name;	
		$this->_sql = $sql;
		
	}
		
	public function getName()
	{
		return $this->_name;
	}
	
	public function getSql()
	{
		return $this->_sql;
	}
	
    public function __get($nazwa)
    {
         throw new Cube_Exception("Dla obiektu Cube_Field nie znaleziono pola \"$nazwa\"");
         
    } 
}

?>
