<?php

/**
 *	CMS for Evipstudio
 *	Author: Switez
 *  04-06.2011 
 */ 

class Cube_SearchOrder_Order extends  Cube_SearchOrder_Abstract
{
	private $_fields = array(); // tablica obiektow Fields. Tablica pól wykorzystywanych przez search. 
    private $_search = NULL; 	// obiekt search, lub rozszerzony obiek search np. search_date
    private $_status = NULL;	//status sortowanej kolumny (0 lub 1)
	private $_column = NULL;	//nazwa sortowanej kolumny
	private $_link = NULL;		//poczatek linka ktory bedzie wrzucowny do href
	private $_img_up=NULL;		//link do zdjecia gdy srtujemy rosnaco
	private $_img_down=NULL;	//link do zdjecia gdy sortujemy malejaco
    
     
    public function __construct($status,$field,$link,$image_up,$image_down,Cube_SearchOrder_Search $search=NULL) 
    {
    	$this->_search= $search;
    	$this->_link=$link;
    	$this->_status=$status;
    	$this->_column= $field;
    	$this->_img_down=$image_down;
    	$this->_img_up=$image_up;
    	//$this->_fields = new ArrayIterator(); 
 	}
		
	public function addField(Cube_SearchOrder_Field $field)
	{
		$this->_fields[]=$field;
		
	}
	
	public function getFieldSql()
	{
		if( is_null($this->_column) )
		{
			throw new Cube_Exception("Nie mozna pobrac wartosci FieldSQL poniewaz nie zostala wybrana kolumna do sortowania");
		}
		return $this->search($this->_fields,$this->_column);
		/*foreach($this->_fields as $field)
		{
			if(! strcmp($field->getName(),$this->_column) )
			{		
				return $field->getSql();	
			}
		}*/	
	}
	
	
	public function createWhere()
	{
		if( is_null($this->_search) )
			throw new Cube_Exception("Nie mozna pobrac wartosci where poniewaz obiekt Search nie zostal przekazany do obiektu Order");
		else
			return $this->_search->createWhere();
	}
	
	public function getLinkPaging()
	{
		$link==null;
		$link=$this->_link.',column_'.$this->_column.',state_'.$this->_status.',search_'.$this->_search->getSearchString();
		return $link;
	}	
	
	
	public function getLink($col)
	{
		$search=NULL;
		if(! is_null($this->_search) )
			$search=',search_'.$this->_search->getSearchString();
		$status=$this->getStatus($col);
		$link=$this->_link.',sort,column_'.$col.',state_'.$status.$search;
		//"zgloszenia,sort,column_1,state_'.$botton_state[1].',search_'.$search_string.'
		return $link;
	}
	
	public function getStatus($col)
	{
		//$this->check_column($col);
		if( !strcmp($col,$this->_column) )
		{
			//jezeli pole sortowane to zamien status
			$status=$this->_status;	
			if( $status==1 )
				return 0;
			else if( $status==0 )
				return 1;
			//else
			// throw new Cube_Exception("Dla obiektu Cube_Order status musi byc w przedziale 0-1. Obecna wartosc to \"$status\"");
		}
		else
			//jezeli to nie sortowane pole to zwroc 0
			return 0;
	}
	
	public function getOrder()
	{
		$status=$this->_status;	
		if( $status == 1 )
			return 'DESC';
		else if( $status == 0 )
			return 'ASC';
		else
		 throw new Cube_Exception("Dla obiektu Cube_Order status musi byc w przedziale 0-1. Obecna wartosc to \"$status\"");
	}
	
	
	
	public function getImageStatus($col)
	{
		$this->check_column($this->_fields,$col);
		if( !strcmp($col,$this->_column) )
		{
			//jezeli pole sortowane to zamien status
			$status=$this->_status;	
			if( $status==1 )
				return $this->_img_up;
			else if( $status==0 )
				return $this->_img_down;
			else
			 throw new Cube_Exception("Dla obiektu Cube_Order status musi byc w przedziale 0-1. Obecna wartosc to \"$status\"");
		}
		else
			//jezeli to nie sortowane pole to zwroc 0
			return null;
	}
	
	
	
	public function __get($nazwa)
    {
         throw new Cube_Exception("Dla obiektu Cube_Field nie znaleziono pola \"$nazwa\"");
         
    } 
}

?>
