<?php
/**
 *	CMS for Evipstudio
 *	Author: Switez
 *  06-06.2011 
 */ 

abstract class Cube_SearchOrder_Abstract
{
	
	public function search($list_objects, $col)
	{
		foreach($list_objects as $field)
		{
			if(! strcmp($field->getName(),$col) )
			{		
				return $field->getSql();	
			}
		}
		$object=get_class($field);
		throw new Cube_Exception("Pole \"$col\" nie zostało wyszukane w obiekcie \"$object\"");	
	}
	
	public function check_column($list_objects, $col)
	{
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($this->_fields);
		//print("</pre><BR>");
		foreach($list_objects as $field)
		{
			if(! strcmp($field->getName(),$col) )
			{		
				return;
			}
		}	
		//jelzei nie znajdzie kolumny we wszystkich nazwach pol
		$object=get_class($field);
		throw new Cube_Exception("W obiekcie \"$object\" nie ma pola \"$col\"");
	}
	
}
?>
