<?php

/**
 *	CMS for Evipstudio
 *	Author: Switez
 *  04-06.2011 
 */ 

class Cube_SearchOrder_Search
{
	private $_fields = array(); // tablica obiektow Fields. Tablica pól wykorzystywanych przez search. 
    private $_search_string = NULL; // szukany string
    

    public function __construct($string) 
    {
    	$this->_search_string = $string;	
 	}
		
	public function getSearchString()
	{
		return $this->_search_string;
	}
	
	public function addField(Cube_SearchOrder_Field $field)
	{
		$this->_fields[]=$field;
		
	}
	
	public function createWhere($search=null)
	{
		if(is_null($search)) 
			$search=$this->_search_string;
		else
			$this->_search_string=$search;
		
		$where='(';
		//print_r($this->_fields);
		foreach($this->_fields as $field)
		{
			//utworz Like.
			//$where_id='p.id like "%'.$search.'%"';
			$string_like=$field->getSql().' like "%'.$search.'%"';
			$where.=$string_like.' OR ';	
			
		}
		$where = substr($where, 0, -4);
		return $where.')';
			
	}
	
	
    public function __get($nazwa)
    {
         throw new Cube_Exception("Dla obiektu Cube_Field nie znaleziono pola \"$nazwa\"");
         
    } 
}

?>
