<?php 

class Cube_Mail{

	private $hash = null;
	private $hash2 = null;

	public $is_html = null;
	public $attach = null;
	public $types = array();
	public $names = array();

    function __construct(){
        $this->hash = time();
        $this->hash2 = time() + 2;
    }
    
    
    public function send($from, $to, $subject, $contents, $attach = null){
       
       	$tmp = $contents;
       	$contents = str_replace('="', '=3D"', $tmp);
       

		$headers = "From:   $from \r\n"
					."Reply-To: $from \r\n"
					."MIME-Version: 1.0 \r\n"
					."Content-Type: multipart/mixed; \r"
					."boundary=\"$this->hash\"\r\n\r\n";
        		 
        $content = "--$this->hash\r\n"
					."Content-Type: multipart/alternative; \r boundary=\"$this->hash2\"\r\n\r\n"
					."--$this->hash2\r\n";

		$content .= "Content-Type: text/html; charset=UTF-8 \r\n"
					."Content-Transfer-Encoding: quoted-printable\r\n\r\n";
					

		$content .="$contents\r\n"
					."--$this->hash2--\r\n"
					."--$this->hash\r\n";
			
		
		
		if (count($this->attach) > 0) {
			for($i=0; $i < count($this->attach); $i++){
				$a = chunk_split(base64_encode(file_get_contents($this->attach[$i])));
				$name = $this->names[$i];
				$type = $this->types[$i];
			
				if ($i >= 1){
					$content .= "--$this->hash\r\n";
				}
			
				$content .="Content-Type: $type;\r\n"
 					."name=\"$name\"\r\n"
					."Content-Transfer-Encoding: base64\r\n"
					."Content-ID: <$name>\r\n\r\n"				
					."$a\r\n";
			}
		}
								
		$content .= "--$this->hash--";
		
				 
        $mail_sent = @mail( $to, $subject, $content, $headers );
		return $mail_sent ? true : false;
    }
    
}


?>
