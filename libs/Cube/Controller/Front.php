<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Controller/Abstract.php';
	
class Cube_Controller_Front
{
	private $_request = null;
	private $_controllersPaths = array('admin' => 'system/controllers/admin/', 'default' => 'system/controllers/');
	private $_defaults = array('module' => 'default', 'controller' => 'index', 'action' => 'index');
	
	private $_dispatchStatus = false;
	
	private function __clone() {}
	private function __construct($dbSettings) {}
	
	public static function getInstance($dbSettings = null)
	{
		static $instance = null;		
		if (is_null($instance)) $instance = new self($dbSettings);
		return $instance;		
	}
	
	public function getDefaultAction()
	{
		return $this->_defaults['action'];
	}
	
	public function setDefault($k, $v)
	{
		$this->_defaults[$k] = $v;
	}
	
	public function dispatch() 
	{
		if ($this->_dispatchStatus) return;
		$this->_dispatchStatus = true;
		
		if (is_null($this->_request)) {
			$request = $this->_request = Cube_Registry::get('request');
		}
		
		$session = Cube_Registry::get('session');
		
		while (!$request->isDispatched()) 
		{
			$request->setDispatched(true);
			// ładuje dane		
			$module     = $this->_request->getModule();
			$controller = $this->_request->getController();
			$action     = $this->_request->getAction();
			if (empty($module)) $module = $this->_request->setModule($this->_defaults['module']);
			if (empty($controller) || $controller == 'index') {
				if ($module == 'admin') {
					$this->_defaults['controller'] = DEFAULT_ADMIN_MODULE;
				} else {
					$config = Cube_Registry::get('config');
					$this->_defaults['controller'] = $config->get('default_module');
				}
				$controller = $this->_request->setController($this->_defaults['controller']);
			}
			if (empty($action))     $action     = $this->_request->setAction($this->_defaults['action']);
			
			//echo "uruchamianie: $controllerClassName, akcja: $actionName<br>";
			
			// zablokuj dostęp
			if ($module == 'admin') {
				if ($session->isLoggedIn() && ($session->getRole() === 'administrator' || $session->getRole() === 'root') && $controller != 'auth') {
					//To nie jest uzywane - Tomek
					/*$model = new Modules();
					//$row = $model->get($controller);// - oryginal ale nie dziala
					$row = $model->getByName($controller); //- Tomek
					if ($row['admin_access'] == 'root' && $session->getRole() == 'administrator') 
					{
						//echo 'Jestem w funkcji dispatch - zablokuj dostep';
						//exit;
						$controller = 'advnews';
					}*/	
				}
				else $controller = 'auth';
			}
			
			// nazwy i ściezki TODO
			if ($module != 'default') {
				$controllerClassName = ucfirst($module).'_'.ucfirst($controller).'Controller';
				$controllerName = ucfirst($controller).'Controller';
				$controllerPath = $this->_controllersPaths[$module].$controllerName.'.php';
			} else {
				$controllerClassName = ucfirst($controller).'Controller';
				$controllerPath = $this->_controllersPaths['default'].$controllerClassName.'.php';
			}
			$actionName = $action.'Action';
				
			if (!class_exists($controllerClassName, FALSE)) {		
				if (!is_readable($controllerPath)) {
					throw new Cube_Exception('Plik ' . $controllerPath . ' nie istnieje.');		
			        break;
			        //def
				}
				require_once $controllerPath;
				if (!class_exists($controllerClassName, FALSE)) {
					throw new Cube_Exception('FC Plik istnieje, ale klasa nie.');
					break;
					//def
				}		
			}
			
			$controller = new $controllerClassName($request, $this);
			if (!$controller instanceof Cube_Controller_Abstract) {
				throw new Cube_Exception('controller nie jest implementacja Cube_Controller_Abstract');
				break;
			}
			$controller->run($actionName);
			$controller = NULL;	
		}
	} 
	
	public function setRequest() {}
	
}	

?>
