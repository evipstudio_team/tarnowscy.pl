<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_View
{
	private $_language = null;
	private $_request = null;
	private $_session = null;
	private $_config = null;
	private $_templatesPath = 'templates/';
	private $_template = 'default';
	private $_theme = 'theme';	
	private $_blockPath = 'system/blocks/:blockBlock.php';
	private $_script;
	private $_render = true;
	private $_noOutput = false;
	
	public function __construct($request)
	{
		$this->_request = $request;
		$this->_session = Cube_Registry::get('session');
		$this->_config = Cube_Registry::get('config');
		$this->_language = Cube_Registry::get('language');
		$module = $request->getModule();
		
		if ($module == 'admin')	$this->setTemplate('admin');
		else $this->setTemplate($this->_session->getTheme());
			
		$this->_script = $request->getController() .'_'. $request->getAction() .'.tpl.php';				
	}
	
	public function setTemplate($template)
	{
		if ($template != '') $this->_template = $template;	
		else $this->_template = 'default';			
	}
	
	public function setTheme($theme)
	{
		$this->_theme = $theme;					
	}
		
	public function render($name, $noController = false)
	{
		if ($noController) $this->_script = $name .'.tpl.php';
		else $this->_script = $this->_request->getController() .'_'. $name .'.tpl.php';	
	}
	
	public function noRender($flag = false)
	{
		$this->_render = $flag;	
	}
	
	public function noOutput($flag = true)
	{
		$this->_noOutput = $flag;	
	}
		
	public function output()
	{
		// jezyki
		$moduleName = strtolower($this->_request->getController());
		$language = $this->_session->getLanguage();
		$language_temp = $this->_session->getLanguageTemp();
		$aciveLang=$this->_language->getActiveLang();
		//print_r($aciveLang);
		if (file_exists("languages/$language/general.php")) 
			require_once "languages/$language/general.php";
	//	else throw new Cube_Exception('Nie istnieje plik: "languages/'.$language.'/general.php"');
				
		if ($this->_request->getModule() == 'admin') {
			if (file_exists('languages/'.$language.'/'.$moduleName.'Admin.php'))
				require_once 'languages/'.$language.'/'.$moduleName.'Admin.php';
			elseif (file_exists('languages/pl/'.$moduleName.'Admin.php'))
				require_once 'languages/pl/'.$moduleName.'Admin.php';
		} else {
			if (file_exists('languages/'.$language.'/'.$moduleName.'.php'))
				require_once 'languages/'.$language.'/'.$moduleName.'.php';
		}
		
		// meta
		$title = $this->_config->get('title_'.$language);
		
		if (isset($this->title)) {
			$title = $this->title;
		}	
		
		$meta = '<title>'.$title.'</title>
		<meta name="description" content="'.$this->_config->get('description_'.$language).'" />
		<meta name="keywords" content="'.$this->_config->get('keywords_'.$language).'" />';
		
		$styles = null;
		if ($this->_request->getModule() == 'admin') {
			if (file_exists('templates/admin/styles/style_'.$language.'.css'))
				$styles = '<link rel="stylesheet" type="text/css" href="/templates/admin/styles/style_'.$language.'.css" />';
			$languagesDir = '/templates/admin/languages/'.$language.'/';
			$stylesDir = '/templates/admin/styles/';
			$imagesDir = '/templates/admin/images/';		
		} else {
			if (file_exists('templates/'.$this->_template.'/styles/style_'.$language.'.css'))
				$styles = '<link rel="stylesheet" type="text/css" href="/templates/'.$this->_template.'/styles/style_'.$language.'.css" />';		
			$languagesDir = '/templates/'.$this->_template.'/languages/'.$language.'/';
			$stylesDir = '/templates/'.$this->_template.'/styles/';
			$imagesDir = '/templates/'.$this->_template.'/images/';		
		}
			
		$footer = $this->_config->get('footer_'.$language);
		
		$tmp = explode('/', $_SERVER['REQUEST_URI']);
		$currentURL = $tmp[count($tmp)-1];
		
		if ($this->_request->getModule() != 'admin') 
		{
			//MENUS Tomek
			$model = new MenusTree();
			$allRows=$model->getAllwPhoto('m.active=1','m.id');
			foreach ($allRows as $row)
			{
				$menu_name = 'menu_'.$row['id'];
				$$menu_name = array();
		 		
				if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) 
				{
					//echo 'NOT LOGGED';
					//jezeli nie zalogowany to pobierz jego rodzica i dla tego rodzica pobierz jeszcze raz jego dzieci uwzgledniajac to ze nie zalogowany
					$menu_name = 'menu_'.$row['parent_id'];
					$$menu_name = array();
					$$menu_name=$model->getAllChildren($row['parent_id'],'active=1 AND logged=0');
					//continue;
				}
				else
					$$menu_name=$model->getAllChildren($row['id'],'active=1');
				/*if( empty($$menu_name) )
				{
					//jezeli nie ma dzieci to zwroc element
					$$menu_name=$model->get($row['id']);
				}*/
				
			}
			unset($allRows);
			unset($model);
				
			// MENUS
			/*$model = new Menus();
			$allMenus = (array)$model->getMenus();
			$allRows = (array)$model->getMenusRows();
			foreach ($allMenus as $menu)
		 	{
		 		$menu_name = 'menu_'.$menu['id'];
		 		$$menu_name = array();
		 		
		 		if ($menu['logged'] == 1 && !$this->_session->isLoggedIn()) 
					continue;
		 		
		 		foreach ($allRows as $row)
				{
					if ($row['mid'] == $menu['id']) {
						array_push($$menu_name, $row);
					}
				}	
		 	}
			unset($allMenus);
			unset($allRows);
			unset($model);*/
		}
		
		// rest
		if ($this->_noOutput) return;	
		if ($this->_render) {	
			ob_start();	
			include $this->_templatesPath.$this->_template.'/'.$this->_script;
			$controller = ob_get_contents();
			ob_clean();	
		} else $controller = null;
			
		ob_start();	
		include $this->_templatesPath.$this->_template.'/'.$this->_theme.'.tpl.php';
		$output = ob_get_contents();
		ob_clean();				
		echo $output;
	}

	public function getBlock($blockName)
	{
		$blockName = ucfirst($blockName); 
		$path = str_replace(':block', $blockName, $this->_blockPath);
		
		if (file_exists($path)) {
			require_once $path;
			$class = $blockName.'Block';
			if (class_exists($class)) {
				$obj = new $class();
				return $obj;
			} else throw new Cube_Exception('Plik '.$blockName.' istnieje, ale klasa nie.');
		} else throw new Cube_Exception('Blok '.$blockName.' nie istnieje.');
	}
	
	private function _getModules()
	{
		if ($this->_request->getModule() != 'admin') 
		return;	
		//Tomek
		$id=$this->_session->getUserId();
		$where='u.id='.$id;
		$model = new Modules();
		$modules = $model->getModules($where, 'm.pos ASC,m.name');
		
		//oryginalne
		/*switch ($this->_session->getRole())
		{
			case 'root': $where = '(admin_access = "root" OR admin_access = "administrator") AND active = 1'; break;
			case 'administrator': $where = 'admin_access = "administrator" AND active = 1'; break;
			default: return;
		}
		
		$model = new Modules();
		$modules = $model->getModules($where, 'admin_access, name');
		*/
		foreach ($modules as $module)
		{
			if ($this->_request->getModule() == 'admin') {
				if (file_exists('languages/'.$language.'/'.$module['name'].'Admin.php'))
					require_once 'languages/'.$language.'/'.$module['name'].'Admin.php';
				elseif (file_exists('languages/pl/'.$module['name'].'Admin.php'))
					require_once 'languages/pl/'.$module['name'].'Admin.php';
			} else {
				if (file_exists('languages/'.$language.'/'.$module['name'].'.php'))
					require_once 'languages/'.$language.'/'.$module['name'].'.php';
			}
		}
		
		return $modules;
	}
	
	private function _getActions($name)
	{
		if ($this->_request->getModule() != 'admin') return;
		$name = strtolower($name);
		if (!file_exists("system/modules/$name.php")) return;
		require "system/modules/$name.php";
		return $moduleActions;
	}			
}

?>
