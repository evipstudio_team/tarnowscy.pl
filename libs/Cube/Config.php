<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_Config
{
	private static $_config = null;
	private $configBySegment = array();
	private $configByKey = array();
	
	public function __construct() 
	{
		$stmt = mysql_query('SELECT * FROM config');
		
		while ($r = mysql_fetch_assoc($stmt))
		{
			$this->configBySegment[$r['segment']][] = array('id' => $r['id'], 'k' => $r['k'], 'v' => $r['v']);
			$this->configByKey[$r['k']] = array('id' => $r['id'], 'k' => $r['k'], 'v' => $r['v'], 'segment' => $r['segment']);
		}	
	}
		
	public function __clone() {}
	
	public static function getInstance()
	{
        if (self::$_config === null) {
			self::$_config = new self();
        }
      	return self::$_config;	
	}
	
	public function get($key, $segment = null, $onlyValue = true)
	{
		if (!isset($this->configByKey[$key])) return null;
		
		if (!is_null($segment)) {
			$seg = (array)$this->configBySegment[$segment];
			foreach ($seg as $v) 
			{	
				if ($v['k'] == $key) {
					if ($onlyValue) return $v['v'];
					else return $v;
				}		
			}	
		}
		if ($onlyValue) return $this->configByKey[$key]['v'];
		return $this->configByKey[$key];
	}
	
	public function segment($segment)
	{
		if (!isset($this->configBySegment[$segment])) throw new Exception('Cube_Config::segment('.$segment.') - nie istnieje segment.');
		return $this->configBySegment[$segment];
	}
	
	public function update($k, $v, $s = null)
	{
		if (!is_null($s)) 
			 $s = ' AND segment = "'.$s.'"';
		$sql = 'UPDATE config SET v = "'.$v.'" WHERE k = "'.$k.'"'.$s;
		return mysql_query($sql);
	}
	
	public function insert($k, $v, $s)
	{
		mysql_query('INSERT INTO config VALUES (null, "'.$k.'", "'.$v.'", "'.$s.'")');
	}
	
	public function exists($k)
	{
		if (!isset($this->configByKey[$k])) return false;
		return true;
	}	
}

?>
