<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_Request
{
	const RULE_WILCARD = '*';

	private $_language = null;
	private $_getVars;	 
	private $_postVars;
	private $_cookieVars;
	private $_requestVars;
	private $_params;	// przefiltrowane

	private $_isRedirected = false;			// przekierowany ?
	private $_redirectOnFailure = true;	// przekierować automatycznie jeśli nie przeszlo testow?
	private $_redirectUrl = null;			// gdzie przekierowac
	private $_redirectDefaultUrl = '';	// domyslny adres przekierowania
	
	private $_process = false;	// czy juz testowano
	private $_filters;			// filtry
	private $_validators;		// validatory
	private $_failureKeys	  = array(); // cos alal errors, ile kluczy nie przeszlo testow
	private $_failureMessages = array(); // komunikaty bledow
	private $_data = null;				// dane na ktorych ma operowac
	private $_success = null;	// czy wszystkie testy wypadly pomyslnie
	
	private $_messages;					// obecne komunikaty ustawione po niepomyslnym
	private $_messagesFromLastRequest;  // komunikaty z poprzedniego żadania
	private $_paramsFromLastRequest = array(); // JEDEN PARAM !
	
	/* DODATKOWE */
	private $_dispatched = false;
	private $_module = null;
	private $_controller = null;
	private $_action = null;
	private $_theme = null;
	private $_lang=null;
	private $_url_params = null; //url z parametrami
	
	
	public function getParam($k, $return = null)
	{
		if (!isset($this->_params[$k])) return $return;
		return $this->_params[$k];
	}
	
	public function isDispatched()
	{
		return $this->_dispatched;
	}
	
	public function setDispatched($flag)
	{
		$this->_dispatched = (bool)$flag;
	}
	
	public function getModule()
	{
		return $this->_module;
	}
	
	public function getController()
	{
		return $this->_controller;		
	}
			
	public function getAction()
	{
		return $this->_action;
	}
			
	public function getTheme()
	{
		return $this->_theme;
	}
	
	public function getLang()
	{
		//echo 'LANG='.$this->_lang;
		return $this->_lang;
	}
	
	
	public function setModule($module)
	{
		$this->_module = $module;
		return $module;
	}

//	/*
	public function getPost($key, $default = null) 
	{
		if (!isset($this->_postVars[$key])) {
			return $default;
		}
		return $this->_postVars[$key];	
	}
	
	public function isGet()
	{
		if (sizeof($this->_getVars) > 0) return true;
		return false;
	}
		
	public function get($key, $default = null)
	{
		if (!isset($this->_getVars[$key])) {
			return $default;
		}
		return $this->_getVars[$key];
	}	
		
	public function getServer($key)
	{
		return $_SERVER[$key];
	}	
	
	public function getCookie($key, $default = null)
	{
		if (!isset($this->_cookieVars)) {
			return $default;
		}
		return $this->_cookieVars[$key];
	}
		
	public function getRequest($key, $default = null)
	{
		if (!isset($this->_requestVars[$key])) {
			return $default;
		}
		return $this->_requestVars[$key];
	}	
		
	public function isPost() 
	{
		if (sizeof($this->_postVars) > 0) return true;
		return false;
	}
//	*/
	
	public function setController($controller)
	{
		$this->_controller = $controller;
		return $controller;
	}
			
	public function setAction($action)
	{
		$this->_action = $action;
		return $action;
	}
			
	public function setTheme($theme)
	{
		$this->_theme = $theme;
		return $theme;
	}
	
	public function setLang($lang)
	{
		$this->_lang = $lang;
		return $lang;
	}
		
		
	public function setParams($array)
	{
		foreach ($array as $key => $value)
		{
			$this->_params[$key] = $value;
		}
		return $this;
	}
		
	/* END: DODATKOWE */	
	 	
	public function __construct($checkSession = true)
	{
		$this->_language = Cube_Registry::get('language');
		$this->_getVars 	= $_GET;
		$this->_postVars 	= $_POST;
		$this->_cookieVars  = $_COOKIE;
		$this->_requestVars = $_REQUEST;	
		if ($checkSession) {
			if ($this->_cookieVars['dataFromLastRequest']) {
				$cookieValue = $this->_cookieVars['dataFromLastRequest']; // moze tu
				if (strlen($cookieValue) > 0) {
					$result = setcookie('dataFromLastRequest', '', time() - 3600, '/');
					$dataFromLastRequest = unserialize(stripslashes($cookieValue));
					$this->_messagesFromLastRequest = $dataFromLastRequest['messages'];
					$this->_paramsFromLastRequest = $dataFromLastRequest['params'];
					$this->_cookieVars['dataFromLastRequest'] = '';
					$this->_isRedirected = true;
				}
			} 
		}
		
		foreach($_GET as $k => $v)
		{
			switch ($k)
			{
				case 'module': case 'controller': case 'action': case 'theme': case 'ling': break;
				default: 
					$this->_params[$k] = strip_tags($v);
					$this->_url_params.=','.$k.'_'.$v;
			}
		}
		
		$this->setModule($_GET['module']);
		$this->setController($_GET['controller']);
		$this->setAction($_GET['action']);
		//echo 'LANG='.$_GET['lang'];
		/*if( !(isSet($_GET['lang'])) || ($_GET['lang']=='') )
			$this->setLang('pl');
		else
			$this->setLang(clear($_GET['lang']));
		*/
		if( isSet($_GET['lang']) )
		{
			//czy jest w bazie danych aktywny jezyk z podanym w url rozszerzeniem 
			$this->setLang( $this->checkLang(clear($_GET['lang'])) );
		}
			
		if (strtolower($_GET['theme']) == 'admin' || strtolower($_GET['theme']) == 'adminlogin' ||
		!array_search(strtolower($_GET['theme']), Cube_Registry::get('systemTemplates')))
			$_GET['theme'] = '';
		
		$this->setTheme(clear($_GET['theme']));
		//$this->_redirectDefaultUrl = $_GET['module'].','. $_GET['controller'] .',' . $_GET['action'].'.html';
		$this->_redirectDefaultUrl = $_GET['module'].','. $_GET['controller'] .',' . $_GET['action'].$this->_url_params.'.html';
	}
	
	public function checkLang($lang)
	{
		$array_search=array('pl');
		$aciveLang=$this->_language->getActiveLang();
		if( count($aciveLang) > 0)
		{
			foreach($aciveLang as $activeL)
			{
				$array_search[]=$activeL['short_name'];
			}
		}
		if( is_numeric(array_search($lang,$array_search)) )
		{
			return $lang;
		}
		else
			return null;
	}
	
	public function isRedirectedLang()
	{
		//Czy mozna zrobic przekierowanie ze wzgledu na jezyk.
		//Czy nie jest to wewnetrzne przekierowanie typu /,notifications,edit.html
		
		
		if(preg_match('/^\/,?[a-z]+,[a-z]+\.html$/',$_SERVER['REQUEST_URI']))
			//jezeli znalazl /,notifications,edit.html to nie mozna wywolac funkcji RedirectLang
			return false;
		else
			return true;
	
	}
	
	public function redirectLang()
	{
		//przekierowanie ze względu na jezyki
		$target=createURI_Lang($this->getLang());
		//nie dziala z header('refresh: 3; url=dodaj_zgloszenie.html');
		header("refresh: 0; URL=$target");
		
	}
	
	
	public function process($filters, $validators, $failureMessages = array(), $data = null)
	{
		$this->_process = true;
		// załaduj dane na których bedziemy operować
		if ($this->_data == null) $this->_data = (!is_array($data)) ? $this->_postVars : $data;
		
		// ewentualne komunikaty
		$failureMessages = (array)$failureMessages ;
		if (count($failureMessages) > 0) {
			$this->_failureMessages = $failureMessages;
		}
		$tmpData = array();  // dane tymczasowe	
		// filtruj !
		
		$filters = (array)$filters;
		if (count($filters) < 1) {
			$filters = array('**' => '');	
		}
		
		foreach($filters as $key => $value)
		{		
			// zamien wartosc w tablice
			$value = (array)$value;
			
			// jeżeli nie istnieje tablica z paramterami, stworz ja dla kompatybilnosci
			if (!isset($value['params'])) {
				$params = array();
			} else $params = (array)$value['params'];
				
			// zlikwiduj parametry dodatkowe
			unset($value['params']);
			
			if ($key == self::RULE_WILCARD) {
				foreach($this->_data as $k => $v) // filtruje wszystkie dane 
				{
					$tmpData[$k] = $this->_filter($v, $value, $params);
					$this->_paramsFromLastRequest[$k] = $tmpData[$k];
				}
			} elseif ($key == '**') { // IF NONE
				foreach($this->_data as $k => $v) // filtruje wszystkie dane 
				{
					$tmpData[$k] = $v;
					$this->_paramsFromLastRequest[$k] = $v;
				}				
			} else { // filtruje tylko zmienna o podanym kluczu
				$tmpData[$key] = $this->_filter($this->_data[$key], $value, $params);
				$this->_paramsFromLastRequest[$key] = $tmpData[$key];
			}	
		}		
		// validuj !
		foreach($validators as $key => $value)
		{
			// zamien wartosc w tablice
			$value = (array)$value;
			
			// jeżeli nie istnieje tablica z paramterami, stworz ja dla kompatybilnosci
			if (!isset($value['params'])) {
				$params = array();
			} else $params = (array)$value['params'];
						
			
			if ($key == self::RULE_WILCARD) {	
				foreach($this->_data as $k => $v) // testuj
				{
					// ustaw wartość do testowania
					if (isset($tmpData[$k])) $testVal = $tmpData[$k];
					else $testVal = $v;				
					$this->_validate($testVal, $value, $params, $k);
					$this->_paramsFromLastRequest[$k] = $testVal;
				}				
				
			} elseif ($key == 'Required') {
				if (isset($tmpData[$key])) $testVal = $tmpData[$key];
				else $testVal = $this->_data[$key];
				$this->_validate($testVal, 'Required', $params, $key);
				$this->_paramsFromLastRequest[$key] = $testVal;
			} else {
				if (isset($tmpData[$key])) $testVal = $tmpData[$key];
				else $testVal = $this->_data[$key];	
				$this->_validate($testVal, $value, $params, $key);
				$this->_paramsFromLastRequest[$key] = $testVal;
			}
		}
		
		if ($this->_checkResult()) {
			// zapisz dane
			foreach($tmpData as $k => $v)
			{
				$this->_params[$k] = $v;
				// ZBEDNE ?
				$this->_data[$k] = $v;
			}
			unset($tmpData);		
		}
	}

	public function redirectFailure($errors)
	{
		$messages = array();
		foreach ($errors as $error)
		{
			$messages[] = $error; 
		}
		if ($this->_redirectUrl != null) $target = $this->_redirectUrl;
		else $target = $this->_redirectDefaultUrl;
		$messagesAndParams = array('messages' => $messages, 'params' => $this->_paramsFromLastRequest);
		setcookie('dataFromLastRequest', serialize($messagesAndParams), time() + 3600, '/');
		header("Location: $target");
		exit;		
	}

	protected function _checkResult()
	{	
		$fails = count($this->_failureKeys);
		if ($fails > 0) {	
			// wystapił problem
			// zapisz komunikaty
			$messages = array();
			foreach((array)$this->_failureKeys as $key)
			{
				if (isset($this->_failureMessages[$key]))
					$messages[] = $this->_failureMessages[$key];
				// TODO default msgs		
			}
			
			$messagesAndParams = array('messages' => $messages, 'params' => $this->_paramsFromLastRequest);
			
			// sprawdz czy przekierowac, jesli tak zapisz cookie
			if ($this->_redirectOnFailure) {
				if ($this->_redirectUrl != null) 
					$target = $this->_redirectUrl;
				else $target = $this->_redirectDefaultUrl;
				$res = setcookie('dataFromLastRequest', serialize($messagesAndParams), time() + 3600, '/');
				header("Location: $target");
				exit;	
			}		
			// jesli nie, ustaw komunikaty
			$this->_messages = $messages;
			$this->_success = false;
			return false;
		} else {		
			$this->_success = true;
			return true;
		}
	}

	public function getData() 
	{
		return $this->_data;
	}
	
	public function isRedirected()
	{
		return $this->_isRedirected;
	}
	
	public function getMessagesFromLastRequest()
	{
		if ($this->_isRedirected) return $this->_messagesFromLastRequest;
		else return null;
	}
	
	public function getParamsFromLastRequest()
	{
		if ($this->_isRedirected) return $this->_paramsFromLastRequest;
		else return null;
	}
	
	public function getMessages()
	{
		return $this->_messages;
	}

	protected function _filter($value, $filters, $params)
	{
		$filters = (array)$filters; // arrayuyj
		
		foreach($filters as $filter) 
		{
			$filterName = 'Cube_Filter_'.$filter; // nazwa obiektu		
			Cube_Loader::loadClass($filterName); // załaduj klase
			$filter = new $filterName($params); // stworz obiekt
			$value = $filter->filter($value);	// filtruj !			
		}
		return $value;
	}
	
	protected function _validate($value, $validators, $params, $key)
	{
		$validators = (array)$validators; // arrayuyj	
		foreach($validators as $validator) 
		{
			$validatorName = 'Cube_Validator_'.$validator; // nazwa obiektu
			
			Cube_Loader::loadClass($validatorName); // załaduj klase
			$validator = new $validatorName($params); // stworz obiekt			
			if (!$validator->validate($value)) {	// validuj !
				$this->_failureKeys[] = $key;
			} 		
		}
	}
	
	public function isValid()
	{
		return $this->_success;
	}
	
	public function getCurrentUrl()
	{
		return null;
	}		
}

?>
