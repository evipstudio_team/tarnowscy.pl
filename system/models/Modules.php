<?php

class Modules
{	
	
	public function getAllModules($where = null, $order = null)
	{
		if (!is_null($where)) $where = ' WHERE '.$where;
		if (!is_null($order)) $order = ' ORDER BY '.$order;
		$sql='SELECT * FROM modules'.$where.$order;
		$r = mysql_query($sql);
		//echo $sql;
		//echo mysql_errno() . ": " . mysql_error(). "\n";
		return mysql_fetch_all($r);	
		
	}
	
	//pobiera moduly przpisane do uzytkownikow. Dla jednoego usera where=u.id
	public function getModules($where = null, $order = null, $select = '*')
	{
		if (!is_null($where)) $where = ' AND '.$where;
		if (!is_null($order)) $order = ' ORDER BY '.$order;
		$query='SELECT m.'.$select.' FROM modules m, admin_access a, users u WHERE ( u.id = a.idu AND m.id = a.idm) AND m.active=1 '.$where.$order;	
		$r = mysql_query($query);
		//oryginalne
		//$r = mysql_query('SELECT '.$select.' FROM modules'.$where.$order);
		//echo $query;
		//exit;
		//echo mysql_errno() . ": " . mysql_error(). "\n";
		return mysql_fetch_all($r);	
	}	
	
	public function getByName($name)
	{
		$name = strtolower($name);
		$r = mysql_query('SELECT * FROM modules WHERE name = "'.$name.'"');
		return mysql_fetch_assoc($r);
	}

	public function get($id)
	{
		$r = mysql_query('SELECT * FROM modules WHERE id = "'.$id.'"');
		return mysql_fetch_assoc($r);
	}
	
	public function update($id, $var)
	{
		mysql_query('UPDATE modules SET pos = "'.$var.'" WHERE id = "'.$id.'"');		
	}
	
	
	
	public function isInstalled($name)
	{
		$row = $this->getByName($name);
		return ($row['id'] > 0) ? true : false;	
	}	

	public function isActive($name)
	{
		$row = $this->getByName($name);
		return $row['active'] ? true : false;	
	}
	
	public function install($name)
	{
		$name = strtolower($name);
		if (!file_exists("system/modules/$name.php")) return 0; // plik modulu nie isnieje
		require_once "system/modules/$name.php";
		
		if (!is_array($moduleInstall)) return -1; // bledne pliki modulu
		if (!is_array($moduleInfo)) return -2; // brak modulinfo
		if ($moduleInfo['name'] == '') return -2;
		if ($moduleInfo['admin_access'] == '') return -2;
		//if ($moduleInfo['access'] == '') return -2;
		
		if (!is_array($moduleActions)) return -3; // brak akcji
		
		if ($moduleInfo['admin_access'] != 'none') {
			if (!file_exists("system/controllers/admin/".ucfirst($name)."Controller.php")) return -4;
			else {
				include_once "system/controllers/admin/".ucfirst($name)."Controller.php";
				if (!class_exists("Admin_".ucfirst($name)."Controller")) return -4; 
			}	
		}		
		if ($moduleInfo['access'] != 'none') {
			if (!file_exists("system/controllers/".ucfirst($name)."Controller.php")) return -4;
			else {
				include_once "system/controllers/".ucfirst($name)."Controller.php";
				if (!class_exists(ucfirst($name)."Controller")) return -4; 
			}
		}
		
		$r = mysql_query('SELECT id FROM modules WHERE name = "'.$name.'"');
		if (mysql_num_rows($r) > 0) return -5; // moduł juz jest zainstalowany
		
		foreach ($moduleInstall as $query)
			mysql_query($query);	
		
		mysql_query('INSERT INTO modules VALUES (null, 1, 
			"'.$moduleInfo['admin_access'].'","'.$moduleInfo['access'].'",
			"'.strtolower($moduleInfo['name']).'","'.$moduleInfo['description'].'","'.$moduleInfo['info'].'",0)');
		return 1;		
	}
	
	public function uninstall($name)
	{
		$name = strtolower($name);
		if (!file_exists("system/modules/$name.php")) return 0; // plik modulu nie isnieje
		require "system/modules/$name.php";
		
		if (!is_array($moduleUninstall)) return -1;	// bledne pliki modulu
		
		$r = mysql_query('SELECT id FROM modules WHERE name = "'.$name.'"');
		if (mysql_num_rows($r) == 0) return -2; // moduł nie jest zainstalowany
		
		foreach ($moduleUninstall as $query)
			mysql_query($query);
				
		mysql_query('DELETE FROM modules WHERE name = "'.$name.'"');
		return 1; // usuniety pomyslnie	
	}
	
	public function active($id)
	{
		mysql_query('UPDATE modules SET active = "1" WHERE id = "'.$id.'"');
	}
	
	public function deactive($id)
	{
		mysql_query('UPDATE modules SET active = "0" WHERE id = "'.$id.'"');
	}
	
	public function setAccess($idu, $idm)
	{
		mysql_query('INSERT INTO admin_access (id,idu,idm) VALUES (NULL ,"'.$idu.'","'.$idm.'")');
	}
	
	public function unsetAccess($idu = null, $idm = null)
	{
		$where = ' WHERE ';
		if (!is_null($idu))
		{
			$where .= 'idu="'.$idu.'"';
			if (!is_null($idm)) $where .= ' AND idm="'.$idm.'"';
		}
		else
			if (!is_null($idm)) $where .= 'idm="'.$idm.'"';
		
		mysql_query('DELETE FROM admin_access'.$where);
	}					
}
					 
?>
