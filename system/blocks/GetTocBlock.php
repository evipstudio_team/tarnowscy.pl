<?php 

class GetTocBlock
{
	public function get($id)
	{
		$model = new ArticlesTOC();
		$row = $model->get($id);
		return $row['contents'];
	}					
}

?>
