<?php

$moduleInfo = array(
			'name' 			=> 'banner',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Moduł bannerów reklamowych',
			'info' 			=> 'v1.2.1, Michał Daniel, Cube Interactive'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'categories';


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `banner_categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `note` text collate utf8_polish_ci NOT NULL, 
  `view_amount` int(11) NOT NULL default '0',
  `random` tinyint(1) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '1',  
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `start` int(16) NOT NULL default '0',
  `end` int(16) NOT NULL default '0',
  `filename` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `www` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `width` varchar(255) NOT NULL default '',
  `height` varchar(255) NOT NULL default '',
  `description` text collate utf8_polish_ci  NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
/* CONFIG */								
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `banner_categories`";
$moduleUninstall[] = "DROP TABLE `banner`";

?>
