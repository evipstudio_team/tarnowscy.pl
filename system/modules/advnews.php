<?php

$moduleInfo = array(
			'name' 			=> 'advnews',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Aktualności',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'insert';
$moduleActions[] = 'settings';


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews` (
					  `id` int(11) NOT NULL auto_increment,
					  `add_date` int(11) NOT NULL default '0',
					  `author` varchar(255) COLLATE utf8_polish_ci NOT NULL default '',
					  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL default '',
					  `contents` text COLLATE utf8_polish_ci NOT NULL default '',
					  `short_contents` text COLLATE utf8_polish_ci NOT NULL default '',
					  `language` varchar(3) COLLATE utf8_polish_ci NOT NULL default '',
					  `gallery_cid` int(11) NOT NULL default '0',
					  `active` TINYINT( 1 ) NOT NULL default '1',
					  `pos_all` INT( 6 ) UNSIGNED NOT NULL DEFAULT '0',
					  `main` tinyint( 1 ) NOT NULL DEFAULT '0',
					   PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
		
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews_categories` (
					`id` int(11) NOT NULL auto_increment,
					`parent_id` int(11) NOT NULL default '0',
					`pos` int(6) unsigned NOT NULL default '0',
					`name` varchar(255) collate utf8_polish_ci NOT NULL default '',
					`active` tinyint(1) NOT NULL default '1',
					PRIMARY KEY  (`id`),
					KEY `parent_id_pos` (`parent_id`,`pos`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews_tree_pos` (
					`id` int(10) unsigned NOT NULL auto_increment,
					`parent_id` int(11) unsigned NOT NULL,
					`child_id` int(11) unsigned NOT NULL,
					`depth` int(11) unsigned NOT NULL,
					PRIMARY KEY  (`id`),
					UNIQUE KEY `parent_id_child_id_depth` (`parent_id`,`child_id`,`depth`),
					KEY `child_id_depth` (`child_id`,`depth`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";					
					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews_category` (
					  `id` int(11) NOT NULL auto_increment,
					  `nid` int(11) NOT NULL default '0',
					  `cid` int(11) NOT NULL default '0',
					  `pos` int( 6 ) UNSIGNED NOT NULL DEFAULT '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";					
					
					
/* CONFIG */					
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_page", "6", "advnews")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "max_chars", "300", "advnews")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "start_date", "", "advnews")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "end_date", "", "advnews")';

$moduleInstall[] = 'INSERT INTO advnews_categories VALUES (null, 0, 0,"Główna",1)';
$moduleInstall[] = 'INSERT INTO advnews_tree_pos VALUES (null, 1, 1,0)';
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `advnews`";
$moduleUninstall[] = "DROP TABLE `advnews_categories`";
$moduleUninstall[] = "DROP TABLE `advnews_tree_pos`";
$moduleUninstall[] = "DROP TABLE `advnews_category`";
$moduleUninstall[] = 'DELETE FROM config WHERE segment = "advnews"';

?>
