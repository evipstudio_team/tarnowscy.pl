<?php

$moduleInfo = array(
			'name' 			=> 'menus',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Menu',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'addmenu';

$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `menus` (
					  `id` int(11) NOT NULL auto_increment,
					  `parent_id` int(11) NOT NULL default '0',
					  `pos` int(6) unsigned NOT NULL default '0',
					  `name` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `label` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `href` varchar(255) NOT NULL default '',
					  `extra` varchar(255) NOT NULL default '',
					  `language` varchar(3) NOT NULL default '',
					  `logged` tinyint(1) NOT NULL default '0',
					  `target` tinyint(1) NOT NULL default '0',
					  `active` tinyint(1) NOT NULL default '1',
					PRIMARY KEY  (`id`),
					KEY `parent_id_pos` (`parent_id`,`pos`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `menus_tree_pos` (
					`id` int(10) unsigned NOT NULL auto_increment,
					`parent_id` int(11) unsigned NOT NULL,
					`child_id` int(11) unsigned NOT NULL,
					`depth` int(11) unsigned NOT NULL,
					PRIMARY KEY  (`id`),
					UNIQUE KEY `parent_id_child_id_depth` (`parent_id`,`child_id`,`depth`),
					KEY `child_id_depth` (`child_id`,`depth`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";					
					
$moduleInstall[] = 'INSERT INTO menus VALUES (null, 0, 0,"Główna","","","","",0,0,1)';
$moduleInstall[] = 'INSERT INTO menus_tree_pos VALUES (null, 1, 1,0)';					

$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `menus`";
$moduleUninstall[] = "DROP TABLE `menus_tree_pos`";

?>
