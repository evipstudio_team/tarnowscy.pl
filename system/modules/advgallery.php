<?php

$moduleInfo = array(
			'name' 			=> 'advgallery',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Galeria zdjęć',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'photos';
//$moduleActions[] = 'binding';
$moduleActions[] = 'settings';

$moduleInstall = array();
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_page", "16", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_news", "4", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_articles", "4", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_categories", "4", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "advnews", "1", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "articles", "1", "advgallery")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "advnews_categories", "1", "advgallery")';

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advgallery_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `pos` int(6) unsigned NOT NULL default '0',
  `name` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `active` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `parent_id_pos` (`parent_id`,`pos`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";



$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advgallery_tree_pos` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  `depth` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `parent_id_child_id_depth` (`parent_id`,`child_id`,`depth`),
  KEY `child_id_depth` (`child_id`,`depth`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";


					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advgallery_photos` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `pos` int(6) NOT NULL default '0',
  `pos_all` int(6) NOT NULL default '0',
  `main` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";


					
/* CONFIG */					
$moduleInstall[] = 'INSERT INTO advgallery_categories VALUES (null, 0, 0,"Główna",1)';
$moduleInstall[] = 'INSERT INTO advgallery_tree_pos VALUES (null, 1, 1,0)';
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `advgallery_categories`";
$moduleUninstall[] = "DROP TABLE `advgallery_tree_pos`";
$moduleUninstall[] = "DROP TABLE `advgallery_photos`";
$moduleUninstall[] = 'DELETE FROM config WHERE segment = "advgallery"';

?>
