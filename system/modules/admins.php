<?php

$moduleInfo = array(
			'name' 			=> 'admins',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Administratorzy serwisu',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'add';
$moduleActions[] = 'passwd';

$moduleInstall = array();
$moduleUninstall = array();

?>
