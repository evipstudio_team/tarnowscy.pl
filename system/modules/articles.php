<?php

$moduleInfo = array(
			'name' 			=> 'articles',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Podstrony',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'insert';

$moduleTablesLang = array();
//$moduleTablesLang[]='\'table\'=>\'articles\', \'field\'=>array(\'meta\',\'author\',\'title\',\'contents\')';
//$moduleTablesLang[]="'table'=>'articles', 'field'=>array('meta','author','title','contents')";
//$moduleTablesLang[]="'table'=>'articles_test', 'field'=>array('meta','author','title','contents')";
$moduleTablesLang = array
	(
		array('table'=>'articles', 'field'=>array('meta_title','title','contents') )
	);


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `articles` (
					  `id` int(11) NOT NULL auto_increment,
					  `add_date` int(16) NOT NULL default '0',
					  `views` int(11) NOT NULL default '0',
					  `meta_title` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `author` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `contents` text collate utf8_polish_ci NOT NULL, 
					  `logged` tinyint(1) NOT NULL default '0',
					  `gallery_cid` int(11) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
/*$moduleInstall[] = "CREATE TABLE `articles-toc` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `contents` text NOT NULL, 
					  `language` varchar(3) NOT NULL default '',
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
*/					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `articles`";


?>
