<?php

$moduleInfo = array(
			'name' 			=> 'canteen',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Moduł zarządzania Apartamentami',
			'info' 			=> 'Evipstudio.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'insert';

//$moduleTablesLang = array();
//$moduleTablesLang[]='\'table\'=>\'canteen\', \'field\'=>array(\'meta\',\'author\',\'title\',\'contents\')';
//$moduleTablesLang[]="'table'=>'canteen', 'field'=>array('meta','author','title','contents')";
//$moduleTablesLang[]="'table'=>'canteen_test', 'field'=>array('meta','author','title','contents')";
//$moduleTablesLang = array
//	(
//		array('table'=>'canteen', 'field'=>array('meta_title','title','contents') )
//	);


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `canteen` (
					  `id` int(11) NOT NULL auto_increment,
					  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `city` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `street` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `zip_code` varchar(30) collate utf8_polish_ci NOT NULL default '',
					  `contents` text collate utf8_polish_ci NOT NULL, 
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

$moduleInstall[] = "CREATE TABLE `canteen_dates` (
					`id` int(11) NOT NULL auto_increment,
					`id_canteen` int(11) NOT NULL,
					`start_date` int(16) NOT NULL,
					`end_date` int(16) NOT NULL,
					 PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
$moduleInstall[] = "INSERT INTO `photos_config` (`id`, `directory`, `width`, `height`, `module`) VALUES 
					(null, '', 900, 675, 'canteen'),
					(null, 'mini/', 120, 90, 'canteen');";
					
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `canteen`";
$moduleUninstall[] = "DROP TABLE `canteen_dates`";
$moduleUninstall[] = "DELETE FROM `photos_config` WHERE `module`='canteen'";


?>
