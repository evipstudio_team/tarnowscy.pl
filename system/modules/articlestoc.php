<?php

$moduleInfo = array(
			'name' 			=> 'articlestoc',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Bloki informacyjne',
			'info' 			=> 'Evipstudio.pl - VipoCMS'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'add';

$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `articles-toc` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) collate utf8_polish_ci NOT NULL default '',
					  `contents` text collate utf8_polish_ci NOT NULL, 
					  `language` varchar(3) NOT NULL default '',
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `articles-toc`";

?>
