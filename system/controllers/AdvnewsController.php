<?php 

class AdvnewsController extends Cube_Controller_Abstract
{	
	private $_pp;
	private $_max; // max chars
	private $_comments; // true or false
	private $_search = null;
	private $_where=null;
	private $_year=null;
	private $_month=null;
	private $_search_string=null;
	
	private function search_order()
	{
		$this->_search_string=$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$content = new Cube_SearchOrder_Field('content','n.contents');
		$title = new Cube_SearchOrder_Field('title','n.title');
				
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($content);
		$search->addField($title);
		
	}
	
	private function left_menu()
	{
		$model = new AdvNewsTree();
		
		if($this->_cid >0)
			$this->view->left_menu = $model->leftMenuTree($this->_cid,'pos','name');
		else
			$this->view->left_menu = $model->leftMenuTree(1,'pos','name');
		$this->view->left_menu=show_tree($this->view->left_menu,true);
	
		
	}
	
	public function init()
	{
		$this->_pp = $this->_config->get('per_page', 'advnews');
		$this->view->max = $this->_max = $this->_config->get('max_chars', 'advnews');
		$this->view->statusComments = $this->_comments = $this->_config->get('comments', 'advnews');
		$this->view->year=$this->_year=(int)$this->_request->getParam('year');
		$this->view->month=$this->_month=(int)$this->_request->getParam('month');
		
		$cid = $this->_request->getParam('cid', 0);
		$this->view->cid = $this->_cid = $cid;
		$this->left_menu();
		//aktualnosci prasowe
		/*$model = new MediaTree();
		$info = $this->view->info = $model->getAllTree(null,'t.pos','t.name');
		$this->view->info=show_tree($info,$root=true);
				
		$model = new MediaDocs();
		$this->view->docs = $model->getAll(null,title);*/
				
		$this->search_order();
	}

	public function indexAction()
	{
		if(! $this->_request->isPost() )
		{
			//echo 'NIE POST';
			$this->create_where($this->_year,$this->_month,$this->_search_string);
		}
		
		$model = new AdvNews();
		//$all = $this->_request()->getParam('all', 0);
		
		$cid = $this->_cid;
		
		if($cid != null) {
			$where = 'cs.id = "'.$cid.'"'; 
			$template = '<a href="aktualnosci_'.$cid.',:value.html">:id</a>';
			$this->view->template = 'aktualnosci_'.$cid.',:value.html';
			$news = $model->getAll($where.'AND cs.active=1 AND n.active=1', 'c.pos DESC,n.add_date DESC,n.id DESC');
		}
		 else 
		{
			$template = '<a href="aktualnosci_'.$this->_year.'_'.$this->_month.'_'.$this->_search_string.',:value.html">:id</a>';
			$this->view->template = 'aktualnosci_'.$this->_year.'_'.$this->_month.'_'.$this->_search_string.',:value.html';
			$news = $model->getAllNews($this->_where.'n.active=1', 'n.pos_all DESC,n.add_date DESC,n.id DESC');
		}	
			
		//$this->view->categories = $categories = $model->getCategories();
		
		
		$news_amount = sizeof($news);
	//	$template = ' <a href="aktualnosci,:value.html">[:id]</a> ';
		$start = $this->_request()->getParam('start', 0);
		
		// PAGES
		Cube_Loader::loadClass('Cube_Pages_Advanced');
		$pages = new Cube_Pages_Advanced($start, $news_amount, $this->_pp, $template);
		
		$tmp = array();
		if (count($news) > 0)
		{
			foreach ($news as $k => $v)
			{
				if ( ($k + 1) > $start && ($k + 1) <= ($start + $this->_pp) ) 
					$tmp[] = $v;
			}
		}
		$this->view->pages = $pages;
		$this->view->rows = $tmp;
		
			
		//wyszukiwanie 
		$model = new AdvNews();
		$rows=$model->getYear();
		$temp=null;
		foreach($rows as $row)
		{
			$year=$row['year'];
			
			if( (date("Y") == $year && is_null($this->_year) ) || ($this->_year == $year && !is_null($this->_year)) )
				$temp .= '<option value="'.$year.'" selected="selected">'.$year.'</option>';
			else 
				$temp .= '<option value="'.$year.'">'.$year.'</option>';
		}
		$this->view->yearList=$temp;
		
		$month=array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień');
		$temp=null;
		foreach($month as $key => $m)
		{
			$month=$m;
			$nr=$key +1;
			if ($this->_month == $nr && !is_null($this->_month))	
				$temp .= '<option value="'.$nr.'" selected="selected">'.$month.'</option>';
			else 
				$temp .= '<option value="'.$nr.'">'.$month.'</option>';
		
		}
		$this->view->yearMonth=$temp;		
		
	}

	public function viewAction()
	{		
		$this->view->id = $id = $this->_request()->getParam('id', 0);
		$model = new AdvNews();
		$rows=$model->getAll('n.id='.$id);
		$this->view->row = $news = $rows[0];
		
		if (!isset($news['add_date'])) $this->view->news = null; 
		else $this->view->news = $news;	
		
		$model = new Upload();
		$this->view->module=$module=$this->_request()->getController();
		$this->view->photo=$model->getAll('module="'.$module.'" AND category="photo" AND id_element='.$id,'pos,id');
		$this->view->pdf=$model->getAll('module="'.$module.'" AND category="pdf" AND id_element='.$id,'pos,id');
		$this->view->document=$model->getAll('module="'.$module.'" AND category="document" AND id_element='.$id,'pos,id');
	}

	public function addAction()
	{
		if (!$this->_comments) {
			header('Location: aktualnosci.html');
			return;
		}	
	
		$this->view->id = $id = $this->_request()->getParam('id', 0);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new CommentsFilter();
			$filter->filter();	
			$data = $filter->getData();
			if (strlen($data['contents']) > $this->_max)
				$this->_request->redirectFailure(array('Podana treść jest zbyt długa, maksymalna ilośc znaków to '.$this->_max));
			$model = new Comments();
			$data['module'] = 'advnews';
			$data['id'] = $id;
			$model->insert($data);	
			header('refresh: 3; url=aktualnosci.html');
			$this->view->message = 'Komentarz dodany pomyślnie ! Przekierowywanie...';
		}
	}
	
	public function searchAction()
	{
		$this->view->year=$this->_year=$year=(int)clear($_POST['year']);
		$this->view->month=$this->_month=$month=(int)clear($_POST['month']);
		$this->_search_string=$search=clear($_POST['search']);
		//echo $year;
		//echo $month;
		$this->create_where($year,$month,$search);
		
		$this->view->render('index');
		$this->indexAction();
	}

	private function create_where($year,$month,$search)	
	{
		$where_date=null;
		if( ($month !='') || ( !isSet($month) ) || ($month > 0))
		{
			//echo 'IF='.$month;
			if($month >= 12 )
			{
				$next_month=1;
				$next_year=$year+1;
			}
			else
			{
				$next_month=$month+1;
				$next_year=$year;
			}
			
			$start_date=$year.'-'.$month.'-1';
			$end_date=$next_year.'-'.$next_month.'-1';	
			$where_date = 'add_date >= unix_timestamp("'.$start_date.'") AND add_date < unix_timestamp("'.$end_date.'")';
		}
		else if( ($year !='') || ( !isSet($year) ) || ($year > 0) )
		{
			$start_date=$year.'-1-1';
			$end_date=$year+1;
			$end_date.='-1-1';
			$where_date = 'add_date >= unix_timestamp("'.$start_date.'") AND add_date < unix_timestamp("'.$end_date.'")';
		}	
		
		$where_string=null;
		if( (($search !='') || ( !isSet($search) )) && !is_null($where_date) )
			$where_string=' AND '.$this->_search->createWhere($search);
		else if( (($search !='') || ( !isSet($search) )) && is_null($where_date) )
			$where_string=$this->_search->createWhere($search);
			
			
		if( (is_null($where_date)) && ( is_null($where_string)) )
			$this->_where=null;
		else
			$this->_where=$where_date.$where_string.' AND ';	
		//echo $this->_where;
		
		
	}
	
	
}

?>
