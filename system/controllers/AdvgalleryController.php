<?php 

class AdvgalleryController extends Cube_Controller_Abstract
{	
	private $_pp; // photos per page
	private $_pcat; //photos per categories
	private $_cid=null;
	
	public function init()
	{
		$this->_pp = $this->_config->get('per_page', 'advgallery');
		$this->_pcat = $this->_config->get('per_categories', 'advgallery');
		
		$cid = clear($_POST['category']);
		if ($cid == 0 || $cid == '' || !is_numeric($cid)) $cid = $this->_request->getParam('cid', 1);
			$this->view->cid = $this->_cid = $cid;
	}

	private function paging_cat($rows)
	{
		$start = $this->_request()->getParam('start2', 0);
		$cid=$this->_cid;
		//stronicowanie
		//link do cyferek
		$template = '<a href="galeria2_'.$cid.',:value.html">[:id]</a> ';
		//link do nastepny, poprzedni
		$this->view->template_cat = 'galeria2_'.$cid.',:value.html';	
		
		$rows_amount = sizeof($rows);
		// PAGES
		Cube_Loader::loadClass('Cube_Pages_Advanced');
		$pages = new Cube_Pages_Advanced($start, $rows_amount, $this->_pcat, $template);
		
		$tmp = array();
		foreach ($rows as $k => $v)
		{
			if ( ($k + 1) > $start && ($k + 1) <= ($start + $this->_pcat) ) 
				$tmp[] = $v;
		}
		
		$this->view->pages_cat = $pages;	//obiekt ze stronami
		// $this->view->products = $tmp;
		$this->view->subcats = $tmp;  //wyniki na stronie
	}
	
	
	
	
	private function paging($rows)
	{
		$start = $this->_request()->getParam('start', 0);
		$cid=$this->_cid;
		//stronicowanie
		//link do cyferek
		$template = '<a href="galeria_'.$cid.',:value.html">[:id]</a> ';
		//link do nastepny, poprzedni
		$this->view->template = 'galeria_'.$cid.',:value.html';	
		
		$rows_amount = sizeof($rows);
		// PAGES
		Cube_Loader::loadClass('Cube_Pages_Advanced');
		$pages = new Cube_Pages_Advanced($start, $rows_amount, $this->_pp, $template);
		
		$tmp = array();
		foreach ($rows as $k => $v)
		{
			if ( ($k + 1) > $start && ($k + 1) <= ($start + $this->_pp) ) 
				$tmp[] = $v;
		}
		
		$this->view->pages = $pages;	//obiekt ze stronami
		// $this->view->products = $tmp;
		$this->view->rows = $tmp;  //wyniki na stronie
	}
	
	
	private function _getOnePhoto($cid)
	{
		$model = new AdvGallery();
		$row=$model->getOnePhoto($cid);
		return $row;
	
	}
	private function _getSubCategories($parent_id)
	{
		$model = new AdvGalleryTree();
		$rows=$model->getAllChildren($parent_id,'active=1');
		return $rows;
	}
	
	private function _getBranchPatch($cid)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new AdvGalleryTree();
		$rows=$model->getBranchTree($cid);
		return $rows;		
	}
	
	public function indexAction()
	{		
		$cid=$this->_cid;
		$model = new AdvGalleryTree();
		
		$this->view->left_menu = $model->leftMenuTree($cid,'pos','name');
		$this->view->left_menu=show_tree($this->view->left_menu,true);
				
		$subcats = $this->_getSubCategories($cid);
		$this->view->currentPath = $this->_getBranchPatch($cid);
		
		if (count($subcats) > 0) 
		{
			
						
			foreach ($subcats as $k => $v)
			{
				$photo = $this->_getOnePhoto($v['id']);
				$subcats[$k]['filename'] = $photo['filename'];
			}
			
			//$this->view->subcats = $subcats;
			$this->paging_cat($subcats);	
		} 
		//else 
		{
			
			$model = new AdvGallery();
			$photos = $model->getAll('p.cid = "'.$cid.'"','p.pos,p.id DESC');
			$model = new AdvGalleryTree();
			$this->view->currentCat = $model->get($cid);	
			
			$this->paging($photos);			
		}
	}		
}

?>
