<?php 

class Admin_BannerController extends Cube_Controller_Abstract
{
	private $_id;
	private $_cid;
		
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->username = $this->_session->getUsername();
		$this->_id = $this->_request->getParam('id', 0);
		$this->view->id = $this->_id;
		$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->cid = $this->_cid;		
		$this->view->controller = $this->_request->getController();	
		$this->view->categories = $this->_getCategoriesByName();
		$this->view->categoriesList = $this->_getCategoriesList();
	}

	protected function _getCategories()
	{
		$model = new BannerCategories();
		$cats = $model->getAll(null, 'name ASC');
		return $cats;
	}
	
	protected function _getCategoriesByName()
	{
		$model = new BannerCategories();
		$cats = $model->getAll(null, 'name ASC');
		$temp = array();
		foreach ($cats as $cat)
		{
			$temp[$cat['id']] = $cat;
		}
		return $temp;
	}
		
	protected function _getCategoriesList($cid = null, $no = false)
	{
		$cats = $this->_getCategories();
		$temp = '';
		foreach ($cats as $cat)
		{
			if (!is_null($cid) && $cat['id'] == $cid) {
				if (!$no) $temp .= '<option value="'.$cat['id'].'" selected="selected">'.$cat['name'].'</option>';
			} else $temp .= '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
		}
		return $temp;
	}

	public function indexAction()
	{	
		$this->view->row['od_day'] = date('d');
		$this->view->row['od_month'] = date('m');
		$this->view->row['od_year'] = date('Y');
		$this->view->row['od_hour'] = date('H'); 
		$this->view->row['od_min'] = date('i');	
		
		$this->view->row['do_day'] = date('d');
		$this->view->row['do_month'] = date('m');
		$this->view->row['do_year'] = date('Y');
		$this->view->row['do_hour'] = date('H'); 
		$this->view->row['do_min'] = date('i');		
		$model = new Banner();
		
		if ($this->cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->cid;
		
		if ($cid > 0) {
			$this->view->rows = $model->getAll('cid = "'.$cid.'"', 'id DESC');		
		} else 	$this->view->rows = $model->getAll(null, 'cid DESC, id DESC');
		$this->view->categoriesList = $this->_getCategoriesList($cid);
	}	
	
	public function insertAction()
	{			
		$this->view->render('index');				
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->row 	  = $params;
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);				
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new BannerFilter();
			$filter->filter();	
			$data = $filter->getData();
			$this->view->categoriesList = $this->_getCategoriesList($data['cid']);
			
		/*	if ($data['www'] != '') {
				$data['www'] = str_replace('http://', '', $data['www']);
				$data['www'] = 'http://'.$data['www'];
			}*/
			
			$data['start'] = mktime($data['od_hour'], $data['od_min'], 0, $data['od_month'], $data['od_day'], $data['od_year']);
			$data['end'] = mktime($data['do_hour'], $data['do_min'], 0, $data['do_month'], $data['do_day'], $data['do_year']);
			
			unset($data['od_hour'], $data['od_min'], $data['od_month'], $data['od_day'], $data['od_year'],
			$data['do_hour'], $data['do_min'], $data['do_month'], $data['do_day'], $data['do_year']);
			
		
			if ($_FILES['photo']['name'] != '' &&  $_FILES['photo']['tmp_name'] != '')	{
				$filename = $this->_upload('photo', DIR_BANNERS);
			}  
			//Tomek
			//else {
			//	$this->_request->redirectFailure(array('Nie przesłano żadnego pliku.'));
			//}	
			/*
			$path = DIR_BANNERS;
			$pathTemp = DIR_BANNERS_TEMP;
		
			Cube_Loader::loadClass('Cube_Upload_Image');
			$image = new Cube_Upload_Image('photo');
			$image->setPath($pathTemp);
			$image->setMaxSize(10240);
			$image->setSaveAs(':category_:name', array(':category' => $data['cid']));	
			$image->move();	
			if ($image->errorExists()) $this->_request->redirectFailure($image->getErrors());	
			$filename = $image->getSaveName();		
			if (file_exists($path.$filename)) $this->_request->redirectFailure(array('Już istnieje w bazie zdjęcie o takiej nazwie pliku ('.$filename.').'));
			copy($pathTemp.$filename, $path.$filename);
			$image->deleteImage();
	|*/
			$data['filename'] = $filename;
			$model = new Banner();	
			$model->insert($data);	
			header('refresh: 3; url=admin,banner,redirect.html');
			$this->view->message = 'Wpis dodany pomyślnie ! Przekierowywanie...';
		}		
	} 
	
	public function redirectAction()
	{
		header('Location: admin,banner.html');
	}
	
	public function editAction() 
	{
		$model = new Banner();
		$row = $model->get($this->_id);
		$this->view->id 	  = $row['id'];
		$this->view->row 	  = $row;
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);

		$this->view->row['od_day'] = date('d', $row['start']);
		$this->view->row['od_month'] = date('m', $row['start']);
		$this->view->row['od_year'] = date('Y', $row['start']);
		$this->view->row['od_hour'] = date('H', $row['start']); 
		$this->view->row['od_min'] = date('i', $row['start']);	
		
		$this->view->row['do_day'] = date('d', $row['end']);
		$this->view->row['do_month'] = date('m', $row['end']);
		$this->view->row['do_year'] = date('Y', $row['end']);
		$this->view->row['do_hour'] = date('H', $row['end']); 
		$this->view->row['do_min'] = date('i', $row['end']);			
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->row 	  = $params;
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);							
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new BannerFilter();
			$filter->filter();
			$data = $filter->getData();
		/*	if ($data['www'] != '') {
				$data['www'] = str_replace('http://', '', $data['www']);
				$data['www'] = 'http://'.$data['www'];
			}*/
			
			$data['start'] = mktime($data['od_hour'], $data['od_min'], 0, $data['od_month'], $data['od_day'], $data['od_year']);
			$data['end'] = mktime($data['do_hour'], $data['do_min'], 0, $data['do_month'], $data['do_day'], $data['do_year']);
			
			unset($data['od_hour'], $data['od_min'], $data['od_month'], $data['od_day'], $data['od_year'],
			$data['do_hour'], $data['do_min'], $data['do_month'], $data['do_day'], $data['do_year']);
						
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,banner.html');
			$this->view->message = 'Wpis zaktualizowany pomyślnie! Przekierowywanie...';	
		}
	}	
	
	public function deleteAction()
	{
		$model = new Banner();
		$row = $model->get($this->_id);	
		if (file_exists(DIR_BANNERS.$row['filename'])) unlink(DIR_BANNERS.$row['filename']);
		if (file_exists(DIR_BANNERS_TEMP.$row['filename'])) unlink(DIR_BANNERS_TEMP.$row['filename']);
		
		$model->delete($this->_id);
		header('refresh: 3; url=admin,banner.html');
		$this->view->message = 'Wpis usunięty pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
	// =====================
	public function categoriesAction()
	{
		$this->view->rows = $this->_getCategories();
	}
	
	public function addcatAction()
	{
		$this->view->render('categories');		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->row   = $params;	
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new BannerCategoryFilter();
			$filter->filter();			
			$model = new BannerCategories();
			$model->insert($filter->getData());	
			header('refresh: 3; url=admin,banner,categories.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new BannerCategories();
		$row = $model->get($this->_id);
		$this->view->id 	  = $row['id'];
		$this->view->row   = $row;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->row   = $params;		
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new BannerCategoryFilter();
			$filter->filter();
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,banner,categories.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';	
		}
	}
	/////
	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) {
			$act = clear($_POST['act']);	
			if ($act == 'delete') {
				$model = new BannerCategories();
				$model->delete($this->_id);
				$model->deleteAllPhotos($this->_id);
				header('refresh: 3; url=admin,banner,categories.html');
				$this->view->message = 'Kategoria oraz bannery usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('categories');		
			} else {
				$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść bannery.'));	
				$model = new BannerCategories();
				$model->delete($this->_id);
				$model->setCategories($this->_id, $cat);
				header('refresh: 3; url=admin,banner,categories.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Bannery przeniesione! Przekierowywanie...';
				$this->view->render('categories');					
			}	
		}
	}	
	
	public function deactiveAction()
	{
		$model = new BannerCategories();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,banner,categories.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('categories');
	}
		
	public function activeAction()
	{
		$model = new BannerCategories();
		$model->active($this->_id);
		header('refresh: 3; url=admin,banner,categories.html');
		$this->view->message = 'Kategoria włączona pomyślnie! Przekierowywanie...';
		$this->view->render('categories');
	}	
	
	public function _upload($name, $dir)
	{
		$tmp = $_FILES[$name]['tmp_name'];
		$fname = $_FILES[$name]['name'];

		$result = '';	
		if (is_uploaded_file($tmp)) {
			$tmp_name = time().'_'.$fname;	
			move_uploaded_file($tmp, $dir.$tmp_name);
			return $tmp_name;
		} else {
			$this->_request->redirectFailure(array('Błąd podczas dodawania pliku! Przekierowywanie...'));	
			return;
		}							
	}
}

?>
