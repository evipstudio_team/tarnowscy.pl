<?php 

class Admin_CanteenController extends Cube_Controller_Abstract
{
	private $_act = null;
	private $_id = null;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','id');
		$title = new Cube_SearchOrder_Field('title','title');
		//$date = new Cube_SearchOrder_Field('date','add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,canteen';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		//$order->addField($date);
		
		
		//echo $order->getLink('id');
		
	}
	
	public function sortAction()
	{
		$model = new Canteen();
		$this->view->render('index');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		$rows =$model->getAll($where,$field.' '.$order);
		
		$model = new CanteenDates();
		$dates = $model->getAll(null, 'id ASC');
		$this->view->rows=$this->showDates($rows,$dates);	
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new Canteen();
		$this->view->render('index');
		$where=$this->_search->createWhere($search);
		
		$rows = $model->getAll($where,'id DESC');
		if (sizeof($rows) < 1)
		{
			header('refresh: 3; url=admin,canteen,index.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
		else
		{
			$model = new CanteenDates();
			$dates = $model->getAll(null, 'id ASC');
			$this->view->rows=$this->showDates($rows,$dates);	
		}
	}
	
	private function create_jsonArray($rows)
	{
		$tmp = array();
		$tmp_row = array();
		foreach ($rows as $r)
		{
			$start_date=$r['start_date'];
			$end_date=$r['end_date'];
			
			for($date=$start_date;$date<=$end_date;$date+=24*3600)
			{
				$tmp_row['linkedDate']=date('Ymd', $date);
				//$tmp_row['dateLink']='admin,canteen,edit,id_'.$r['id_canteen'].'.html';
				
				//nie mozna usunac
				$tmp_row['dateLink']='#';
				
				$tmp[]=$tmp_row;
			}
			
			
		}
		$this->view->array_json=$tmp;
		//print_r($tmp);		
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														INIT
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public function init()
	{
		$this->view->setTemplate('admin');
		//$this->_act = $this->_request->getParam('act');	
		$this->_id = $this->_request->getParam('id');	
		$this->view->id = $this->_id;
		$this->search_order();	

		$this->_photo = new Cube_Upload_JPG('canteen',$this->_request);
		$this->_pdf = new Cube_Upload_PDF('canteen',$this->_request);
		$this->_doc = new Cube_Upload_Document('canteen',$this->_request);	
		
		/*$model = new CanteenDates();
		$dates = $model->getAll(null, 'id ASC');
		$this->create_jsonArray($dates);*/	
		   
	}

	private function showDates($rows,$dates)
	{
		if( count($rows)>0 )
		foreach( $rows as $key => $row )
		{
			
			if( count($dates)>0 )
			{
				$dates_string=null;
				foreach($dates as $date)
				{
					if( $row['id']==$date['id_canteen'] )
					{
						$dates_string.=date('d.m.Y', $date['add_date']).', ';
					}
				}
				$rows[$key]['add_date'] = substr($dates_string, 0, -2);
				//print_r($row);
			}
			else
				$rows[$key]['add_date']='';
		}	
		return $rows;
	
	}
	public function indexAction()
	{	
		$model = new Canteen();
		$rows=$model->getAll(null, 'pos,id DESC');

		$model = new CanteenDates();
		$dates = $model->getAll(null, 'id ASC');
		
		$this->view->rows=$this->showDates($rows,$dates);
		//$this->create_jsonArray($dates);	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														ARTICLES
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	/*private function getSelect($repeat=NULL)
	{
		$temp='';
		$repeat_counts=20;
		for($i=0;$i<=$repeat_counts;$i++)
		{
			if( ($i==0 && is_null($repeat)) ||($i==$repeat) )
				$temp .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';	
			else
				$temp .= '<option value="'.$i.'">'.$i.'</option>';			
		}
		$this->view->selectList=$temp;
	}
	
	private function check_date($add_date,$repeat_count,$start_repeat=0)
	{
		//Jezeli nadpisac to nie sprawdzaj dat
		$model= new CanteenDates();
		for($i=$start_repeat;$i<=$repeat_count;$i++)
		{
			//echo $model->check_date($add_date+$i*7*24*3600);
			if($model->check_date($add_date+$i*7*24*3600) != 0)
			{
				$this->_request->redirectFailure(array('Konflikt dat. Jedna lub wiecej data pokrywają się. Jeżeli nadpisać zaznacz opcję "Nadpisać"! Przekierowywanie...'));
				return;
			}
		}
	}*/
	
	/*private function insertCanteen($add_date,$repeat_count,$id,$start_repeat=0)
	{
		$data_date=array();
		$model= new CanteenDates();
		for($i=$start_repeat;$i<=$repeat_count;$i++)
		{
			$data_date['id_canteen']=$id;
			$new_date=$data_date['add_date']=$add_date+$i*7*24*3600;
			$rows=$model->getAll('add_date='.$new_date);
			if(count($rows)>0)
			{
				//konflikt dat zrob update
				$row=$rows[0];
				$data_update=array();
					
				//wartosc ktora bedzie nadpisana
				$id_canteen=$row['id_canteen'];
					
				//nadpisz aktaualnym id CANTEEN
				$data_update['id_canteen']=$id;
					
				$model->update($row['id'],$data_update);	
					//czy sa jeszcze jakies 
				$rows=$model->getAll('id_canteen='.$id_canteen);
				if(count($rows)<1)
				{
					//usun menu wraz z nalezacym do niego uploadem.
					$model = new Canteen();
					$model->delete($id_canteen);
					$this->_photo->deleteAll($id_canteen);
					$this->_pdf->deleteAll($id_canteen);
					$this->_doc->deleteAll($id_canteen);
					$model= new CanteenDates();
				}
					
			}
			else
				$model->insert($data_date);	
		}
	}*/
		
	public function insertAction()
	{		
		
		//$this->getSelect();
		//$this->view->row['add_date'] = now(true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			//$repeat=$this->view->row['repeat_count'];
			//$this->getSelect($repeat);	
			$this->view->id = $this->view->row['id'];	
			return;
		}
		if ($this->_request->isPost()) 
		{
			//$repeat_count=(int) clear($_POST['repeat_count']);
			//$overwrite=(int) clear($_POST['overwrite']);
			$filter = new CanteenFilter();
			$filter->filter();			
			$model = new Canteen();
			$data=$filter->getData();
			//$repeat_count=$data['repeat_count'];
			//$overwrite=$data['overwrite'];
			//if( $overwrite!=1 )
			//	$this->check_date($add_date,$repeat_count);
			//unset ($data['repeat_count']);
			//unset ($data['overwrite']);	
			//unset ($data['add_date']);
			$id=$model->insert($data);
			//$this->insertCanteen($add_date,$repeat_count,$id);
			header('refresh: 3; url=admin,canteen,edit,id_'.$id.'.html');	
			$this->view->message = 'Apartament dodany pomyślnie! Przekierowywanie...';
		}
	}
	
	public function editAction()
	{
		$model= new Upload();
		$this->view->photos=$model->getAll('module="canteen" AND category="photo" AND id_element='.$this->_id,'pos,id');
		$this->view->pdf=$model->getAll('module="canteen" AND category="pdf" AND id_element='.$this->_id,'pos,id');
		$this->view->doc=$model->getAll('module="canteen" AND category="document" AND id_element='.$this->_id,'pos,id');
		
		$model = new CanteenDates();
		$this->view->dates = $dates= $model->getAll('id_canteen='.$this->_id, 'id ASC');
		$this->create_jsonArray($dates);	
		
		
		$this->view->start_date=mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
		$this->view->end_date=mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
		
		
		$model = new Canteen();
		$row = $model->get($this->_id);
		$this->view->row = $row;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest();
			$this->view->id = $this->view->row['id'];
			return;
		}		
		if ($this->_request->isPost()) 
		{
			
			$filter = new CanteenFilter();
			$filter->filter();
			$data=$filter->getData();
			
			$model->update($this->_id,$data);
			
			header('refresh: 3; url=admin,canteen.html');
			$this->view->message = 'Apartament zaktualizowany pomyślnie! Przekierowywanie...';	
		}
	}
	
	public function posdeleteAllAction()
	{
		
		if ($this->_request->isPost())
		{  
			$this->view->render('edit');
			$model = new Canteen();
			$rows = $model->getAll(null, 'id DESC');
			
			if(isset($_POST['pos']))
			{
				
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) 
					{
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePos($r['id'], $v);	
				}
				
				
				header('refresh: 3; url=admin,canteen,index.html');
				$this->view->message = 'Apartamenty zaktualizowane pomyślnie! Przekierowywanie...';
				$this->view->render('index');	
			}

			if(isset($_POST['deleteMark']))
			{
				foreach ($rows as $r)
				{
					$checked = (int)clear($_POST['delete_'.$r['id']]);
					if ($checked == '1') 
					{
						$model->delete($r['id']);
						$this->_photo->deleteAll($r['id']);
						$this->_pdf->deleteAll($r['id']);
						$this->_doc->deleteAll($r['id']);
						$model = new CanteenDates();
						$model->deleteCanteen($r['id']);
						$model = new Canteen();	
					}
				}
				$this->view->message = 'Apartamenty usunięte pomyślnie! Przekierowywanie...';
				header('refresh: 3; url=admin,canteen,index.html');
			}
		}
	}
		
	public function deleteAction()
	{
		$model = new Canteen();
		$model->delete($this->_id);
		$this->_photo->deleteAll($this->_id);
		$this->_pdf->deleteAll($this->_id);
		$this->_doc->deleteAll($this->_id);
		
		$model = new CanteenDates();
		$model->deleteCanteen($this->_id);
		
		header('refresh: 3; url=admin,canteen.html');
		$this->view->message = 'Apartament usunięty pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										DATY
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
public function addDatePeriodAction()
{
	$this->view->render('edit');
	if ($this->_request->isRedirected()) {
		$this->view->errors = $this->_request->getMessagesFromLastRequest();
		$this->view->row = $this->_request->getParamsFromLastRequest(); 
		$this->view->id = $this->view->row['id'];
		$this->view->start_date = convertToTimeStamp($this->view->row['start_date']);
		$this->view->end_date = convertToTimeStamp($this->view->row['end_date']);	
		return;
	}

	if ($this->_request->isPost()) 
	{
		$filter = new CanteenDateFilter();
		$filter->filter();			
		$data=$filter->getData();
		
		$data['start_date']=convertToTimeStamp($data['start_date']);
		$data['end_date']=convertToTimeStamp($data['end_date']);
		if($data['start_date']-$data['end_date'] >0) 
		{
			$this->_request->redirectFailure(array('Data początku nie może być większa od daty końca. Przekierowywanie...'));
			return;
		}
		$model = new CanteenDates();
		if( !$data['overwrite'] )
		{
			if( $model->check_date($data['start_date'],$data['end_date'],$this->_id) )
			{
				$this->_request->redirectFailure(array('Uwaga okredy pokrywają się. Jażeli nadpisac zaznacz checkbox "Nadpisać" Przekierowywanie...'));
				return;
			}
		}
		$data['id_canteen']=$this->_id;
		
		$model->insert($data);
			//$this->insertCanteen($add_date,$repeat_count,$id);
		header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
		$this->view->message = 'Apartament dodany pomyślnie! Przekierowywanie...';	
	}
	
	
}

public function editDatePeriodAction()
{
	
	$this->view->idDate = $idDate = $this->_request->getParam('idDate', 0);
	$this->view->id=$this->_id;
	
	$model = new CanteenDates();
	$row = $model->get($idDate);
	
	$dates= $model->getAll('id_canteen='.$this->_id, 'id ASC');
	$this->create_jsonArray($dates);	
	
	//print_r($row);
	$this->view->start_date =$row['start_date'];
	$this->view->end_date = $row['end_date'];
	if( $idDate==0 )
		$this->view->idDate=$row['id'];
	
	if ($this->_request->isRedirected()) {
		$this->view->errors = $this->_request->getMessagesFromLastRequest();
		$this->view->row = $this->_request->getParamsFromLastRequest(); 
		$this->view->id = $this->view->row['id'];
		$this->view->start_date = convertToTimeStamp($this->view->row['start_date']);
		$this->view->end_date = convertToTimeStamp($this->view->row['end_date']);	
		return;
	}

	if ($this->_request->isPost()) 
	{
		$filter = new CanteenDateFilter();
		$filter->filter();			
		$data=$filter->getData();
		$data['start_date']=convertToTimeStamp($data['start_date']);
		$data['end_date']=convertToTimeStamp($data['end_date']);
		if($data['start_date']-$data['end_date'] >0) 
		{
			$this->_request->redirectFailure(array('Data początku nie może być większa od daty końca. Przekierowywanie...'));
			return;
		}
		$model = new CanteenDates();
		if( !$data['overwrite'] )
		{
			if( $model->check_date($data['start_date'],$data['end_date'],$idDate) )
			{
				$this->_request->redirectFailure(array('Uwaga okredy pokrywają się. Jażeli nadpisac zaznacz checkbox "Nadpisać" Przekierowywanie...'));
				return;
			}
		}
		$data['id_canteen']=$this->_id;
		
		$model->update($idDate,$data);
			//$this->insertCanteen($add_date,$repeat_count,$id);
		header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
		$this->view->message = 'Apartament dodany pomyślnie! Przekierowywanie...';	
	}
	
	
}



public function deleteDatePeriodAction()
	{
		//ECHO 'EDITPHOTO';
		$this->view->idDate = $idDate = $this->_request->getParam('idDate', 0);
		$this->view->id=$this->_id;
		
		if( $idDate > 0 )
		{
			$model = new CanteenDates();
			$model->delete($idDate);
						
			header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
			$this->view->message = 'Daty usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}	
	}
	
	
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										FILE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function setmainPhotoAction()
	{
		$idFile = $this->_request->getParam('idFile', 0);
		if( $idFile > 0)
		{
			$model= new Upload();
			$model->setmain($idFile,$this->_id,'canteen');
			header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}
	}
	
	public function docPosdeleteMarkAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	public function pdfPosdeleteMarkAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
		
	
	public function photoPosdeleteMarkAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->filePosdeleteMark($obj,$msgWordString);
		
	}
	
	private function filePosdeleteMark($obj,$msgWordString)
	{
		if ($this->_request->isPost())
		{  
			$category=$obj->getCategory();
			
			$files=array();
			$model= new Upload();
			$files=$model->getAll('module="canteen" AND category="'.$category.'" AND id_element='.$this->_id,'pos,id');
			if(isset($_POST['pos']))
			{
				
				foreach ($files as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) 
					{
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePos($r['id'], $v);	
				}
				
				
				header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
				$this->view->render('index');	
			}

			if(isset($_POST['deleteMark']))
			{
				
				foreach ($files as $r)
				{
					$checked = (int)clear($_POST['delete_'.$r['id']]);
					if ($checked == '1') 
					{
						$obj->deleteOne($r['id']);	
					}
					
				}
				
				header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');					
			
			}
			
		}
	}
	
	public function deleteDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->deleteFile($obj,$msgWordString);
	}
	
	
	public function deletePDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	
	public function deletePhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	private function deleteFile(Cube_Upload_File $obj,$msgWordString)
	{
		//ECHO 'EDITPHOTO';
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$obj->deleteOne($idFile);
						
			header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
			$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}	
	}
	
	public function editDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->editFile($obj,$msgWordString);
	}	
	
	private function editFile(Cube_Upload_File $obj,$msgWordString)
	{
	//ECHO 'EDITPHOTO';
		if ($this->_request->isRedirected()) 
					$this->view->errors = $this->_request->getMessagesFromLastRequest();
		
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$files=array();
			$model= new Upload();
			$this->view->files=$files=$model->getAll('id='.$idFile,'id');
			if ($this->_request->isPost()) 
			{
				
				$title=clear($_POST['title']);
				$idFile=$files[0]['id'];
				if ($_FILES['file']['name'] != '' &&  $_FILES['file']['tmp_name'] != '')
				{	
					//jezeli zmienione zostalo zdjecie
					$obj->setTitle($title);
					$obj->add('file');
					//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
					$obj->update($this->_id,$idFile);
				}
				else if ($title != '')
				{
					//jezeli zmieniony zostal tytul
					$data['title']=$title;
					$model=new Upload();
					$model->update($idFile,$data);
				
				}
					header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
					$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
					$this->view->render('index');	
				
			}
		}
	
	}
	
	
	public function addDocAction()
	{
		$counter_file=2;
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	public function addPDFAction()
	{
		$counter_file=2;
		$obj=$this->_pdf;
		$msgWordString='PDF';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	
	public function addPhotoAction()
	{
		$counter_file=4;
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	private function addFile($counter_file,Cube_Upload_File $obj,$msgWordString)
	{
		$success = false;
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
			$this->view->render('edit');
			header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
			return;	
		}	
		
		for($i = 1; $i <= $counter_file; $i++)
		{
			if ($_FILES['file'.$i]['name'] != '' &&  $_FILES['file'.$i]['tmp_name'] != '')	
			{
				$title=clear($_POST['title'.$i]);
				$obj->setTitle($title);
				//if( count($photos)>0 )
				{						
						$obj->add('file'.$i);
						$obj->insert($this->_id);
				}
				$success=true;
			}
		}
		
		header('refresh: 3; url=admin,canteen,edit,id_'.$this->_id.'.html');
		if($success)
			$this->view->message = $msgWordString.' dodane pomyślnie! Przekierowywanie...';
		else
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
		$this->view->render('index');	
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														PRIVATE
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
	
	
	private function _getCategoriesList($cid = null, $no = false)
	{
		$model = new AdvGalleryTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].'/'.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].'/';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}	
}

?>
