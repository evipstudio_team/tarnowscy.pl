<?php 

class Admin_TimetableController extends Cube_Controller_Abstract
{
	private $_id;
	private $_cid;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	//parametry dla SQL do indexACTION.
	private $_where=null;
	private $_field=null;
	private $_sequence=null;
	
	
	private function search_order()
	{
		$this->view->state = $state = $this->_request->getParam('state',1);
		$this->view->column = $column = $this->_request->getParam('column');	
		$this->view->search_string = $search_string = $this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','p.id');
		$title = new Cube_SearchOrder_Field('title','p.title');
		//$filename = new Cube_SearchOrder_Field('filename','u.name');
		//$date = new Cube_SearchOrder_Field('date','u.add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		//$search->addField($filename);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,timetable';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		//$order->addField($filename);
		//$order->addField($date);
				
		$this->_where=$this->_order->createWhere();
		$this->_field=$column;
		//echo 'COLUMN='.$column;
		if( ($column!='') )
		{
			//jezeli bylo sortowanie
			$this->_sequence=$this->_order->getOrder();
			//echo 'SEQUENCE='.$this->_sequence;
		}
		
	}
	
	private function create_pdf($body,$filename)
	{
		
		//print_r($body);
		
		//dodaj szblon
		require 'masc/pdf/template.php';  //zwraca html i dodaje body
		
		require_once 'libs/Mpdf/mpdf.php'; // ładujemy klasę
		$mpdf=new mPDF('utf-8','A4','',''); // ustawiamy parametry strony
		$mpdf->SetCompression(true); // włączamy kompresję
		$mpdf->SetProtection(array('print')); // Uprawnienia dokumentu
		//$mpdf->SetTitle("Tytuł dokumentu"); // Podajemy tytuł dokumentu
		//$mpdf->SetAuthor("CodeTip"); // Wprowadzamy autora
		//$mpdf->SetWatermarkText("CodeTip - Porady i Sztuczki"); // umieszczamy znak wodny
		//$mpdf->showWatermarkText = false; // Pokazujemy lub nie, znak wodny powyżej
		//$mpdf->watermark_font = 'DejaVuSansCondensed'; // Czcionka znaku wodnego
		//$mpdf->watermarkTextAlpha = 0.05; // Przeźroczystość znaku wodnego
		$mpdf->SetDisplayMode('fullpage'); // Typ wyświetlenia
		$mpdf->WriteHTML($html); // Przetworzenie treści zmiennej $html*/
		//echo $html;
		$mpdf->Output($filename.'.pdf','D'); // Wygenerowanie i wysłanie do użytkownika pliku
	
	}
	
	
	
	private function _getBranchPatch($cid)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new TimetableTree();
		$rows=$model->getBranchTree($cid);
		return $rows;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		/*$temp='';
		foreach($rows as $row)
			$temp.=$row['name'].'/';
		
		return $temp;*/
		
	}
	
	private function _getCategoriesList($cid = null, $no = false)
	{
		$model = new TimetableTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].' &raquo; '.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].' &raquo; ';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}

	private function _getSubCategories($parent_id)
	{
		$model = new TimetableTree();
		$rows=$model->getAllChildren($parent_id);
		
		//ile dokumentow jest w kazdej kategorii
		$model = new Timetable();
		foreach ($rows as $i=>$row)
		{
			$temp=$model->count_rows('cid='.$row['id']);	
			$rows[$i]['amount']=$temp['amount'];
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		return $rows;		
	}

	private function getRowsIndex()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->cid=$cid;
	
		//wyswietl rozklady
		$model = new Timetable();
		
		//przygotuj zmienne pod zapytanie
		if( !is_null($this->_sequence) )
			$order=$this->_sequence.',';
		else
			$order=$this->_sequence;
		$field=$this->_field;
		$where=$this->_where;
		
		
		if ($this->_cid > 0)
		{
			if( !is_null($where) ) 
				$where=' AND '.$where;
			
			$rows = $model->getAll('p.cid = "'.$cid.'"'.$where,$field.' '.$order.'p.pos,p.id DESC');	
		}
		else
			$rows =$model->getAll($where,$field.' '.$order.'p.cid DESC,p.pos_all,p.id DESC');	
			
		return $rows;	
	}
	
	private function getRowsEdit()
	{
		$model = new Timetable();
		$rows = $model->getAll('p.id='.$this->_id);
		return $rows[0];
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														PUBLIC
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->username = $this->_session->getUsername();
		$this->_id = $this->_request->getParam('id', 0);
		$this->view->id = $this->_id;
		//$this->_cid = $this->_request->getParam('cid', 0);
		//$this->view->cid = $this->_cid;	
		 $this->view->cid=$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->jsTree = $this->displayJsTree2($this->_cid);
		
		$this->search_order();
		$this->_photo = new Cube_Upload_JPG('timetable',$this->_request);
		$this->_pdf = new Cube_Upload_PDF('timetable',$this->_request);
		$this->_doc = new Cube_Upload_Document('timetable',$this->_request);
		
		//echo $this->_request->getAction();
	}

	
	
	
	public function indexAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->cid=$cid;
		
		$this->view->rows = $this->_getSubcategories($cid);		
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		
		$this->view->elements=$this->getRowsIndex();	
			
	}
	
		
	public function sortAction()
	{
		$this->_where=$this->_order->createWhere();
		$this->_field=$this->_order->getFieldSql();
		$this->_sequence=$this->_order->getOrder();
		
		$this->view->render('index');
		$this->indexAction();		
		
	}	
		
	
	public function searchAction()
	{
		$this->view->search_string = $search=clear($_POST['search']);
		$this->_where=$this->_search->createWhere($search);
		$this->view->render('index');
		$this->indexAction();		
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														PRINT - PDF
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function printindexAction()
	{
		//drukuj liste zgloszen
		$this->view->render('index');
		$this->view->print=1;
		$this->indexAction();
		
		
		//$this->indexAction();
	}
	
	public function printeditAction()
	{
		//drukuj liste zgloszen
		$this->view->render('edit');
		$this->view->print=1;
		$this->view->row = $this->getRowsEdit();		
	}
	
	public function pdfeditAction()
	{
		$body='';
		$this->view->render('edit');
		$pdf_rows=$this->getRowsEdit();
		
		require_once 'templates/admin/pdf_edit.php';
		$filename='Lista zgłoszeń';
		//echo $body;
		$this->create_pdf($body,$filename);
	}
	
	
	public function pdfindexAction()
	{
		$module=$this->_request()->getController();
		$body='';
		$this->view->render('index');
		$pdf_rows=$this->getRowsIndex();
		
		require_once 'templates/admin/pdf_index.php';
		$filename='Lista zgłoszeń';
		//echo $body;
		$this->create_pdf($body,$filename);
	}
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														ELEMENTS
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function setmainElementAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->render('index');
		if(( $this->_id > 0) && ( $cid>0 ))
		{
			$model= new Timetable();
			$model->setmain($this->_id);
			header('refresh: 3; url=admin,timetable,index,cid_'.$cid.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
				
		}
	}
	
	
	public function posdeleteMarkAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->render('index');
		$model = new Timetable();
		if(isset($_POST['pos']))
		{
			if( $cid>0 )
			{
				$rows = $model->getAll('p.cid = "'.$cid.'"', 'p.pos,p.id DESC');
				//print_r($rows);
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosCategory($r['id'], $v);	
				}
		
				$this->view->message = 'Pozycje kategorii zaktualizowane pomyślnie! Przekierowywanie...';
				header('refresh: 3; url=admin,timetable,index,cid_'.$cid.'.html');
			}
			else
			{
				$rows =$model->getAll(null,'p.pos_all,p.id DESC');
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosTimetable($r['id'], $v);	
				}
			
			$this->view->message = 'Pozycje zaktualizowane pomyślnie! Przekierowywanie...';
			
			}
		}
		if(isset($_POST['deleteMark']))
		{
			if( $cid>0 )
				$rows = $model->getAll('p.cid = "'.$cid.'"', 'p.pos,p.id DESC');
			else
				$rows =$model->getAll(null,'p.pos_all,p.id DESC');
			foreach ($rows as $r)
			{
				$checked = (int)clear($_POST['delete_'.$r['id']]);
				if ($checked == '1') 
				{
					$model->delete($r['id']);
					$this->_photo->deleteAll($r['id']);
					$this->_pdf->deleteAll($r['id']);
					$this->_doc->deleteAll($r['id']);		
				}
					
			}
			$this->view->message = 'Projety usunięte pomyślnie! Przekierowywanie...';
		}
		header('refresh: 3; url=admin,timetable,index.html');
		
	}	
	
	public function insertAction()
	{			
		if ($this->_cid == 0) $this->view->cid=$cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
			$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		
		//$this->view->render('elements');
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $params = $this->_request->getParamsFromLastRequest(); 
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);				
			return;
		}
		if ($this->_request->isPost()) 
		{
		
			$filter = new TimetableFilter();
			$filter->filter();	
			$data = $filter->getData();
			
			//print_r($data);
			$model = new Timetable();		
			$id=$model->insert($data);
				
			header('refresh: 3; url=admin,timetable,edit,id_'.$id.'.html');	
			$this->view->message = 'Rozkład jazdy dodany pomyślnie! Przekierowywanie...';
		}
			
	} 
	
	public function editAction() 
	{
		$model= new Upload();
		$this->view->photos=$model->getAll('module="timetable" AND category="photo" AND id_element='.$this->_id,'pos,id');
		$this->view->pdf=$model->getAll('module="timetable" AND category="pdf" AND id_element='.$this->_id,'pos,id');
		$this->view->doc=$model->getAll('module="timetable" AND category="document" AND id_element='.$this->_id,'pos,id');
				
		$this->view->row = $row=$this->getRowsEdit();
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		
		$model = new Timetable();
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $params = $this->_request->getParamsFromLastRequest(); 
			$this->view->categoriesList = $this->_getCategoriesList($params['oldCid']);	
			$this->view->row['id']=$params['oldCid'];	
			return;
		}
		
		if ($this->_request->isPost()) 
		{
			$filter = new TimetableFilter();
			$filter->filter();
			$data=$filter->getData();
			$model->update($this->_id,$data);
			
			//tabela upload
			/*$title=$data['title'];
			$obj->setTitle($title);
				
			$obj->add('file');
			//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
			$idFile=$row['idFile'];
			$obj->update($this->_id,$idFile);*/
			
			//echo 'ID='.$this->_id;
			//print_r($filter->getData());
			header('refresh: 3; url=admin,timetable.html');
			$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';	
		}
	}	
	
	public function deleteAction()
	{
		$model = new Timetable();
		$rows = $model->getAll('p.id='.$this->_id);
		$row = $rows[0];
			
		$model->delete($this->_id);
		$this->_photo->deleteAll($this->_id);
		$this->_pdf->deleteAll($this->_id);
		$this->_doc->deleteAll($this->_id);
		
		
		header('refresh: 3; url=admin,timetable.html');
		$this->view->message = 'Wpis usunięty pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														CATEGORIES
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	public function poscategoriesAction()
	{
		$this->view->render('index');
		$model = new TimetableTree();
		$rows=$model->getAllChildren($this->_cid);	
		
		foreach ($rows as $r)
		{
			$data['pos'] = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $data);	
		}
		header('refresh: 3; url=admin,timetable,index,cid_'.$this->_cid.'.html');
		$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';
	}	
	
	
	public function addcatAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->name = null;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			
			$cid=$this->view->cid 	  = $params['cid'];
			$this->view->name   = $params['name'];	
			$this->view->currentPath = $this->_getBranchPatch($cid);
			$this->view->categoriesList = $this->_getCategoriesList($cid);		
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new TimetableCategoryFilter();
			$filter->filter();			
			$model = new TimetableTree();
			$data = $filter->getData();
			
			//$parent = $this->_categoriesModel->get($data['pid']);
			$parent = $model->get($data['cid']);
			$data['parent_id'] = $data['cid'];
			unset($data['cid']);
					
			$model->insertTree($data);	
			header('refresh: 3; url=admin,timetable,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new TimetableTree();
		$row = $model->get($this->_id);
		//print_r($row);
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		//$this->view->description   = $row['description'];	
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];	
			//$this->view->description   = $params['description'];
					
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new TimetableEditCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			//$this->_cid=$data['cid'];
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,timetable,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';		
		}
		
	}
	
	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) 
		{
			$filter = new TimetableDeleteCategoryFilter();
			$filter->filter();	
			$data = $filter->getData();
			
			$act=$data['act'];
			$cat=$data['cat'];
			$class=array('Timetable');	//Tabela z nazwami modeli do tabel zaiazanych z drzewem z ktorych trzeba usunac np. dokumenty, zdjecia itp
			if ($act == 'delete') {
							
				$model = new TimetableTree();
				$model->deleteTree($this->_id,$class);	
				header('refresh: 3; url=admin,timetable,index.html');
				$this->view->message = 'Kategoria oraz rozklady usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');		
			} 
			
			else if ($act == 'przenies_doc')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść rozklady.'));	
				$model = new TimetableTree();
				$model->deleteTree($this->_id,$class,$cat,false);
					header('refresh: 3; url=admin,timetable,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Rozkład jazdyy przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			} 
			
			else if ($act == 'przenies_cat')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść rozklady i podkategorie.'));	
				$model = new TimetableTree();
				$model->moveAllBranchTree($this->_id,$cat);
				//$model = new TimetableTree();
				$model->deleteTree($this->_id,$class,$cat,false);	
				header('refresh: 3; url=admin,timetable,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Rozkład jazdyy przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			}	
		}
	}	
	
	public function deactivecatAction()
	{
		$model = new TimetableTree();
		$model->deactiveSubTree($this->_id);
		header('refresh: 3; url=admin,timetable,index.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}
		
	public function activecatAction()
	{
		$model = new TimetableTree();
		$model->activeSubTree($this->_id);
		header('refresh: 3; url=admin,timetable,index.html');
		$this->view->message = 'Kategoria włączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										FILE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function setmainPhotoAction()
	{
		$idFile = $this->_request->getParam('idFile', 0);
		if( $idFile > 0)
		{
			$model= new Upload();
			$model->setmain($idFile,$this->_id,'timetable');
			header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}
	}
	
	public function docPosdeleteMarkAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	public function pdfPosdeleteMarkAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	
	public function photoPosdeleteMarkAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->filePosdeleteMark($obj,$msgWordString);
		
	}
	
	private function filePosdeleteMark($obj,$msgWordString)
	{
		if ($this->_request->isPost())
		{  
			$category=$obj->getCategory();
			
			$files=array();
			$model= new Upload();
			$files=$model->getAll('module="timetable" AND category="'.$category.'" AND id_element='.$this->_id,'pos,id');
			if(isset($_POST['pos']))
			{
				
				foreach ($files as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) 
					{
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePos($r['id'], $v);	
				}
				
				
				header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
				$this->view->render('index');	
			}

			if(isset($_POST['deleteMark']))
			{
				
				foreach ($files as $r)
				{
					$checked = (int)clear($_POST['delete_'.$r['id']]);
					if ($checked == '1') 
					{
						$obj->deleteOne($r['id']);	
					}
					
				}
				
				header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');					
			
			}
			
		}
	}
	
	public function deleteDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->deleteFile($obj,$msgWordString);
	}
	
	
	public function deletePDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	
	public function deletePhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	private function deleteFile(Cube_Upload_File $obj,$msgWordString)
	{
		//ECHO 'EDITPHOTO';
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$obj->deleteOne($idFile);
						
			header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
			$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}	
	}
	
	public function editDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->editFile($obj,$msgWordString);
	}	
	
	private function editFile(Cube_Upload_File $obj,$msgWordString)
	{
	//ECHO 'EDITPHOTO';
		if ($this->_request->isRedirected()) 
					$this->view->errors = $this->_request->getMessagesFromLastRequest();
		
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$files=array();
			$model= new Upload();
			$this->view->files=$files=$model->getAll('id='.$idFile,'id');
			if ($this->_request->isPost()) 
			{
				
				$title=clear($_POST['title']);
				$idFile=$files[0]['id'];
				if ($_FILES['file']['name'] != '' &&  $_FILES['file']['tmp_name'] != '')
				{	
					//jezeli zmienione zostalo zdjecie
					$obj->setTitle($title);
					$obj->add('file');
					//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
					$obj->update($this->_id,$idFile);
				}
				else if ($title != '')
				{
					//jezeli zmieniony zostal tytul
					$data['title']=$title;
					$model=new Upload();
					$model->update($idFile,$data);
				
				}
					header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
					$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
					$this->view->render('index');	
				
			}
		}
	
	}
	
	
	public function addDocAction()
	{
		$counter_file=2;
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	public function addPDFAction()
	{
		$counter_file=2;
		$obj=$this->_pdf;
		$msgWordString='PDF';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	
	public function addPhotoAction()
	{
		$counter_file=4;
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	private function addFile($counter_file,Cube_Upload_File $obj,$msgWordString)
	{
		$success = false;
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
			$this->view->render('edit');
			header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
			return;	
		}	
		
		for($i = 1; $i <= $counter_file; $i++)
		{
			if ($_FILES['file'.$i]['name'] != '' &&  $_FILES['file'.$i]['tmp_name'] != '')	
			{
				$title=clear($_POST['title'.$i]);
				$obj->setTitle($title);
				//if( count($photos)>0 )
				{						
						$obj->add('file'.$i);
						$obj->insert($this->_id);
				}
				$success=true;
			}
		}
		
		header('refresh: 3; url=admin,timetable,edit,id_'.$this->_id.'.html');
		if($success)
			$this->view->message = $msgWordString.' dodane pomyślnie! Przekierowywanie...';
		else
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
		$this->view->render('index');	
	}
	
		
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														CONFIG
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function deactiveArticlesAction()
	{
		$this->_config->update('articles', 0, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeArticlesAction()
	{
		$this->_config->update('articles', 1, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function deactiveNewsAction()
	{
		$this->_config->update('timetable', 0, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeNewsAction()
	{
		$this->_config->update('timetable', 1, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function deactiveCatsAction()
	{
		$this->_config->update('timetable_categories', 0, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeCatsAction()
	{
		$this->_config->update('timetable_categories', 1, 'timetable');
		
		header('refresh: 3; url=admin,timetable,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('timetable'); 
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
			//echo $s['k'].' '.$s['v'];
		}
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {		
			$pp = (int)clear($_POST['per_page']);
			$pc = (int)clear($_POST['per_categories']);
			
			if ($pp == '') $this->_request->redirectFailure(array('Pole "Zdjęć na stronę" jest wymagane.'));
			if ($pc == '') $this->_request->redirectFailure(array('Pole "Kategorie na stronę" jest wymagane.'));
								
			$this->_config->update('per_page', $pp, 'timetable');
			$this->_config->update('per_categories', $pc, 'timetable');

			header('refresh: 3; url=admin,timetable,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie! Przekierowywanie...';
		}			
	}
	
	private function displayJsTree2($current)
	{
		$conts = "<a href=\"javascript: d.openAll();\" class=\"close\">+ rozwiń drzewko</a><a href=\"javascript: d.closeAll();\" class=\"close\">- zwiń drzewko</a><br clear=\"all\" /><br />
			<div class=\"dtree\">
			<noscript>
				Twoja przegladarka nie obsługuje javascript!
			</noscript>
			<script type=\"text/javascript\">
		<!--

		d = new dTree('d');";
		
		$model = new TimetableTree();
		$rows=$model->getAllTreeWSTCC(null,'t.pos,t.name');
		
		
		if(count($rows) > 0)
		{
			
			//$was = false;
			foreach($rows as $row)
			{
				if( !isSet($row['count_child']) )
					$row['count_child']=0;
					
				$link='admin,timetable,index,cid_'.$row['id'].'.html';
				require 'templates/admin/tree/show_tree.php';
		}
							
 
		$conts .= '
			d.openTo('.$current.', true);
			document.write(d);
		
		//-->
		</script>
		</div>';
				
		} else $conts = '<p>brak kategorii</p>';
		
		return $conts;	
	}
}

?>
