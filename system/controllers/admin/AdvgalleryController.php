<?php 

class Admin_AdvgalleryController extends Cube_Controller_Abstract
{
	private $_id;
	private $_cid;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','p.id');
		$title = new Cube_SearchOrder_Field('title','u.title');
		$filename = new Cube_SearchOrder_Field('filename','u.name');
		$date = new Cube_SearchOrder_Field('date','u.add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		$search->addField($filename);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,advgallery';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		$order->addField($filename);
		$order->addField($date);
		
		
		//echo $order->getLink('id');
		
	}
	
	private function _getBranchPatch($cid)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new AdvGalleryTree();
		$rows=$model->getBranchTree($cid);
		return $rows;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		/*$temp='';
		foreach($rows as $row)
			$temp.=$row['name'].'/';
		
		return $temp;*/
		
	}
	
	private function _getCategoriesList($cid = null, $no = false)
	{
		$model = new AdvGalleryTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].' &raquo; '.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].' &raquo; ';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}

	private function _getSubCategories($parent_id)
	{
		$model = new AdvGalleryTree();
		$rows=$model->getAllChildren($parent_id);
		
		//ile dokumentow jest w kazdej kategorii
		$model = new AdvGallery();
		foreach ($rows as $i=>$row)
		{
			$temp=$model->count_rows('cid='.$row['id']);	
			$rows[$i]['amount']=$temp['amount'];
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		return $rows;		
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														PUBLIC
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->username = $this->_session->getUsername();
		$this->_id = $this->_request->getParam('id', 0);
		$this->view->id = $this->_id;
		//$this->_cid = $this->_request->getParam('cid', 0);
		//$this->view->cid = $this->_cid;	
		 $this->view->cid=$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->jsTree = $this->displayJsTree2($this->_cid);
		
		$this->search_order();
		$this->_photo = new Cube_Upload_JPG('advgallery',$this->_request);
		
	}

	public function indexAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->cid=$cid;
		//echo 'CID='.$cid;
		//$this->view->jsTree = $this->displayJsTree2($cid);
		$this->view->rows = $this->_getSubcategories($cid);		
		//$cats =  $this->_getCategories();
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		//$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		//wyswietl zdjecia
		$model = new AdvGallery();
		if ($cid > 0) {
			$this->view->photos = $photos = $model->getAll('p.cid = "'.$cid.'"','p.pos,p.id DESC');		
		} else 	$this->view->photos = $photos =$model->getAll(null, 'p.cid DESC, p.pos_all,p.id DESC');
		
		//$this->view->photos = $photos;		
	}
	
	
	
	
	public function sortAction()
	{
		$model = new AdvGallery();
		$this->view->render('index');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		//$this->view->categoriesList = $this->_getCategoriesList($cid);
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		$this->view->rows = $this->_getSubcategories($cid);	
		
		if ($cid > 0) {
			$this->view->photos = $model->getAll('p.cid = "'.$cid.'" AND '.$where,$field.' '.$order);		
		} 
		else 
			$this->view->photos =$model->getAll($where,$field.' '.$order);
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new AdvGallery();
		$this->view->render('index');
		$where=$this->_search->createWhere($search);
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		$this->view->rows = $this->_getSubcategories($cid);	
		
		if ($cid > 0) {
			$this->view->photos = $model->getAll('p.cid = "'.$cid.'" AND '.$where, 'p.id DESC');		
		} 
		else 
			$this->view->photos = $model->getAll($where,'p.id DESC');
			
		if (sizeof($this->view->photos) < 1)
		{
			header('refresh: 3; url=admin,advgallery,index.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
	}

	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														PHOTOS
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function setmainPhotoAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->render('index');
		if(( $this->_id > 0) && ( $cid>0 ))
		{
			$model= new AdvGallery();
			$model->setmain($this->_id);
			header('refresh: 3; url=admin,advgallery,index,cid_'.$cid.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
				
		}
	}
	
	
	public function posdeleteMarkAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->render('index');
		$model = new AdvGallery();
		if(isset($_POST['pos']))
		{
			if( $cid>0 )
			{
				$rows = $model->getAll('p.cid = "'.$cid.'"', 'p.pos,p.id DESC');
				//print_r($rows);
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosCategory($r['id'], $v);	
				}
		
				$this->view->message = 'Pozycje kategorii zaktualizowane pomyślnie! Przekierowywanie...';
				header('refresh: 3; url=admin,advgallery,index,cid_'.$cid.'.html');
			}
			else
			{
				$rows =$model->getAll(null,'p.pos_all,p.id DESC');
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosAdvgallery($r['id'], $v);	
				}
			
			$this->view->message = 'Pozycje zaktualizowane pomyślnie! Przekierowywanie...';
			
			}
		}
		if(isset($_POST['deleteMark']))
		{
			if( $cid>0 )
				$rows = $model->getAll('p.cid = "'.$cid.'"', 'p.pos,p.id DESC');
			else
				$rows =$model->getAll(null,'p.pos_all,p.id DESC');
			foreach ($rows as $r)
			{
				$checked = (int)clear($_POST['delete_'.$r['id']]);
				if ($checked == '1') 
				{
					$model->delete($r['id']);
					$this->_photo->deleteAll($r['id']);	
				}
					
			}
			$this->view->message = 'Zdjęcia usunięte pomyślnie! Przekierowywanie...';
		}
		header('refresh: 3; url=admin,advgallery,index.html');
		
	}	
	
	public function photosAction()
	{		
		$model = new AdvGallery();
		
		
		if ($this->_cid == 0) $this->view->cid=$cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		//if ($cid > 0) {
		//	$this->view->rows = $rows= $model->getAll('p.cid = "'.$cid.'"', 'p.id DESC');		
		//} else 	$this->view->rows = $rows= $model->getAll(null, 'p.cid DESC, p.id DESC');
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		//dodaj sciezke do pliku
		//$this->view->photos = $rows;		
		
	}	
	
	public function insertAction()
	{			
		$counter_file=6;
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		
		$this->view->render('photos');
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->category 	  = $params['cid'];
			$this->view->title1 	  = $params['title1'];
			$this->view->title2 	  = $params['title2'];
			$this->view->title3 	  = $params['title3'];
			$this->view->title4 	  = $params['title4'];
			$this->view->title5 	  = $params['title5'];
			$this->view->title6 	  = $params['title6'];
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);				
			return;
		}

		$filter = new AdvGalleryFilter();
		$filter->filter();	
		$data = $filter->getData();
		
		
		$this->view->categoriesList = $this->_getCategoriesList($data['cid']);
		$model = new AdvGallery();		
		for($i = 1; $i <= $counter_file; $i++)
		{
				
			
			if ($_FILES['file'.$i]['name'] != '' &&  $_FILES['file'.$i]['tmp_name'] != '')	
			{
				//zapisz do tabeli photos
				$data2['cid'] = $data['cid'];
				$id=$model->insert($data2);
				
				//zapisz zdjecie na dysku i do upload 
				$title=$data['title'.$i];
				$obj->setTitle($title);
				//if( count($photos)>0 )
				{						
						$obj->add('file'.$i);
						$obj->insert($id);
				}
				$success=true;
			}
			
		}
		if($success)
		{
			header('refresh: 3; url=admin,advgallery,photos.html');
			$this->view->message = $msgWordString.' dodane pomyślnie! Przekierowywanie...';
		}
		else
			$this->_request->redirectFailure(array($msgWordString.' nie zostalo dodane.' ));
			
		
			
	} 
	
	/*public function redirectAction()
	{
		header('Location: admin,advgallery.html');
	}
	
	public function redirectsettAction()
	{
		header('Location: admin,advgallery,settings.html');
	}*/	
	
	public function editAction() 
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		
		$model = new AdvGallery();
		$rows = $model->getAll('p.id='.$this->_id);
		$row = $rows[0];
		$this->view->row = $row;
		$this->view->id 	  = $row['id'];
		$this->view->filename 	  = $row['filename'];
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		$this->view->title 	  = $row['title'];
		
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->title 	  = $params['title'];
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);
			return;
		}
		
		if ($this->_request->isPost()) 
		{
			$filter = new AdvGalleryEditFilter();
			$filter->filter();
			$data=$filter->getData();
			if( !isset($this->_id) || ($this->_id == 0) || ($this->_id=='') ) 
				$this->_id=$data['oldCid'];
			//tabela photos
			$data2['cid']=$data['cid'];
			$model->update($this->_id,$data2 );
			
			//tabela upload
			$title=$data['title'];
			$obj->setTitle($title);
				
			$obj->add('file');
			//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
			$idFile=$row['idFile'];
			$obj->update($this->_id,$idFile);
			
			//echo 'ID='.$this->_id;
			//print_r($filter->getData());
			header('refresh: 3; url=admin,advgallery.html');
			$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';	
		}
	}	
	
	public function deleteAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		
		$model = new AdvGallery();
		$rows = $model->getAll('p.id='.$this->_id);
		$row = $rows[0];
			
		//usun z photos
		$model->delete($this->_id);
		
		//usun z upload
		$idFile=$row['idFile'];
		$obj->deleteOne($idFile);
		
		header('refresh: 3; url=admin,advgallery.html');
		$this->view->message = 'Wpis usunięty pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														CATEGORIES
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	public function poscategoriesAction()
	{
		$this->view->render('index');
		$model = new AdvGalleryTree();
		$rows=$model->getAllChildren($this->_cid);	
		
		foreach ($rows as $r)
		{
			$data['pos'] = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $data);	
		}
		header('refresh: 3; url=admin,advgallery,index,cid_'.$this->_cid.'.html');
		$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';
	}	
	
	
	public function addcatAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->name = null;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			
			$cid=$this->view->cid 	  = $params['cid'];
			$this->view->name   = $params['name'];	
			$this->view->currentPath = $this->_getBranchPatch($cid);
			$this->view->categoriesList = $this->_getCategoriesList($cid);		
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvGalleryCategoryFilter();
			$filter->filter();			
			$model = new AdvGalleryTree();
			$data = $filter->getData();
			
			//$parent = $this->_categoriesModel->get($data['pid']);
			$parent = $model->get($data['cid']);
			$data['parent_id'] = $data['cid'];
			unset($data['cid']);
					
			$model->insertTree($data);	
			header('refresh: 3; url=admin,advgallery,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new AdvGalleryTree();
		$row = $model->get($this->_id);
		//print_r($row);
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		//$this->view->description   = $row['description'];	
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];	
			//$this->view->description   = $params['description'];
					
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvGalleryEditCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			//$this->_cid=$data['cid'];
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,advgallery,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';		
		}
		
	}
	
	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) 
		{
			$filter = new AdvGalleryDeleteCategoryFilter();
			$filter->filter();	
			$data = $filter->getData();
			
			$act=$data['act'];
			$cat=$data['cat'];
			$class=array('AdvGallery');	//Tabela z nazwami modeli do tabel zaiazanych z drzewem z ktorych trzeba usunac np. dokumenty, zdjecia itp
			if ($act == 'delete') {
							
				$model = new AdvGalleryTree();
				$model->deleteTree($this->_id,$class);	
				header('refresh: 3; url=admin,advgallery,index.html');
				$this->view->message = 'Kategoria oraz zdjęcia usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');		
			} 
			
			else if ($act == 'przenies_doc')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść zdjęcia.'));	
				$model = new AdvGalleryTree();
				$model->deleteTree($this->_id,$class,$cat,false);
					header('refresh: 3; url=admin,advgallery,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Zdjęcia przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			} 
			
			else if ($act == 'przenies_cat')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść dokumenty i podkategorie.'));	
				$model = new AdvGalleryTree();
				$model->moveAllBranchTree($this->_id,$cat);
				//$model = new AdvGalleryTree();
				$model->deleteTree($this->_id,$class,$cat,false);	
				header('refresh: 3; url=admin,advgallery,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Zdjęcia przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			}	
		}
	}	
	
	public function deactivecatAction()
	{
		$model = new AdvGalleryTree();
		$model->deactiveSubTree($this->_id);
		header('refresh: 3; url=admin,advgallery,index.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}
		
	public function activecatAction()
	{
		$model = new AdvGalleryTree();
		$model->activeSubTree($this->_id);
		header('refresh: 3; url=admin,advgallery,index.html');
		$this->view->message = 'Kategoria włączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														CONFIG
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function deactiveArticlesAction()
	{
		$this->_config->update('articles', 0, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeArticlesAction()
	{
		$this->_config->update('articles', 1, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function deactiveNewsAction()
	{
		$this->_config->update('advnews', 0, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeNewsAction()
	{
		$this->_config->update('advnews', 1, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function deactiveCatsAction()
	{
		$this->_config->update('advnews_categories', 0, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function activeCatsAction()
	{
		$this->_config->update('advnews_categories', 1, 'advgallery');
		
		header('refresh: 3; url=admin,advgallery,settings.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('settings');
	}
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('advgallery'); 
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
			//echo $s['k'].' '.$s['v'];
		}
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {		
			$pp = (int)clear($_POST['per_page']);
			$pn = (int)clear($_POST['per_news']);
			$pa = (int)clear($_POST['per_articles']);
			$pc = (int)clear($_POST['per_categories']);
			
			if ($pp == '') $this->_request->redirectFailure(array('Pole "Galeria - Zdjęć na stronę" jest wymagane.'));
			if ($pn == '') $this->_request->redirectFailure(array('Pole "Aktualności - Zdjęć na stronę" jest wymagane.'));
			if ($pa == '') $this->_request->redirectFailure(array('Pole "Podstrony - Zdjęć na stronę" jest wymagane.'));
			if ($pc == '') $this->_request->redirectFailure(array('Pole "Kategorie akutalności - Zdjęć na stronę" jest wymagane.'));
								
			$this->_config->update('per_page', $pp, 'advgallery');
			$this->_config->update('per_news', $pn, 'advgallery');
			$this->_config->update('per_articles', $pa, 'advgallery');
			$this->_config->update('per_categories', $pc, 'advgallery');

			header('refresh: 3; url=admin,advgallery,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie! Przekierowywanie...';
		}			
	}
	
	private function displayJsTree2($current)
	{
		$conts = "<a href=\"javascript: d.openAll();\" class=\"close\">+ rozwiń drzewko</a><a href=\"javascript: d.closeAll();\" class=\"close\">- zwiń drzewko</a><br clear=\"all\" /><br />
			<div class=\"dtree\">
			<noscript>
				Twoja przegladarka nie obsługuje javascript!
			</noscript>
			<script type=\"text/javascript\">
		<!--

		d = new dTree('d');";
		
		$model = new AdvGalleryTree();
		$rows=$model->getAllTreeWSTCC(null,'t.pos,t.name');
		
		
		if(count($rows) > 0)
		{
			
			//$was = false;
			foreach($rows as $row)
			{
				if( !isSet($row['count_child']) )
					$row['count_child']=0;
					
				$link='admin,advgallery,index,cid_'.$row['id'].'.html';
				require 'templates/admin/tree/show_tree.php';
		}
							
 
		$conts .= '
			d.openTo('.$current.', true);
			document.write(d);
		
		//-->
		</script>
		</div>';
				
		} else $conts = '<p>brak kategorii</p>';
		
		return $conts;	
	}
}

?>
