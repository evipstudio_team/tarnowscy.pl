<?php 

class Admin_ArticlesController extends Cube_Controller_Abstract
{
	private $_act = null;
	private $_id = null;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','id');
		$title = new Cube_SearchOrder_Field('title','title');
		$author = new Cube_SearchOrder_Field('author','author');
		$date = new Cube_SearchOrder_Field('date','add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		$search->addField($author);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,articles';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		$order->addField($author);
		$order->addField($date);
		
		
		//echo $order->getLink('id');
		
	}
	
	public function sortAction()
	{
		$model = new Articles();
		$this->view->render('index');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		$this->view->rows =$model->getAll($where,$field.' '.$order);
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new Articles();
		$this->view->render('index');
		$where=$this->_search->createWhere($search);
		
		$this->view->rows = $model->getAll($where,'id DESC');
			
		if (sizeof($this->view->rows) < 1)
		{
			header('refresh: 3; url=admin,articles,index.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														INIT
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->_act = $this->_request->getParam('act');	
		$this->_id = $this->_request->getParam('id');	
		$this->view->id = $this->_id;
		$this->search_order();	

		$this->_photo = new Cube_Upload_JPG('articles',$this->_request);
		$this->_pdf = new Cube_Upload_PDF('articles',$this->_request);
		$this->_doc = new Cube_Upload_Document('articles',$this->_request);	
		   
	}

	public function indexAction()
	{	
		$model = new Articles();
		$this->view->rows = $model->getAll(null, 'id DESC');	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														ARTICLES
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	
	public function insertAction()
	{		
		$this->view->row['author'] = $this->_session->getUsername();
		$this->view->categoriesList = $this->_getCategoriesList($params['gallery_cid']);
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['gallery_cid']);		
			return;
		}
		if ($this->_request->isPost()) 
		{
			$filter = new ArticlesFilter();
			$filter->filter();			
			$model = new Articles();
			$id = $model->insert($filter->getData());	
			header('refresh: 3; url=admin,articles,edit,id_'.$id.'.html');	
			$this->view->message = 'Podstrona/artykuł dodana pomyślnie! Przekierowywanie...';
		}
	}
	
	public function editAction()
	{
		$model= new Upload();
		$this->view->photos=$model->getAll('module="articles" AND category="photo" AND id_element='.$this->_id,'pos,id');
		$this->view->pdf=$model->getAll('module="articles" AND category="pdf" AND id_element='.$this->_id,'pos,id');
		$this->view->doc=$model->getAll('module="articles" AND category="document" AND id_element='.$this->_id,'pos,id');
		
		
		$model = new Articles();
		$row = $model->get($this->_id);
		$this->view->row = $row;
		$this->view->categoriesList = $this->_getCategoriesList($row['gallery_cid']);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];
			$this->view->categoriesList = $this->_getCategoriesList($row['gallery_cid']);
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new ArticlesFilter();
			$filter->filter();
			//print_r($filter->getData());
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,articles.html');
			$this->view->message = 'Podstrona zaktualizowana pomyślnie! Przekierowywanie...';	
		}
	}
	
	public function deleteAllAction()
	{
		$this->view->render('edit');
		$model = new Articles();
		$rows = $model->getAll(null, 'id DESC');
		foreach ($rows as $r)
		{
			$checked = (int)clear($_POST['delete_'.$r['id']]);
			if ($checked == '1') 
			{
				$model->delete($r['id']);
				$this->_photo->deleteAll($r['id']);
				$this->_pdf->deleteAll($r['id']);
				$this->_doc->deleteAll($r['id']);	
			}
				
		}
		$this->view->message = 'Aktualności usunięte pomyślnie! Przekierowywanie...';
		header('refresh: 3; url=admin,articles,index.html');
	}
		
	public function deleteAction()
	{
		$model = new Articles();
		$model->delete($this->_id);
		$this->_photo->deleteAll($this->_id);
		$this->_pdf->deleteAll($this->_id);
		$this->_doc->deleteAll($this->_id);
		header('refresh: 3; url=admin,articles.html');
		$this->view->message = 'Podstrona usunięta pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										FILE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function setmainPhotoAction()
	{
		$idFile = $this->_request->getParam('idFile', 0);
		if( $idFile > 0)
		{
			$model= new Upload();
			$model->setmain($idFile,$this->_id,'articles');
			header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}
	}
	
	public function docPosdeleteMarkAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	public function pdfPosdeleteMarkAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
		
	
	public function photoPosdeleteMarkAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->filePosdeleteMark($obj,$msgWordString);
		
	}
	
	private function filePosdeleteMark($obj,$msgWordString)
	{
		if ($this->_request->isPost())
		{  
			$category=$obj->getCategory();
			
			$files=array();
			$model= new Upload();
			$files=$model->getAll('module="articles" AND category="'.$category.'" AND id_element='.$this->_id,'pos,id');
			if(isset($_POST['pos']))
			{
				
				foreach ($files as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) 
					{
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePos($r['id'], $v);	
				}
				
				
				header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
				$this->view->render('index');	
			}

			if(isset($_POST['deleteMark']))
			{
				
				foreach ($files as $r)
				{
					$checked = (int)clear($_POST['delete_'.$r['id']]);
					if ($checked == '1') 
					{
						$obj->deleteOne($r['id']);	
					}
					
				}
				
				header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');					
			
			}
			
		}
	}
	
	public function deleteDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->deleteFile($obj,$msgWordString);
	}
	
	
	public function deletePDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	
	public function deletePhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	private function deleteFile(Cube_Upload_File $obj,$msgWordString)
	{
		//ECHO 'EDITPHOTO';
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$obj->deleteOne($idFile);
						
			header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
			$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}	
	}
	
	public function editDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->editFile($obj,$msgWordString);
	}	
	
	private function editFile(Cube_Upload_File $obj,$msgWordString)
	{
	//ECHO 'EDITPHOTO';
		if ($this->_request->isRedirected()) 
					$this->view->errors = $this->_request->getMessagesFromLastRequest();
		
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$files=array();
			$model= new Upload();
			$this->view->files=$files=$model->getAll('id='.$idFile,'id');
			if ($this->_request->isPost()) 
			{
				
				$title=clear($_POST['title']);
				$idFile=$files[0]['id'];
				if ($_FILES['file']['name'] != '' &&  $_FILES['file']['tmp_name'] != '')
				{	
					//jezeli zmienione zostalo zdjecie
					$obj->setTitle($title);
					$obj->add('file');
					//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
					$obj->update($this->_id,$idFile);
				}
				else if ($title != '')
				{
					//jezeli zmieniony zostal tytul
					$data['title']=$title;
					$model=new Upload();
					$model->update($idFile,$data);
				
				}
					header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
					$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
					$this->view->render('index');	
				
			}
		}
	
	}
	
	
	public function addDocAction()
	{
		$counter_file=2;
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	public function addPDFAction()
	{
		$counter_file=2;
		$obj=$this->_pdf;
		$msgWordString='PDF';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	
	public function addPhotoAction()
	{
		$counter_file=4;
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	private function addFile($counter_file,Cube_Upload_File $obj,$msgWordString)
	{
		$success = false;
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
			$this->view->render('edit');
			header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
			return;	
		}	
		
		for($i = 1; $i <= $counter_file; $i++)
		{
			if ($_FILES['file'.$i]['name'] != '' &&  $_FILES['file'.$i]['tmp_name'] != '')	
			{
				$title=clear($_POST['title'.$i]);
				$obj->setTitle($title);
				//if( count($photos)>0 )
				{						
						$obj->add('file'.$i);
						$obj->insert($this->_id);
				}
				$success=true;
			}
		}
		
		header('refresh: 3; url=admin,articles,edit,id_'.$this->_id.'.html');
		if($success)
			$this->view->message = $msgWordString.' dodane pomyślnie! Przekierowywanie...';
		else
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
		$this->view->render('index');	
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														PRIVATE
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
	
	
	private function _getCategoriesList($cid = null, $no = false)
	{
		$model = new AdvGalleryTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].'/'.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].'/';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}	
}

?>
