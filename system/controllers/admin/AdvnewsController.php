<?php 

class Admin_AdvnewsController extends Cube_Controller_Abstract
{
	private $_id;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	private $_photo=null;
	private $_doc=null;
	private $_pdf=null;
	
	private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','n.id');
		$title = new Cube_SearchOrder_Field('title','n.title');
		$author = new Cube_SearchOrder_Field('author','n.author');
		$date = new Cube_SearchOrder_Field('date','n.add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		$search->addField($author);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,advnews';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		$order->addField($author);
		$order->addField($date);
		
		
		//echo $order->getLink('id');
		
	}
	
	public function sortAction()
	{
		$model = new AdvNews();
		$this->view->render('index');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		//$this->view->categoriesList = $this->_getCategoriesList($cid);
		$this->view->rows = $this->_getSubcategories($cid);		
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		
		if ($cid > 0) 
		{
			$this->view->news = $model->getAll('c.cid = "'.$cid.'" AND '.$where,$field.' '.$order);		
		} else 	$this->view->news = $model->getAllNews($where,$field.' '.$order);
		
		/*if ($cid > 0) {
$this->view->rows = $model->getAllNewsFromCat('c.cid = "'.$cid.'" AND '.$where,$field.' '.$order);		
		} 
		else 
			$this->view->rows =$model->getAllNews($where,$field.' '.$order);
			//$this->view->rows =$model->getAllNews($where,$field.' '.$order);
		*/
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new AdvNews();
		$this->view->render('index');
		$where=$this->_search->createWhere($search);
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		//$this->view->categoriesList = $this->_getCategoriesList($cid);
		$this->view->rows = $this->_getSubcategories($cid);		
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		
		if ($cid > 0) 
		{
			$this->view->news = $model->getAll('c.cid = "'.$cid.'" AND '.$where, 'n.id DESC');		
		} else 	$this->view->news = $model->getAllNews($where,'n.id DESC');
		
		
		
		/*if ($cid > 0) {
			$this->view->rows = $model->getAllNewsFromCat('c.cid = "'.$cid.'" AND '.$where, 'n.id DESC');		
		} 
		else 
			$this->view->rows = $model->getAllNews($where,'n.id DESC');
			//$this->view->rows = $model->getAllNews($where,'id DESC');
		*/	
		if (sizeof($this->view->news) < 1)
		{
			header('refresh: 3; url=admin,advnews,index.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														INIT
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public function init()
	{
		$this->view->setTemplate('admin');		
		$this->view->id = $this->_id = $this->_request->getParam('id', 0);
		//$this->_cid = $this->_request->getParam('cid', 0);
		 $this->view->cid=$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->jsTree = $this->displayJsTree2($this->_cid);
		//$this->view->cid = $this->_cid;		
		$this->_categoriesModel = new AdvGalleryTree();
		$this->search_order();
		
		//Cube_Loader::loadClass('Cube_Upload_Photo');
		$this->_photo = new Cube_Upload_JPG('advnews',$this->_request);
		$this->_pdf = new Cube_Upload_PDF('advnews',$this->_request);
		$this->_doc = new Cube_Upload_Document('advnews',$this->_request);
	}

	
	
	public function indexAction()
	{
		//$this->view->rows = $this->_getCategories();
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->cid=$cid;
		//echo 'CID='.$cid;
		//$this->view->jsTree = $this->displayJsTree2($cid);
		$this->view->rows = $this->_getSubcategories($cid);		
		//$cats =  $this->_getCategories();
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		//$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		//wyswietl newsy
		$model = new AdvNews();
		if ($cid > 0) 
		{
			$this->view->news = $model->getAll('c.cid = "'.$cid.'"', 'c.pos,n.id DESC');		
		} else 	$this->view->news = $model->getAllNews($where,'n.pos_all,n.id DESC');
		
		//print_r($this->view->news);
		
		//$tablica = array ("kolor" => array("niebieski", "czerwony", "zielony"),
        //          "rozmiar" => array("mały", "średni", "duży"));
        //print_r(array_keys ($this->view->news,));
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												CATEGORIES
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	public function poscategoriesAction()
	{
		$this->view->render('index');
		$model = new AdvNewsTree();
		$rows=$model->getAllChildren($this->_cid);	
		
		foreach ($rows as $r)
		{
			$data['pos'] = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $data);	
		}
		header('refresh: 3; url=admin,advnews,index,cid_'.$this->_cid.'.html');
		$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';
	}	
	
	
	public function addcatAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->name = null;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		//$this->view->render('index');		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->name   = $params['name'];		
			$cid= $this->view->cid   = $params['cid'];
			$this->view->categoriesList = $this->_getCategoriesList($cid);
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvNewsCategoryFilter();
			$filter->filter();			
			$model = new AdvNewsTree();
			$data = $filter->getData();
			$data['parent_id'] = $data['cid'];
			unset($data['cid']);
			$model->insertTree($data);			
			header('refresh: 3; url=admin,advnews,index.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new AdvNewsTree();
		$row = $model->get($this->_id);
		
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		
		//$model = new AdvGalleryBinding();
		//$this->view->binded = $model->getForItem($this->_id, 'advnews_categories');
		
		/*$segment = $this->_config->segment('advgallery'); 		
		foreach ($segment as $s){
			$this->view->row[$s['k']] = $s['v'];
		}*/
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];		
			return;
		}		
		if ($this->_request->isPost()) 
		{
			$filter = new AdvNewsEditCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,advnews,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';	
		}
	}

	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) {
			$filter = new AdvNewsDeleteCategoryFilter();
			$filter->filter();	
			$data = $filter->getData();
			$act=$data['act'];
			$cat=$data['cat'];
			$class=array('AdvNews');	//Tabela z nazwami modeli do tabel zaiazanych z drzewem z ktorych trzeba usunac np. dokumenty, zdjecia itp
			if ($act == 'delete') 
			{
				/*$model = new AdvNews();
				$model->deleteCategory($this->_id);
				$model->deleteAllNews($this->_id);*/
				$model = new AdvNewsTree();
				$model->deleteTree($this->_id,$class);	
				header('refresh: 3; url=admin,advnews,index.html');
				$this->view->message = 'Kategoria oraz newsy usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');		
			} 
			else if ($act == 'przenies_doc')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść dokumenty.'));	
				$model = new AdvNewsTree();
				$model->deleteTree($this->_id,$class,$cat,false);
				
				header('refresh: 3; url=admin,advnews,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Newsy przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			} 
			
			else if ($act == 'przenies_cat')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść dokumenty i podkategorie.'));	
				$model = new AdvNewsTree();
				$model->moveAllBranchTree($this->_id,$cat);
				$model->deleteTree($this->_id,$class,$cat,false);
				/*$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść zdjęcia.'));	
				$model = new AdvNews();
				$model->deleteCategory($this->_id);
				$model->setCategories($this->_id, $cat);*/
				header('refresh: 3; url=admin,advnews,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Newsy przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			}	
		}
	}	
	
	public function deactive2Action()
	{
		$this->_deactive('news');
	}
	
	public function deactiveAction()
	{
		$this->_deactive('index');
	}
		
	private function _deactive()
	{
		$model = new AdvNews();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,advnews,'.$header.'.html');
		$this->view->message = 'Aktualność wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}	
		
		
	public function active2Action()
	{
		$this->_active('news');
	}	
	
	public function activeAction()
	{
		$this->_active('index');
	}	
	
	private function _active($header)
	{
		$model = new AdvNews();
		$model->active($this->_id);
		header('refresh: 3; url=admin,advnews,'.$header.'.html');
		$this->view->message = 'Aktualność włączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}
	
	
	public function deactivecatAction()
	{
		$model = new AdvNewsTree();
		$model->deactiveSubTree($this->_id);
		header('refresh: 3; url=admin,advnews,index.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}
		
	public function activecatAction()
	{
		$model = new AdvNewsTree();
		$model->activeSubTree($this->_id);
		header('refresh: 3; url=admin,advnews,index.html');
		$this->view->message = 'Kategoria włączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}	
	# END CATEGORIES	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											AKTUALNOŚCI
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function insertAction()
	{		
		$this->view->row['author'] = $this->_session->getUsername();
		$this->view->row['add_date'] = now(true);
		$this->view->cats = $this->_getCategories();
		
		//print("Wynik  funkcji cats:<BR><pre>");
		//print_r($this->view->cats);
		//print("</pre><BR>");
		
		$this->view->categoriesList = $this->_getCategoriesList2($row['gallery_cid']);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];	
			$this->view->categoriesList = $this->_getCategoriesList2($params['gallery_cid']);		
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvNewsFilter();
			$filter->filter();	
			$data = $filter->getData();
			$cat_values = (array)$_POST['cat_value'];
			$cat_ids  = (array)$_POST['cat_id'];
			
			/*print("Wynik  funkcji cat_values:<BR><pre>");
			print_r($cat_values);
			print("</pre><BR>");
			
			print("Wynik  funkcji cats_ids:<BR><pre>");
			print_r($cat_ids);
			print("</pre><BR>");*/
			
			$test = 0;
			foreach ($cat_values as $k => $v) 
			{
				$v = explode('_',$v);
				if ($v[0] == 1) 
					$test++;
			}
			
			if ($test == 0)
				$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
				
			$model = new AdvNews();
			$id = $model->insert($data);
			
			#kategorie
			foreach ($cat_ids as $k => $v) 
			{
				foreach ($cat_values as $b) 
				{
					if ($b == '1_'.$v) 
					{
						$model->appendCat($id, $v);
						break;
					}
				}
			}
			
			# wysyła maila dla newsinfo
			/*if (file_exists('system/models/Newsinfo.php')) {
				$newsinfo = new Newsinfo();
				$newsinfo->send($data);	
			}*/
			
			header('refresh: 3; url=admin,advnews,edit,id_'.$id.'.html');	
			//header('refresh: 3; url=admin,edit.html');
			$this->view->message = 'News dodany pomyślnie! Przekierowywanie...';
		}
	}
	
	public function deleteAction()
	{
		$model = new AdvNews();
		$model->delete($this->_id);
		$this->_photo->deleteAll($this->_id);
		$this->_pdf->deleteAll($this->_id);
		$this->_doc->deleteAll($this->_id);
		header('refresh: 3; url=admin,advnews.html');
		$this->view->message = 'News usunięty pomyślnie! Przekierowywanie...';
		$this->view->render('edit');
	}
	
	
	public function editAction()
	{
		$model= new Upload();
		$this->view->photos=$model->getAll('module="advnews" AND category="photo" AND id_element='.$this->_id,'pos,id');
		$this->view->pdf=$model->getAll('module="advnews" AND category="pdf" AND id_element='.$this->_id,'pos,id');
		$this->view->doc=$model->getAll('module="advnews" AND category="document" AND id_element='.$this->_id,'pos,id');
		
		$model = new AdvNews();
		$this->view->row = $row = $model->get($this->_id);
		$this->view->cats = $this->_getCategories();
		$this->view->categoriesList = $this->_getCategoriesList2($row['gallery_cid']);
		
		
		/*$model = new AdvGalleryBinding();
		//$this->view->binded = $model->getForItem($this->_id, 'advnews');
		
		$segment = $this->_config->segment('advgallery'); 		
		foreach ($segment as $s){
			$this->view->row[$s['k']] = $s['v'];
		}*/
		
		$this->view->catsList = $this->_getCatsList($this->_id);
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $params = $this->_request->getParamsFromLastRequest(); 
			$this->view->row['add_date']=convertToTimeStamp($this->view->row['add_date']);
			$this->view->id = $this->view->row['id'];
			$this->view->categoriesList = $this->_getCategoriesList2($params['gallery_cid']);			
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvNewsFilter();
			$filter->filter();
			
			$model = new AdvNews();
			$cat_values = (array)$_POST['cat_value'];
			$cat_ids  = (array)$_POST['cat_id'];
			
			//print("Wynik  funkcji cat_values:<BR><pre>");
			//print_r($cat_values);
			//print("</pre><BR>");
			
			$test = 0;
			foreach ($cat_values as $k => $v) {
				$v = explode('_',$v);
				if ($v[0] == 1) 
					$test++;
			}
			
			$data = $filter->getData();
			if ($test == 0)
				$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
			
			$model->update($this->_id, $data);
			
			#kategorie

			foreach ($cat_ids as $k => $v) {
				$find = false;
				foreach ($cat_values as $b)
				{
					if ($b == '1_'.$v) {
						$model->appendCat($this->_id, $v);
						$find = true;
						break;
					}	
				}
				if (!$find)
					$model->deleteCat($this->_id, $v);	
			}			
			
			header('refresh: 3; url=admin,advnews.html');
			$this->view->message = 'News zaktualizowany pomyślnie! Przekierowywanie...';	
		}
	}
	
	public function posdeleteMarkAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->render('index');
		$model = new AdvNews();
		if(isset($_POST['pos']))
		{
			if( $cid>0 )
			{
				$rows = $model->getAll('c.cid = "'.$cid.'"', 'c.pos,n.id DESC');
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['cat_news_id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosCategory($r['cat_news_id'], $v);	
				}
		
				$this->view->message = 'Pozycje kategorii zaktualizowane pomyślnie! Przekierowywanie...';
				header('refresh: 3; url=admin,advnews,index,cid_'.$cid.'.html');
			}
			else
			{
				$rows =$model->getAllNews(null,'n.pos_all,n.id DESC');
				foreach ($rows as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) {
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePosAdvnews($r['id'], $v);	
				}
			
			$this->view->message = 'Pozycje kategorii zaktualizowane pomyślnie! Przekierowywanie...';
			
			}
		}
		if(isset($_POST['deleteMark']))
		{
			if( $cid>0 )
				$rows = $model->getAll('c.cid = "'.$cid.'"', 'c.pos,n.id DESC');
			else
				$rows =$model->getAllNews(null,'n.pos_all,n.id DESC');
			foreach ($rows as $r)
			{
				$checked = (int)clear($_POST['delete_'.$r['id']]);
				if ($checked == '1') 
				{
					$model->delete($r['id']);
					$this->_photo->deleteAll($r['id']);	
				}
					
			}
			$this->view->message = 'Newsy usunięte pomyślnie! Przekierowywanie...';
		}
		header('refresh: 3; url=admin,advnews,index.html');
		
	}	
	
	
	public function setmainAction()
	{
		$this->view->render('index');
		$model = new AdvNews();
		$model->setmain($this->_id);
		header('refresh: 2; url=admin,advnews,index.html');
		$this->view->message = 'Aktualność miesiąca ustawione pomyślnie! Przekierowywanie...';
	}	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										FILE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function setmainPhotoAction()
	{
		$idFile = $this->_request->getParam('idFile', 0);
		if( $idFile > 0)
		{
			$model= new Upload();
			$model->setmain($idFile,$this->_id,'advnews');
			header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
			$this->view->message = 'Zdjęcie główne ustawione pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}
	}
	
	public function docPosdeleteMarkAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	public function pdfPosdeleteMarkAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->filePosdeleteMark($obj,$msgWordString);
	}
	
	
	
	public function photoPosdeleteMarkAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->filePosdeleteMark($obj,$msgWordString);
		
	}
	
	private function filePosdeleteMark($obj,$msgWordString)
	{
		if ($this->_request->isPost())
		{  
			$category=$obj->getCategory();
			
			$files=array();
			$model= new Upload();
			$files=$model->getAll('module="advnews" AND category="'.$category.'" AND id_element='.$this->_id,'pos,id');
			if(isset($_POST['pos']))
			{
				
				foreach ($files as $r)
				{
					$v = (int)clear($_POST['pos_'.$r['id']]);
					if (!is_numeric($v)) 
					{
						$this->_request->redirectFailure(array('Wszystkie pola "Pozycja" muszą zostać wypełnione! Przekierowywanie...'));
						return;
					}
					$model->updatePos($r['id'], $v);	
				}
				
				
				header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
				$this->view->render('index');	
			}

			if(isset($_POST['deleteMark']))
			{
				
				foreach ($files as $r)
				{
					$checked = (int)clear($_POST['delete_'.$r['id']]);
					if ($checked == '1') 
					{
						$obj->deleteOne($r['id']);	
					}
					
				}
				
				header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
				$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
				$this->view->render('index');					
			
			}
			
		}
	}
	
	public function deleteDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->deleteFile($obj,$msgWordString);
	}
	
	
	public function deletePDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	
	public function deletePhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->deleteFile($obj,$msgWordString);
	
	}
	
	private function deleteFile(Cube_Upload_File $obj,$msgWordString)
	{
		//ECHO 'EDITPHOTO';
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$obj->deleteOne($idFile);
						
			header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
			$this->view->message = $msgWordString.' usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}	
	}
	
	public function editDocAction()
	{
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPDFAction()
	{
		$obj=$this->_pdf;
		$msgWordString='PDF';
		$this->editFile($obj,$msgWordString);
	}		
	
	
	
	public function editPhotoAction()
	{
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		$this->editFile($obj,$msgWordString);
	}	
	
	private function editFile(Cube_Upload_File $obj,$msgWordString)
	{
	//ECHO 'EDITPHOTO';
		if ($this->_request->isRedirected()) 
					$this->view->errors = $this->_request->getMessagesFromLastRequest();
		
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		
		if( $idFile > 0 )
		{
			$files=array();
			$model= new Upload();
			$this->view->files=$files=$model->getAll('id='.$idFile,'id');
			if ($this->_request->isPost()) 
			{
				
				$title=clear($_POST['title']);
				$idFile=$files[0]['id'];
				if ($_FILES['file']['name'] != '' &&  $_FILES['file']['tmp_name'] != '')
				{	
					//jezeli zmienione zostalo zdjecie
					$obj->setTitle($title);
					$obj->add('file');
					//przy wiekszej ilosci zdjec zamiast 0 nr $_FILES
					$obj->update($this->_id,$idFile);
				}
				else if ($title != '')
				{
					//jezeli zmieniony zostal tytul
					$data['title']=$title;
					$model=new Upload();
					$model->update($idFile,$data);
				
				}
					header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
					$this->view->message = $msgWordString.' zaktualizowane pomyślnie! Przekierowywanie...';
					$this->view->render('index');	
				
			}
		}
	
	}
	
	
	public function addDocAction()
	{
		$counter_file=2;
		$obj=$this->_doc;
		$msgWordString='Dokumenty';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	public function addPDFAction()
	{
		$counter_file=2;
		$obj=$this->_pdf;
		$msgWordString='PDF';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	
	
	public function addPhotoAction()
	{
		$counter_file=4;
		$obj=$this->_photo;
		$msgWordString='Zdjęcie';
		
		$this->addFile($counter_file,$obj,$msgWordString);
	}	
	
	private function addFile($counter_file,Cube_Upload_File $obj,$msgWordString)
	{
		$success = false;
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
			$this->view->render('edit');
			header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
			return;	
		}	
		
		for($i = 1; $i <= $counter_file; $i++)
		{
			if ($_FILES['file'.$i]['name'] != '' &&  $_FILES['file'.$i]['tmp_name'] != '')	
			{
				$title=clear($_POST['title'.$i]);
				$obj->setTitle($title);
				//if( count($photos)>0 )
				{						
						$obj->add('file'.$i);
						$obj->insert($this->_id);
				}
				$success=true;
			}
		}
		
		header('refresh: 3; url=admin,advnews,edit,id_'.$this->_id.'.html');
		if($success)
			$this->view->message = $msgWordString.' dodane pomyślnie! Przekierowywanie...';
		else
			$this->view->message = $msgWordString.' nie zostalo dodane! Przekierowywanie...';
		$this->view->render('index');	
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//													SETTINGS
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('advnews'); 
		
		
		//$this->view->row['start_date'] = now(true);
		//$this->view->row['end_date'] = now(true);
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
		}
		
		//if( ($this->view->row['start_date']=='') || (is_null($this->view->row['start_date'])) )
			//$this->view->row['start_date']=convertToTimeStamp(now(true));
		//if( ($this->view->row['end_date']=='') || (is_null($this->view->row['end_date'])) )
		//	$this->view->row['end_date']=convertToTimeStamp(now(true));
			
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $params = $this->_request->getParamsFromLastRequest();
			//$this->view->row['start_date']=convertToTimeStamp($this->view->row['start_date']);
			//$this->view->row['end_date']=convertToTimeStamp($this->view->row['end_date']);
			return;
		}
		if ($this->_request->isPost()) {		
			//$pp = (int)clear($_POST['per_page']);
			$filter = new AdvNewsSettingsFilter();
			$filter->filter();	
			$data = $filter->getData();
			//if ($pp == '') $this->_request->redirectFailure(array('Pole "Newsów na stronę" jest wymagane.'));
			//if ($max == '') $this->_request->redirectFailure(array('Pole "Max znaków komentarza" jest wymagane.'));
			$this->_config->update('per_page', $data['per_page'], 'advnews');
			$this->_config->update('comments', $data['comments'], 'advnews');
			$this->_config->update('max_chars', $data['max_chars'], 'advnews');
			//$start_date = convertToTimeStamp($data['start_date']);
			//$end_date = convertToTimeStamp($data['end_date']);
			//$this->_config->update('start_date', $start_date, 'advnews');
			//$this->_config->update('end_date', $end_date, 'advnews');

			header('refresh: 3; url=admin,advnews,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie! Przekierowywanie...';
		}			
	}
	
	
		
	
	
	
	
	
	# CATEGORIES
	private function _getCategories()
	{
		$model = new AdvNewsTree();
		$rows=$model->getAll(null,'id');
		
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");	
		foreach($rows as $key => $row)
		{
			$rows[$key]['name']=$this->_getBranchPatch($row['id'],true);
			//$rows[$key]['name']='STRING';
			//echo $this->_getBranchPatch($row['id'],true);
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");	
		
		return $rows;
		
		
		//$model = new AdvNews();
		//$cats = $model->getCategories(null, 'name ASC');
		//return $cats;
	}
	
	private function _getCatsList($nid)
	{
		$model = new AdvNews();
		$cats = $model->getCatsList($nid);
		return $cats;
	}	
	
	//TREE
	private function displayJsTree2($current)
	{
		$conts = "<a href=\"javascript: d.openAll();\" class=\"close\">+ rozwiń drzewko</a><a href=\"javascript: d.closeAll();\" class=\"close\">- zwiń drzewko</a><br clear=\"all\" /><br />
			<div class=\"dtree\">
			<noscript>
				Twoja przegladarka nie obsługuje javascript!
			</noscript>
			<script type=\"text/javascript\">
		<!--

		d = new dTree('d');";
		
		$model = new AdvNewsTree();
		$rows=$model->getAllTreeWSTCC(null,'t.pos,t.name');
		
		
		if(count($rows) > 0)
		{
			
			//$was = false;
			foreach($rows as $row)
			{
				if( !isSet($row['count_child']) )
					$row['count_child']=0;
					
				$link='admin,advnews,index,cid_'.$row['id'].'.html';
				require 'templates/admin/tree/show_tree.php';
			}
							
 
			$conts .= '
			d.openTo('.$current.', true);
			document.write(d);
		
			//-->
			</script>
			</div>';
				
		} else $conts = '<p>brak kategorii</p>';
		
		return $conts;	
	}

	private function _getCategoriesList2($cid = null, $no = false)
	{
		$model = new AdvGalleryTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].' &raquo; '.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].' &raquo; ';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}
	
	
	private function _getCategoriesList($cid = null, $no = false)
	{
		$model = new AdvNewsTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].'/'.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].'/';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}

	private function _getSubCategories($parent_id)
	{
		//$cats = $this->_categoriesModel->getSubcategories($parent_id);
		//return $cats;		
		
		$model = new AdvNewsTree();
		$rows=$model->getAllChildren($parent_id);
		
		//ile dokumentow jest w kazdej kategorii
		$model = new AdvNews();
		foreach ($rows as $i=>$row)
		{
			$temp=$model->count_rows('cid='.$row['id']);	
			$rows[$i]['amount']=$temp['amount'];
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		return $rows;		
	}
	
	private function _getBranchPatch($cid,$path_string=null)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new AdvNewsTree();
		$rows=$model->getBranchTree($cid);
		if( is_null($path_string) )
			return $rows;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		$temp='';
		foreach($rows as $row)
			$temp.=$row['name'].'/';
		
		return $temp;
		
	}
	
	
	
}

?>
