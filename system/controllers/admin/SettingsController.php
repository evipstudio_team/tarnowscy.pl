<?php 

class Admin_SettingsController extends Cube_Controller_Abstract
{
	public function init()
	{
		$this->view->setTemplate('admin');						   
	}

	public function indexAction()
	{
		$this->view->systemLanguages = $systemLanguages = Cube_Registry::get('systemLanguages');
		$segment = $this->_config->segment('general');
		$data = array(); 
		foreach ($segment as $s)
		{
			$data[$s['k']] = $s['v'];
			
		}
		$this->view->row = $data;
		$model = new Modules();
		$this->view->allowed_modules = $model->getAllModules('access <> "none" AND (name NOT IN("modules", "admins", "settings"))');
		$username = $this->_session->getUsername();
		$wu = 'wysiwyg_'.$username;
		$this->view->wys = $wu;
								
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {
			if ($this->_session->getRole() == 'administrator') {
				$wysiwyg = clear($_POST['wysiwyg']);
				if (!$this->_config->exists($wu)) 
					$this->_config->insert($wu, $wysiwyg, 'general');
				else $this->_config->update($wu, $wysiwyg, 'general');
			} else {
				$default_module = clear($_POST['default_module']);
				$default_language = clear($_POST['default_language']);
				$wysiwyg = clear($_POST['wysiwyg']);	
					
				if ($_POST['act']) {
					if ($default_module == '') $this->_request->redirectFailure(array('Pole "Domyślny moduł" jest wymagane.'));
					if ($default_language == '') $this->_request->redirectFailure(array('Pole "Domyślny język" jest wymagane.'));					
					$this->_config->update('default_language', $default_language, 'general');
					$this->_config->update('default_module', $default_module, 'general');
				
					if (!$this->_config->exists($wu)) 
						$this->_config->insert($wu, $wysiwyg, 'general');
					else $this->_config->update($wu, $wysiwyg, 'general');
				} else {
					foreach ($systemLanguages as $k => $v)
					{
						if ($_POST['lang'] == $k) {
							if (!$this->_config->exists('title_'.$k)) 
								$this->_config->insert('title_'.$k, clear($_POST['title_'.$k]), 'general');
							else $this->_config->update('title_'.$k, clear($_POST['title_'.$k]), 'general');
							if (!$this->_config->exists('footer_'.$k)) 
								$this->_config->insert('footer_'.$k, clear($_POST['footer_'.$k]), 'general');
							else $this->_config->update('footer_'.$k, clear($_POST['footer_'.$k]), 'general');
							if (!$this->_config->exists('keywords_'.$k)) 
								$this->_config->insert('keywords_'.$k, clear($_POST['keywords_'.$k]), 'general');
							else $this->_config->update('keywords_'.$k, clear($_POST['keywords_'.$k]), 'general');
							if (!$this->_config->exists('description_'.$k)) 
								$this->_config->insert('description_'.$k, clear($_POST['description_'.$k]), 'general');
							else $this->_config->update('description_'.$k, clear($_POST['description_'.$k]), 'general');													
						}
					}
				}
			}
			header('refresh: 3; url=admin,settings,redirect.html');
			$this->view->message = 'Ustawienia zapisane pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function redirectAction()
	{
		header('Location: admin,settings.html');	
	}
						
}

?>
