<?php 

class Admin_ModulesController extends Cube_Controller_Abstract
{
	private $_id; 
	private $_modules;  //spis wszystkich dostepnych modulow jezykowych
	private $_modules_access; //spis wszystich modułow bez modules dostępnych dla wybranego id.
	private $_languages;
	private $_warningsLang=null;	 //komunikaty ostrzezen
	
	public function init()
	{	
		$this->view->id = $this->_id = $this->_request->getParam('id');
		$this->_languages=Cube_Registry::get('language');					
	}

	public function indexAction()
	{ 	
		$model = new Modules();
		//$this->view->rows = $model->getModules('m.name <> "modules"', 'name');
		$this->view->rows = $model->getAllModules('name <> "modules"', 'pos,name');
		//print_r($this->view->rows);
		
								
	}
	
	public function posAction()
	{
		$this->view->render('info');
		$model = new Modules();
		$rows=$model->getAllModules('name <> "modules"', 'pos,name');
		foreach ($rows as $r)
		{
			$var = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $var);	
		}
		header('refresh: 3; url=admin,modules,index.html');
		$this->view->message = 'Pozycje zaktualizowane pomyślnie! Przekierowywanie...';
	}
	
	
	private function _getNotInstalledModules($installed)
	{
		$modules = array('valid' => array(), 'invalid' => array());
		$dir_name = 'system/modules/';	
		$dir_handle = dir($dir_name);
		while (FALSE !== ($fileData = $dir_handle->read()))
		{
			if (is_file($dir_name . $fileData)) {
				if (!array_key_exists(str_replace('.php', '', $fileData), $installed)) 
				{
					include "system/modules/$fileData";			
					// sprawdz czy $fileData = $moduleInfo['name']
					if ($fileData == $moduleInfo['name'].'.php' && $moduleInfo['admin_access'] != '') 
					{
						$modules['valid'][] = $moduleInfo;					
					} 
					else 
						$modules['invalid'][] = $fileData;
					
					unset($moduleInfo);
				}	
			}
		}
		$dir_handle->rewind();	
		$dir_handle->close();
		return $modules;
	}
	
	public function addAction()
	{
		$model = new Modules();
		//$rows = $model->getModules(null, 'name');
		$rows= $model->getAllModules(null,'name');
		$modules = array();
		foreach ($rows as $row)
			$modules[] = $row['name'];
		$this->view->modules = $this->_getNotInstalledModules(array_flip($modules));	
	}	
	
	public function installAction()
	{
		$this->view->render('info');
		$model = new Modules();
		$name = $this->_request->getParam('name');
		if ($name == 'modules' || $name == 'admins' || $name == 'settings') header('Location: admin,modules.html');		
		$result = $model->install($name);
		
		$this->view->error = false;
		if ($result < 1) $this->view->error = true;
		
		switch ($result)
		{
			case 1:  $msg = "Moduł $name zainstalowany pomyślnie !"; break;
			case -1: $msg = "Moduł $name nie został zainstalowany, powód: brak tablicy 'moduleInstall'"; break;
			case -2: $msg = "Moduł $name nie został zainstalowany, powód: nieprawidłowa tablica 'moduleInfo'"; break;
			case -3: $msg = "Moduł $name nie został zainstalowany, powód: brak tablicy 'moduleActions'"; break;
			case -4: $msg = "Moduł $name nie został zainstalowany, powód: nieprawidłowe Controllery modułu"; break;
			case -5: $msg = "Moduł $name nie został zainstalowany, powód: moduł o tej nazwie jest już zainstalowany"; break;
			default: $msg = "Moduł $name nie został zainstalowany, powód: plik system/modules/$name.php nie istnieje"; break;
		}	
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = $msg.' Przekierowywanie...';
	}	

	public function uninstallAction()
	{
		$this->view->render('info');
		$model = new Modules();
		$name = $this->_request->getParam('name');
		if ($name == 'modules' || $name == 'admins' || $name == 'settings') 
			header('Location: admin,modules.html');
		
		//przed usunieciem pobierz id modulu
		$module=$model->getByName($name);
		$id_module=$module['id'];
		$result = $model->uninstall($name);
		
		$this->view->error = false;
		if ($result < 1) $this->view->error = true;
		
		switch ($result)
		{
			case 1:  $msg = "Moduł $name odinstalowany pomyślnie !"; 
				$model = new Modules();
				//usun modul z tabeli admin_access
				$model->unsetAccess(null ,$id_module);
				break;
			case -1: $msg = "Moduł $name nie został odinstalowany, powód: brak tablicy 'moduleUninstall'"; break;
			case -2: $msg = "Moduł $name nie został odinstalowany, powód: moduł nie jest zainstalowany"; break;
			default: $msg = "Moduł $name nie został odinstalowany, powód: plik system/modules/$name.php nie istnieje"; break;
		}	
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = $msg.' Przekierowywanie...';
	}
	
	public function activeAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');
		$model = new Modules();
		$model->active($this->_id);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł aktywowany pomyślnie! Przekierowywanie...';
		$this->view->render('info');
	}
	
	public function deactiveAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');	
		$model = new Modules();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł deaktywowany pomyślnie! Przekierowywanie...';
		$this->view->render('info');
	}	

	public function accessAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');
		$model = new Modules();
		$access = clear($_POST['access']);
		if ($access == 'root' || $access == 'administrator' || $access == 'none')
			$model->setAccess($this->_id, $access);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł zaktualizowany pomyślnie! Przekierowywanie...';
		$this->view->render('info');		
	} 		
	
	
	//LANG
	public function activelangAction()
	{
		$model = new Langs();
		$model->active($this->_id);
		header('refresh: 3; url=admin,modules,addlang.html');
		$this->view->message = 'Język aktywowany pomyślnie! Przekierowywanie...';
		$this->view->render('addlang');
	}
	
	public function deactivelangAction()
	{
		
		$model = new Langs();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,modules,addlang.html');
		$this->view->message = 'Język deaktywowany pomyślnie! Przekierowywanie...';
		$this->view->render('addlang');
	}	
	
	public function editlangAction()
	{	
		$model = new Langs();
		$this->view->row = $model->get($this->_id);
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest();
			
			 
		}
		if ($this->_request->isPost()) 
		{		
			$filter = new LangsEditFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Langs();
			$this->checkLang($data,$this->_id);
			$model->update($this->_id,$data);
			$this->view->render('addlang');
			header('refresh: 3; url=admin,modules,addlang.html');
			$this->view->message = 'Język zmieniony pomyślnie! Przekierowywanie...';
			
		}
		
	}
	
	
	public function addlangAction()
	{
		$this->view->Languages =$Languages= Cube_Registry::get('selectLanguages');

		$model = new Langs();
		$this->view->rows = $model->getAll(null,'id');	
	
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			//$this->view->id = $this->view->row['id'];	
			//$this->view->name = $this->view->row['name'];
			//$this->view->short_name = $this->view->row['short_name'];
			//$this->view->active = $this->view->row['active'];
			return;
		}
		if($this->_request->isGet() && ($this->_id !='' && is_numeric($this->_id)))
		{
			//echo 'GET';
			//pobierz wybrany jezyk
			//echo 'ID='.$this->_id;
			$this->view->row['name']=$Languages[$this->_id - 1]['name'];
			$this->view->row['short_name']=$Languages[$this->_id - 1]['short_name'];
			//$this->view->render('add');	
		}
		if ($this->_request->isPost()) {		
			$filter = new LangsFilter();
			$filter->filter();			
			$data = $filter->getData();
			$this->checkLang($data);
			$model = new Langs();
			$model->insert($data);
				
			header('refresh: 3; url=admin,modules,addlang.html');
			$this->view->message = 'Nowy język dodany pomyślnie! Przekierowywanie...';
			
		}
	}	
	
	private function checkModulesLang($InstallModules,$ModulesFromFile)
	{
		//sprawdza czy czy kazdy moduł ma wpis w pliku modules
		$Modules=array();
		$WarningsLang=$this->_warningsLang;
		//print_r($ModulesFromFile);
		foreach ($InstallModules as $key=> $InsMod)
		{
			//echo 'mod='.($InsMod['name']);
			if( is_numeric(array_search($InsMod['name'],$ModulesFromFile)) )
			{
				$Modules[]=$InsMod;
				
			}
			else
				$WarningsLang[]='W module '.$InsMod['name'].' nie znaleziono poprawnego wpisu w pliku /modules/'.$InsMod['name'].'.php';
		}
		$this->_warningsLang=$this->view->warningsLang=$WarningsLang;
		return $Modules;
	}
	
	public function addlang2modulesAction()
	{
		
		//$ModulesNotLang= $this->_languages->getModulesNotLang();
		
		
		//pobierz zainstalowane moduły
		$installModules=$this->_languages->getInstallModules();
		//print_r ($installModules);
		
		//pobierz moduly ktore maja poprawny wpis w pliku modules
		$modulesFromFiles=$this->_languages->getModulesLang();
		//print_r($modulesFromFiles);
		//sprawdz czy wszystkie zainstalowane moduly maja wpis w katalogu modules
		$this->_modules=$this->view->modules =$this->checkModulesLang($installModules,$modulesFromFiles);
		//print_r($this->view->modules);
		
		$model = new Langs();
		$this->view->langs =$this->_langs = $model->getAll(null,'id');
		//print_r($this->view->langs);
		$this->view->idLang=$this->_idLang = $this->_request->getParam('idlang');
		if ( ($this->_idLang =='') || (!isSet($this->_idLang)) )
				$this->view->idLang=$this->_idLang=$this->_langs[0]['id'];
		
		//$this->_modules_access = $this->view->modules_access=$this->_languages->getModulesAccess($this->_idLang);
		
		//pobierz info o uprawnieniach jezyka w modułach
		$where_not_lang=$this->_languages->whereNotLangs('m');
		$where=$where_not_lang.' AND l.id='.$this->_idLang;
		$this->_modules_access = $this->view->modules_access = $model->getModules($where, 'm.name');
		
		//$this->view->warningsLang[]='KONIEC';
		
		//echo 'ID_LANG='.$this->_idLang;
				
		
		//if($this->_request->isGet() && ($this->_idLang !='' && is_numeric($this->_idLang)))
		//{
			//echo 'GET';
			//pobierz wybrany jezyk
			//echo 'ID='.$this->_id;
			//$this->view->row['name']=$Languages[$this->_id - 1]['name'];
			//$this->view->row['short_name']=$Languages[$this->_id - 1]['short_name'];
			//$this->view->render('add');	
		//}
		if ($this->_request->isPost()) 
		{	
			foreach($this->_langs as $key => $r)
			{
				if ($r['id']==$this->_idLang)
				{
					$short_name_lang=$this->_langs[$key]['short_name'];
					break;
				}
			}
			
			foreach($this->_modules as $row) 
			{
				$id=$row['id'];
				$access = ($_POST['access'.$id]); //nowy wybor usera
				$status=0; //user nie mial uprawnien do modulu
				foreach($this->_modules_access as $module_access)
				{
					if($id==$module_access['id'])
					{
						//user mial uprawnienia do modulu
						$status=1; 
						break;
					}
				}
				if($status==0)	
				{
				//echo 'Access='.$access;
					if ($access == '1')
					{
					//jesli zero utaw none
					//echo 'STATUS='.$status.',ID='.$id.'Zmiana accessu. Aktualny access to'.$access;
						$model = new Langs();
						$model->setAccess($this->_idLang ,$id);
						
						$tables_field=$this->_languages->getTablesAndFieldLang($row['name']);
						$this->setAccessLang($tables_field ,$short_name_lang,$row['name']);
						
					}
				}
				else
				{
					//jezeli takie same
					if (!(isset($access)))
					{
						//jesli zero utaw $this->_role;
						//echo 'STATUS='.$status.',ID='.$id.'Zmiana accessu. Aktualny access to'.$access;
						$model = new Langs();
						$model->unsetAccess($this->_idLang ,$id);
						
						$tables_field=$this->_languages->getTablesAndFieldLang($row['name']);
						$this->unsetAccessLang($tables_field ,$short_name_lang,$row['name']);
					}
				}
				if (!isset($this->view->errors))
				{
					header('refresh: 3; url=admin,modules,addlang2modules,idlang_'.$this->_idLang.'.html');
					$this->view->message= 'Uprawnienia zmienione pomyślnie! Przekierowywanie...';
				}
				else
				{
					header('refresh: 6; url=admin,modules,addlang2modules,idlang_'.$this->_idLang.'.html');
					$this->view->message= 'Pojawiły się BŁĘDY! Przekierowywanie za 6 sekund ...';
					
				}
				//$this->view->render('add');
			}
		}	//end if_request->isPost		
		
	}	//end addlang2modulesAction
	
	private function setAccessLang($tables_field ,$short_name_lang,$module)
	{
		$this->AccessLang(1,$tables_field ,$short_name_lang,$module);
	}
	
	private function unsetAccessLang($tables_field ,$short_name_lang,$module)
	{
		$this->AccessLang(0,$tables_field ,$short_name_lang,$module);
	}
	
	private function AccessLang($set,$tables_field ,$short_name_lang,$module)
	{
		$model = new Langs();
		
		if( count($tables_field) )
		{
			//Czy sa jakies tabele
			$tables=array();
			foreach($tables_field as $mod)
			{
				$tables=$mod['table'];
				$field=$mod['field'];
				//print("Wynik rows:<BR><pre>");
				//print_r($model->describe($tables));
				//print("<BR><pre>") ;
				if( $all_field=$model->describe($tables) )
				{
					//czy tablica jest w bazie danych
					if( count($field) )
					{
					//czy tabela ma jakies pola
						foreach($field as $f)
						{
							//dla kazdego pola
							if($set==1)
							{	
								//dodaj kolumne	
								$exist=false; //czy pole jezykowe juz istnieje w bazie danych
								$index=null;  //czy pole wpisane do pliku modules istnieje w bazie danych. JEZELI index=null to $f nie ma w bazie danych
								foreach($all_field as $key=>$a_field)
								{
									//dla kazdego pola w tabeli 
									//print_r($f);
									if(! strcmp($f,$a_field['Field']))
									{
										$index=$key;
										//echo 'INDEX='.$index;	
									}	
									if(! strcmp($f.'_'.$short_name_lang,$a_field['Field']))
									{
										$exist=true;	
									}
								}
								if(!$exist && $index)
								{
									if($all_field[$index]['Null']=='NO')
										$null='NOT NULL';
									else
										$null='NULL';
								
									if($all_field[$index]['Default']!='')
										$default=' DEFAULT '.$all_field[$index]['Default'];
									else
										$default=null;	
								
									$sql='ALTER TABLE '.$tables.
										' ADD '.$f.'_'.$short_name_lang.' '.
										$all_field[$index]['Type'].
										' COLLATE '.$all_field[$index]['Collation'].
										' '.$null.			
										$default.
										' AFTER '.$f;
									//ALTER TABLE `articles` ADD `title_test` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'pl' AFTER `title` ;
									//echo $sql;
									mysql_query($sql);
								}
								else
								{
									if($exist)
										$this->view->errors[]='Dla mdodułu "'.$module.'" pole "'.$f.'_'.$short_name_lang.'" juz istnieje w tabeli "'.$tables.'"';
									if(!$index)
										$this->view->errors[]='Dla mdodułu "'.$module.'" pole "'.$f.'" nie istnieje w tabeli "'.$tables.'"';
								}
							}
							if($set==0)
							{
								$exist=false; //czy pole jezykowe juz istnieje w bazie danych
								foreach($all_field as $key=>$a_field)
								{
									//dla kazdego pola w tabeli 
									//print_r($f);
									if(! strcmp($f.'_'.$short_name_lang,$a_field['Field']))
									{
										$exist=true;	
									}
								}
								if($exist)
								{	
									$sql='ALTER TABLE '.$tables.' DROP '.$f.'_'.$short_name_lang;
									mysql_query($sql);
									//echo $sql;
								}
								else 
									$this->view->errors[]='Nie można usunąć pola. W mdodule "'.$module.'" w bazie danych dla tabeli "'.$tables.'" nie znaleziono pola "'.$f.'_'.$short_name_lang.'"';
							}
						}	
					
					}
					else
						$this->view->errors[]='Dla mdodułu "'.$module.'" dla tabeli "'.$tables.'" nie znaleziono żadnych pól';
				}
				else
					//echo 'BRAK TABELI';
					$this->view->errors[]='Dla mdodułu "'.$module.'" w bazie danych nie znaleziono tabeli "'.$tables.'"';
			}
		}
	}
	
	private function checkLang($data,$id=null)
	{
	
		if (!preg_match('/^[a-z]{2}$/',$data['short_name']))
				$this->_request->redirectFailure(array('Kod powinien zawierać dwie małe litery.'));
			
		$this->checkLangField('name',$data,$id=null,'Język');
		$this->checkLangField('short_name',$data,$id=null,'Kod');
		
	}	
	
	private function checkLangField($title,$data,$id=null,$text)
	{	
		$model = new Langs();
		$field=$data[$title];
		//echo 'FIELD='.$field;
		$rows=$model->getAll($title.'="'.$field.'"');
		//print_r($rows);
		if(is_null($id))
		{
		//dla insert
			if(count($rows)>0)
				$this->_request->redirectFailure(array($text.' o nazwie "'.$field.'" już istnieje.'));
		}
		else
		{
		//dla edit
			if(count($rows)>0) 
			{
				//echo 'ID='.$id;
				if($rows[0]['id']!=$id)
					//jezeli znajdzie rekord niz edytowany. O innym id
					$this->_request->redirectFailure(array($text.' o nazwie '.$name.' już istnieje.'));
			}
		}
	}
}

?>
