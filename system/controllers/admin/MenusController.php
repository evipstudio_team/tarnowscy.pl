<?php 

class Admin_MenusController extends Cube_Controller_Abstract
{
	private $_id;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
	
	/*private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','n.id');
		$title = new Cube_SearchOrder_Field('title','n.title');
		$author = new Cube_SearchOrder_Field('author','n.author');
		$date = new Cube_SearchOrder_Field('date','n.add_date');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		$search->addField($author);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,advnews';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		$order->addField($author);
		$order->addField($date);
		
		
		//echo $order->getLink('id');
		
	}*/
	
	public function init()
	{
		$this->view->setTemplate('admin');		
		$this->view->id = $this->_id = $this->_request->getParam('id', 0);
		 $this->view->cid=$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->jsTree = $this->displayJsTree2($this->_cid);
		$this->_png = new Cube_Upload_PNG('menu',$this->_request);
		//$this->_cid = $this->_request->getParam('cid', 0);
		//$this->view->cid = $this->_cid;		
		//$this->search_order();				   
	}

	/*public function sortAction()
	{
		$model = new AdvNews();
		$this->view->render('news');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$cid);
		
		if ($cid > 0) {
			$this->view->rows = $model->getAll('c.cid = "'.$cid.'" AND '.$where,$field.' '.$order);		
		} 
		else 
			$this->view->rows =$model->getAll($where,$field.' '.$order);
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new AdvNews();
		$this->view->render('news');
		$where=$this->_search->createWhere($search);
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$cid);
		
		if ($cid > 0) {
			$this->view->rows = $model->getAll('c.cid = "'.$cid.'" AND '.$where, 'n.id DESC');		
		} 
		else 
			$this->view->rows = $model->getAll($where,'n.id DESC');
			
		if (sizeof($this->view->rows) < 1)
		{
			header('refresh: 3; url=admin,advnews,news.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
	}*/
	
	public function indexAction()
	{
		//$this->view->rows = $this->_getCategories();
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		$this->view->cid=$cid;
		//echo 'CID='.$cid;
		$this->view->jsTree = $this->displayJsTree2($cid);
		$this->view->rows = $this->_getSubcategories($cid);		
		//$cats =  $this->_getCategories();
		$this->view->currentPath =  $this->_getBranchPatch($cid);
		//$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$cid);
		
		//wyswietl zdjecia
		/*$model = new AdvNews();
		if ($cid > 0) 
		{
			$this->view->news = $model->getAll('c.cid = "'.$cid.'"', 'cs.id DESC');		
		} else 	$this->view->news = $model->getAll(null, 'c.cid DESC, cs.id DESC');
		*/
	}
	
	public function poscategoriesAction()
	{
		$this->view->render('index');
		$model = new MenusTree();
		$rows=$model->getAllChildren($this->_cid);	
		
		foreach ($rows as $r)
		{
			$data['pos'] = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $data);	
		}
		header('refresh: 3; url=admin,menus,index,cid_'.$this->_cid.'.html');
		$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';
	}	
	
	public function addmenuAction()
	{
		$this->view->render('addmenu');
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->name = null;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->name   = $params['name'];		
			$cid= $this->view->cid   = $params['cid'];
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new MenusAddmenuFilter();
			$filter->filter();			
			$model = new MenusTree();
			$data = $filter->getData();
			$data['parent_id'] = $data['cid'];
			unset($data['cid']);
			$data['label']="";
			$data['href']="";
			$data['extra']="";
			$data['language']="";
			$model->insertTree($data);			
			header('refresh: 3; url=admin,menus,index.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	
	}
		
	public function addcatAction()
	{
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		if ($cid == 1)
		{
			//Jezeli korzeń to dodaj menu a nie submenu.
			$this->addmenuAction();
			return;
		}	
		$this->view->name = null;
		$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$cid);
		$this->getSubpageSelect();
		$this->view->advnews=$this->getAdvnewsSelect();
		$this->view->advgallery=$this->getAdvgallerySelect();
		//$this->view->render('index');		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->name   = $params['name'];		
			$cid= $this->view->cid   = $params['cid'];
			$this->view->label   = $params['label'];	
			$this->view->href   = $params['href'];	
			$this->view->language   = $params['language'];
			$this->view->extra   = $params['extra'];	
			$this->view->logged   = $params['logged'];	
			$this->view->target   = $params['target'];	
			$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$cid);
			$this->getSubpageSelect($params['subpage']);
			$this->view->advnews=$this->getAdvnewsSelect($params['advnews']);
			$this->view->advgallery=$this->getAdvgallerySelect($params['advgallery']);
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new MenusCategoryFilter();
			$filter->filter();			
			$model = new MenusTree();
			$data = $filter->getData();
			$data['parent_id'] = $data['cid'];
			$data['href'].=$data['href_readonly'];
			unset($data['cid']);
			unset ($data['subpage']);
			unset ($data['advnews']);
			unset ($data['advgallery']);
			unset ($data['href_readonly']);
			$id=$model->insertTree($data);	
			if ($_FILES['file1']['name'] != '' &&  $_FILES['file1']['tmp_name'] != '')	
			{
				$this->_png->add('file1');
				$this->_png->insert($id);
			}	
			header('refresh: 3; url=admin,menus,index.html');
			$this->view->message = 'Kategoria dodana pomyślnie! Przekierowywanie...';
		}			
	}	
	
	public function editmenuAction()
	{
		$this->view->render('editmenu');
		$model = new MenusTree();
		$row = $model->get($this->_id);
		
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];
			return;
		}		
		if ($this->_request->isPost()) 
		{
			$filter = new MenusEditmenuFilter();
			$filter->filter();
			$data = $filter->getData();
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,menus,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';	
		}
	}
	
	
	
	public function editcatAction()
	{
		$model= new Upload();
		$files=$model->getAll('module="menu" AND category="png" AND id_element='.$this->_id,'pos,id');
		$this->view->photo=$files[0];
		$idFile=$files[0]['id'];
		
		$model = new MenusTree();
		$row = $model->get($this->_id);
		
		if ($row['parent_id'] == 1)
		{
			//Jezeli rodzic to korzeń to dodaj menu a nie submenu.
			$this->editmenuAction();
			return;			
		}	
		
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		$this->view->label   = $row['label'];	
		$this->view->href   = $row['href'];	
		$this->view->language   = $row['language'];
		$this->view->extra   = $row['extra'];	
		$this->view->logged   = $row['logged'];		
		$this->view->target   = $row['target'];	
		$this->getSubpageSelect();
		$this->view->advnews=$this->getAdvnewsSelect();
		$this->view->advgallery=$this->getAdvgallerySelect();
		$this->view->advgallery=$this->getAdvgallerySelect();
		
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];
			$this->view->label   = $params['label'];	
			$this->view->href   = $params['href'];	
			$this->view->language   = $params['language'];
			$this->view->extra   = $params['extra'];	
			$this->view->logged   = $params['logged'];	
			$this->view->target   = $params['target'];	
			$this->getSubpageSelect($params['subpage']);
			$this->view->advnews=$this->getAdvnewsSelect($params['advnews']);
			$this->view->advgallery=$this->getAdvgallerySelect($params['advgallery']);	
			return;
		}		
		if ($this->_request->isPost()) 
		{
			$filter = new MenusEditCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			$data['href'].=$data['href_readonly'];
			unset ($data['subpage']);
			unset ($data['advnews']);
			unset ($data['advgallery']);
			unset ($data['href_readonly']);
			$model->update($this->_id, $data);
			
			if ($_FILES['file1']['name'] != '' &&  $_FILES['file1']['tmp_name'] != '')	
			{
				$this->_png->add('file1');
				
				if( $idFile > 0 )
					$this->_png->update($this->_id,$idFile);
				else
					$this->_png->insert($this->_id);
			}
			
			header('refresh: 3; url=admin,menus,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie! Przekierowywanie...';	
		}
	}
	public function deletephotoAction()
	{
		//ECHO 'EDITPHOTO';
		$this->view->idFile = $idFile = $this->_request->getParam('idFile', 0);
		$this->view->id=$this->_id;
		if( $idFile > 0 )
		{
			$this->_png->deleteOne($idFile);
						
			header('refresh: 3; url=admin,menus,editcat,id_'.$this->_id.'.html');
			$this->view->message ='Zdjęcie usunięte pomyślnie! Przekierowywanie...';
			$this->view->render('index');	
		}
		else
		{
			header('refresh: 3; url=admin,menus,editcat,id_'.$this->_id.'.html');
			$this->view->message ='Nie udało się usunąć zdjęcia! Przekierowywanie...';
			$this->view->render('index');	
		
		}
	}
	

	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList('MenusTree',$params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) {
			$filter = new MenusDeleteCategoryFilter();
			$filter->filter();	
			$data = $filter->getData();
			$act=$data['act'];
			$cat=$data['cat'];
			$class=array('MenusTree');	//Tabela z nazwami modeli do tabel zaiazanych z drzewem z ktorych trzeba usunac np. dokumenty, zdjecia itp
			//$class=null;
			if ($act == 'delete') 
			{
				$model = new MenusTree();
				$model->deleteTree($this->_id,$class);	
				header('refresh: 3; url=admin,menus,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Przekierowywanie...';
				$this->view->render('index');		
			} 
			/*else if ($act == 'przenies_doc')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść zdjęcia.'));	
				$model = new AdvNewsTree();
				$model->deleteTree($this->_id,$class,$cat,false);
				
				header('refresh: 3; url=admin,advnews,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Newsy przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			}*/ 
			
			else if ($act == 'przenies_cat')  {
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść podkategorie.'));	
				$model = new MenusTree();
				$model->moveAllBranchTree($this->_id,$cat);
				$model->deleteTree($this->_id,$class,$cat,false);
				header('refresh: 3; url=admin,menus,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie! Podkategorie przeniesione! Przekierowywanie...';
				$this->view->render('index');					
			}	
		}
	}
	
	public function deactivecatAction()
	{
		$model = new MenusTree();
		$model->deactiveSubTree($this->_id);
		header('refresh: 3; url=admin,menus,index.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}
		
	public function activecatAction()
	{
		$model = new MenusTree();
		$model->activeSubTree($this->_id);
		header('refresh: 3; url=admin,menus,index.html');
		$this->view->message = 'Kategoria włączona pomyślnie! Przekierowywanie...';
		$this->view->render('index');
	}	
	# END CATEGORIES
	
	private function getSubpageSelect($last_id=null)
    {
		$model=new Articles();
		$rows=$model->getAll(null,'id DESC');
		if(count($rows))
		{
			foreach($rows as $row)
			{
				$title=$row['title'];
				$id=$row['id'];
				if( $id==$last_id )
					$temp .= '<option value="'.$id.'" selected="selected">'.$id.' - '.$title.'</option>';
				else 
					$temp .= '<option value="'.$id.'">'.$id.' - '.$title.'</option>';
			}
		}
		$this->view->subpage=$temp;
	}	

	
	private function getAdvgallerySelect($last_id=null)
    {
		$temp=null;
		$cats = $this->_getCategoriesPath('AdvGalleryTree') ;
		
		if(count($cats))
		{
			foreach($cats as $cat)
			{
				$path=$cat['path'];
				$id_cat=$cat['id'];
				
				if( $id_cat==$last_id )
					$temp .= '<option value="'.$id_cat.'" selected="selected">'.$id_cat.' - '.$path.'</option>';
				else 
					$temp .= '<option value="'.$id_cat.'">'.$id_cat.' - '.$path.'</option>';								
				
			}
		}
		return $temp;
	}	
	
	private function getAdvnewsSelect($last_id=null)
    {
		$temp=null;
		$cats = $this->_getCategoriesPath('AdvNewsTree') ;
		$model = new AdvNews();
		$newsy = $model->getAll();
		//echo 'last_id='.$last_id;		
		if(count($cats))
		{
			$cat_id=null;
			if( preg_match('/^C-(\d+)$/', $last_id, $matches) )	
				$cat_id=$matches[1];
			
			$news_id=null;
			if( preg_match('/^E-(\d+)$/', $last_id, $matches) )	
				$news_id=$matches[1];
			
			foreach($cats as $cat)
			{
				$path=$cat['path'];
				$id_cat=$cat['id'];
				
				if( $id_cat==$cat_id )
					$temp .= '<option value="C-'.$id_cat.'" selected="selected">'.$id_cat.' - '.$path.'</option>';
				else 
					$temp .= '<option value="C-'.$id_cat.'">'.$id_cat.' - '.$path.'</option>';
				if(count($newsy))
				{	
					foreach($newsy as $news)
					{
						$title=$news['title'];
						$id=$news['id'];
					
						if($news['cat_id']==$id_cat)
						{
							if( $id==$news_id )
								$temp .= '<option value="E-'.$id.'" selected="selected">&nbsp&nbsp&nbsp'.$id.' - '.$title.'</option>';
							else
								$temp .= '<option value="E-'.$id.'">&nbsp&nbsp&nbsp'.$id.' - '.$title.'</option>';
						}
					}
				}
			}
		}
		return $temp;
	}	
	
	//TREE
	private function displayJsTree2($current)
	{
		$conts = "<a href=\"javascript: d.openAll();\" class=\"close\">+ rozwiń drzewko</a><a href=\"javascript: d.closeAll();\" class=\"close\">- zwiń drzewko</a><br clear=\"all\" /><br />
			<div class=\"dtree\">
			<noscript>
				Twoja przegladarka nie obsługuje javascript!
			</noscript>
			<script type=\"text/javascript\">
		<!--

		d = new dTree('d');";
		
		$model = new MenusTree();
		$rows=$model->getAllTreeWSTCC(null,'t.pos,t.name');
		
		
		if(count($rows) > 0)
		{
			
			//$was = false;
			foreach($rows as $row)
			{
				if( !isSet($row['count_child']) )
					$row['count_child']=0;
					
				$link='admin,menus,index,cid_'.$row['id'].'.html';
				require 'templates/admin/tree/show_tree.php';
			}
							
 
			$conts .= '
			d.openTo('.$current.', true);
			document.write(d);
		
			//-->
			</script>
			</div>';
				
		} else $conts = '<p>brak kategorii</p>';
		
		return $conts;	
	}

	private function _getCategoriesPath($modelName, $cid = null, $no = false)
	{
		$model = new $modelName();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		$pathTab = array();
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		$pathTab[0]['path'] = $row['name'];
		$pathTab[0]['id'] = $row['id'];
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			//print("Wynik  funkcji print_r:<BR><pre>");
			//print_r($rows);
			//print("</pre><BR>");
			$count=1;
			foreach($rows as $row)
			{
				//print_r($row);
				if($row['id'] == 1)
				{
					$path=$row['name'].'&nbsp/&nbsp'.$path;
					$pathTab[$count]['path'] = $path;
					$pathTab[$count]['id'] = $last_id;
					$path='';
					$count++;
						
				}
				else
				{
					$path.=$row['name'].'&nbsp/&nbsp';
					$last_id=$row['id'];	
				
				}
				
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $pathTab;		
	}
	
	
	private function _getCategoriesList($modelName, $cid = null, $no = false)
	{
		$model = new $modelName();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].'/'.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].'/';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;		
	}

	private function _getSubCategories($parent_id)
	{
		$model = new MenusTree();
		$rows=$model->getAllChildren($parent_id);
		
		//ile dokumentow jest w kazdej kategorii
		/*$model = new AdvNews();
		foreach ($rows as $i=>$row)
		{
			$temp=$model->count_rows('cid='.$row['id']);	
			$rows[$i]['amount']=$temp['amount'];
		}*/
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		return $rows;		
	}
	
	private function _getBranchPatch($cid,$path_string=null)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new MenusTree();
		$rows=$model->getBranchTree($cid);
		if( is_null($path_string) )
			return $rows;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		$temp='';
		foreach($rows as $row)
			$temp.=$row['name'].'/';
		
		return $temp;
		
	}
	
}

?>
