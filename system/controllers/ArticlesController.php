<?php 

class ArticlesController extends Cube_Controller_Abstract
{
	private $_page = 'default';
	private $_table = null;
	private $_language_temp;
	public function init()
	{
		$this->view->page = $this->_page = $this->_request()->getParam('page', 'default');
		$this->view->table = $this->_table = $this->_request()->getParam('table', null);
		$this->_language_temp=$this->_session->getLanguageTemp();
	}

	public function indexAction()
	{	
		$this->view->render('subpage', true);
		$this->view->toc = null;
		if ($this->_page == 'default') {
			$this->view->homePage = true;
			if (!is_null($this->_table)) 
			{
				$model = new ArticlesTOC();
				$row = $model->get($this->_table);
				$this->view->toc = $row['contents'];
				if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) 
					$this->view->toc = '';
			}				
		} else {
			$this->view->homePage = false;
			$model = new Articles();
			$row = $model->get($this->_page,$this->_language_temp);
			if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) {
				header('location: zaloguj.html');
				return;
			}	
			
			$this->view->row = $row;
			$this->view->title = $row['meta_title'];
			$this->view->toc = null;
			if (!is_null($this->_table)) 
			{
				$model = new ArticlesTOC();
				$row = $model->get($this->_table);
				$this->view->toc = $row['contents'];
				if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) 
					$this->view->toc = '';
				$model = new Articles();
			}
			$model->updateViews($this->_page);	
			
			$model = new Upload();
			$this->view->module=$module=$this->_request()->getController();
			$this->view->photo=$model->getAll('module="'.$module.'" AND category="photo" AND id_element='.$this->_page,'pos,id');
			$this->view->pdf=$model->getAll('module="'.$module.'" AND category="pdf" AND id_element='.$this->_page,'pos,id');
			$this->view->document=$model->getAll('module="'.$module.'" AND category="document" AND id_element='.$this->_page,'pos,id');
		}
	}	
}

?>
