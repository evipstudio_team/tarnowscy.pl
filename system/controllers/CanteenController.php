<?php 

class CanteenController extends Cube_Controller_Abstract
{	
	private function create_jsonArray($rows)
{
 $tmp = array();
 $tmp_row = array();
 foreach ($rows as $r)
 {
  $start_date=$r['start_date'];
  $end_date=$r['end_date'];
  
  for($date=$start_date;$date<=$end_date;$date+=24*3600)
  {
   $tmp_row['eventDate']=date('Ymd', $date);
   //$tmp_row['dateLink']='admin,canteen,edit,id_'.$r['id_canteen'].'.html';
   
   //nie mozna usunac
   $tmp_row['eventDescription']='';
   
   $tmp[]=$tmp_row;
  }
  
  
 }
 $this->view->array_json=$tmp;
 //print_r($tmp);  
}
	
	
	
	public function init()
	{
		//$this->_pp = $this->_config->get('per_page', 'canteen');
		//$this->search_order();
	}

	public function indexAction()
	{
		
		$model = new Canteen;
		$this->view->rows= $model->getAll(null, 'pos,id ASC');		
		
	}

	/*public function sortAction()
	{
		$this->_where=$this->_order->createWhere();
		$this->_field=$this->_order->getFieldSql();
		$this->_sequence=$this->_order->getOrder();
		
		$this->view->render('index');
		$this->indexAction();
	}
	
	
	public function searchAction()
	{
		
		$search=clear($_POST['search']);
		$this->_where=$this->_search->createWhere($search);
		
		$this->view->render('index');
		$this->indexAction();
	}*/
	
	
	
	
	public function viewAction()
	{		
		$this->view->id = $id = $this->_request()->getParam('id', 0);
		
		$model = new Canteen();
		$this->view->row=$model->get($id);
		$model = new CanteenDates();
		$dates= $model->getAll('id_canteen='.$id, 'id ASC');
		$this->create_jsonArray($dates);	
		
		
		$model = new Upload();
		$this->view->module=$module=$this->_request()->getController();
		$this->view->photo=$model->getAll('module="'.$module.'" AND category="photo" AND id_element='.$id,'pos,id');
		$this->view->pdf=$model->getAll('module="'.$module.'" AND category="pdf" AND id_element='.$id,'pos,id');
		$this->view->document=$model->getAll('module="'.$module.'" AND category="document" AND id_element='.$id,'pos,id');
	}

	
	
}

?>
