<?php 

class ArticlesTOCController extends Cube_Controller_Abstract
{
	private $_page = 'default';
	private $_table = null;
	
	public function init()
	{
		$this->view->page = $this->_page = $this->_request()->getParam('page', 'default');
		$this->view->table = $this->_table = $this->_request()->getParam('table', null);
	}

	public function indexAction()
	{	
		$this->view->render('subpage', true);
		$this->view->toc = null;
		
		if ($this->_page == 'default') {
			$this->view->homePage = true;
			if (!is_null($this->_table)) {
				$row = $model->getTOC($this->_table);
				$this->view->toc = $row['contents'];
				if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) 
					$this->view->toc = '';
			}				
		} else {
			$this->view->homePage = false;
			$model = new Articles();
			$row = $model->get($this->_page);
			
			if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) {
				header('location: zaloguj.html');
				return;
			}	
			
			$this->view->row = $row;
			$this->view->title = $row['meta_title'];
			$this->view->toc = null;
			if (!is_null($this->_table)) {
				$row = $model->getTOC($this->_table);
				$this->view->toc = $row['contents'];
				if ($row['logged'] == 1 && !$this->_session->isLoggedIn()) 
					$this->view->toc = '';
			}
			$model->updateViews($this->_page);	
		}
	}	
}

?>
