<html>
<head>
<link rel="stylesheet" href="jscripts/calendarEvent/css/jec-grey.css" type="text/css" />
<link rel="stylesheet" href="jscripts/calendarEvent/css/jec-styled.css" type="text/css" />
<script type="text/javascript" src="jscripts/calendarEvent/calendar-2.2.js"></script>
  
 <body>

  <div id='myCalendarContainer'></div>
  <script type="text/javascript">
  
	var obj = eval(<? echo json_encode($array_json); ?>);
    //document.createElement("ul");
	//for(val in obj){
	//	alert(obj[val].eventDate);
	//	} 
  
  
  
      var myCal = new JEC('myCalendarContainer', 
		{
			tableClass: 'greyCalendar',
			linkNewWindow: false,
			firstDayOfWeek: 2,
			specialDays: [ 1,7 ],
			weekdays: [
						"Nd",
						"Pon",
						"Wt",
						"&#346r",
						"Czw",
						"Pt",
						"Sob"						
					  ],
			months: [
					"Stycze&#324",
					"Luty",
					"Marzec",
					"Kwiecie&#324",
					"Maj",
					"Czerwiec",
					"Lipiec",
					"Sierpie&#324",
					"Wrzesie&#324",
					"Pa&#378dziernik",
					"Listopad",
					"Grudzie&#324"
					]
		  
			//firstMonth: 201003,
			//lastMonth: 201010
		});
		
	
	myCal.defineEvents(obj);
	myCal.linkDates(obj);
	/*myCal.defineEvents(
	[
		{
			eventDate: 20100407,
			eventDescription: 'JEC 2.0 Released',
			eventLink: 'http://calendar.ilsen.net'
		},
		{ eventDate: 20100705, eventDescription: 'Kevin\'s Birthday' }
	]
	);*/
	
	//myCal.linkDate(20110805, 'http://calendar.ilsen.net');
	//myCal.defineEvent(20110807, '<img src="/public/seat/photo/mini/1312196417_Tree.jpg" alt="" />', 'http://calendar.ilsen.net', "/public/seat/photo/mini/1312196417_Tree.jpg", 50, 50)	
	myCal.showCalendar();
  </script>
	


</body> 
</head>